# Markov Chain based WFC

This project is a research performed on Markov-chain based wave function collapse(WFC) for procedrual content generation of 2D images. 
The project provides 3 Wave Function Collapse models, they are:

1. Overlapping WFC
2. Tiling WFC
3. Markov-chain WFC

# Data used

The collection of images used as inputs to generate outputs are located at

- *Path: /Run/Data/Images/WFCInputSamples/*

The outputs are generated in a folder under the location:

- *Path: /Run/Data/WFCResults/Run_<ExecutionDateTime>/*

The WFC generators that will be used are defined using an XML file.
The XML generator data is located in teh file at:

- *Samples: /Run/Data/Gameplay/samples.xml*

There is also a file that contains all the possible WFC options that can be used to 
generate results in WFC. This file is located at:

- *All Samples: /Run/Data/Gameplay/allSamples.xml*

# Source Code

The source code can be accessed by viewing the WFCProject.sln file with Visual Studio. This poject uses C++ 17 
and requires that you have the windows SDK installed on your system.

# Run

To run the application, you can access the x64 versions of the application. The path to the executable is :

- *Path: /Run/WFCProject_x64.exe*