var class_propagator =
[
    [ "PropagatorState", "class_propagator.html#a3d0b974c6d6d7bf843cf9716d8a8a97c", null ],
    [ "Propagator", "class_propagator.html#a3f8770db297f45c8decada89a9f4805c", null ],
    [ "AddToPropagator", "class_propagator.html#a0b59096232fa757f461314fac59297ab", null ],
    [ "Propagate", "class_propagator.html#afc5df4966762aeceb74dc68877d2759e", null ],
    [ "compatible", "class_propagator.html#ab54f0184e24c080ec58bc6dc8637d2c9", null ],
    [ "m_patternsSize", "class_propagator.html#ab0f78e1830e55b95c98d5c5f04c4a2d4", null ],
    [ "m_propagator_state", "class_propagator.html#aaf5d16df14c5f191ebe8badc5061e840", null ],
    [ "m_waveHeight", "class_propagator.html#a1f2d4a05ac53151407b041860dfb5c2d", null ],
    [ "m_waveWidth", "class_propagator.html#a53f20719bd61895a7594ef4851ee8da7", null ],
    [ "periodic_output", "class_propagator.html#ad50503c2608a972d072fc0a893e69e3d", null ],
    [ "propagating", "class_propagator.html#ae63ad7410cd2b4736bd985afe05462dc", null ]
];