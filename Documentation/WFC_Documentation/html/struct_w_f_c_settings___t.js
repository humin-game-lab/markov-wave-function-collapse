var struct_w_f_c_settings___t =
[
    [ "configFileName", "struct_w_f_c_settings___t.html#a6e19f96cd532fb6b4d01f759213de9fa", null ],
    [ "configReadPath", "struct_w_f_c_settings___t.html#ad5cafca2c60eacc8eaedb984512261ec", null ],
    [ "defaultHeight", "struct_w_f_c_settings___t.html#a2bf88d478a696e5fe7f6bb415890dd6a", null ],
    [ "defaultNumOutputImages", "struct_w_f_c_settings___t.html#aa0ea59da6467d3d6f03d9e326f06f330", null ],
    [ "defaultWidth", "struct_w_f_c_settings___t.html#ac821b717a54de1b0b62355aaa8e0e243", null ],
    [ "imageOutPath", "struct_w_f_c_settings___t.html#afde8a78a045a327f0180b2cd2e8fe529", null ],
    [ "imageReadPath", "struct_w_f_c_settings___t.html#a98dde36a25d09c9197a9dabac3cbf381", null ]
];