var struct_overlapping_w_f_c_options =
[
    [ "GetWaveHeight", "struct_overlapping_w_f_c_options.html#afe1fa7520187ff2471f5e8eacd280814", null ],
    [ "GetWaveWidth", "struct_overlapping_w_f_c_options.html#a46b65994e2289160a2c34c3fca59a778", null ],
    [ "m_ground", "struct_overlapping_w_f_c_options.html#aaddc17fb72b43daaf0a49719b794151c", null ],
    [ "m_outHeight", "struct_overlapping_w_f_c_options.html#a820260c63766e8fc5edbd317e2e8d2d5", null ],
    [ "m_outWidth", "struct_overlapping_w_f_c_options.html#a1b16b11e25687c351762a1bdd6335380", null ],
    [ "m_patternSize", "struct_overlapping_w_f_c_options.html#a181b386a1888af0e06423354d53fd786", null ],
    [ "m_periodicInput", "struct_overlapping_w_f_c_options.html#a00b52fcd65c875ed08515767e8007f4b", null ],
    [ "m_periodicOutput", "struct_overlapping_w_f_c_options.html#a893c894807dc6fcf010c5ad5d524ed7f", null ],
    [ "m_symmetry", "struct_overlapping_w_f_c_options.html#a69621550ab9a35ad7f77dd5e1bda0578", null ]
];