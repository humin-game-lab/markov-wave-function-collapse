var class_w_f_c =
[
    [ "ObserveStatus", "class_w_f_c.html#ac9e861b5bd887434b6ca607fc833a8f9", [
      [ "SUCCESS", "class_w_f_c.html#ac9e861b5bd887434b6ca607fc833a8f9a445bc565200657843a6e6862db9986b6", null ],
      [ "FAILURE", "class_w_f_c.html#ac9e861b5bd887434b6ca607fc833a8f9a9927d3debe7ea6db428fcc3cc5e20dbd", null ],
      [ "TO_CONTINUE", "class_w_f_c.html#ac9e861b5bd887434b6ca607fc833a8f9a68b87df5710d84a348c73d373c758645", null ]
    ] ],
    [ "WFC", "class_w_f_c.html#a9bb0bea4ee153ef20db9943b795e8ea1", null ],
    [ "Observe", "class_w_f_c.html#a0eb8a3932f56b06b258298198765425d", null ],
    [ "Propagate", "class_w_f_c.html#a5647b2329ec619879baaf246ca30ba29", null ],
    [ "RemoveWavePattern", "class_w_f_c.html#a14050306fd54e58a5aec5edd2b68024a", null ],
    [ "Run", "class_w_f_c.html#ab06acf84715a6576a26b9f99c833a71d", null ],
    [ "m_propagator", "class_w_f_c.html#a7baebfce9974122325f6b1aec6ed5c5f", null ]
];