var struct_entropy_memoisation =
[
    [ "entropy", "struct_entropy_memoisation.html#ab3ab4b9549b345045904051d6ba43cf9", null ],
    [ "log_sum", "struct_entropy_memoisation.html#a610462bad5c829822e3038f345b39c6a", null ],
    [ "nb_patterns", "struct_entropy_memoisation.html#ab306517a27bf4882bb7dd498709175a9", null ],
    [ "plogp_sum", "struct_entropy_memoisation.html#ace4c80ce1eec81af6c59dcdcc4f672d3", null ],
    [ "sum", "struct_entropy_memoisation.html#aac342f8b145cbcaf6e6745abdf5ba0b7", null ]
];