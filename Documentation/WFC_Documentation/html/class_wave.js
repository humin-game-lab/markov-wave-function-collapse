var class_wave =
[
    [ "Wave", "class_wave.html#a432bb5673e0ba1c925a4c019641d7843", null ],
    [ "Get", "class_wave.html#aebf640fa7c8c05116190f99db3bb575f", null ],
    [ "Get", "class_wave.html#a828283fa1a1471acfd256fd5e0fa95c3", null ],
    [ "GetMinEntropy", "class_wave.html#a2b109d8f8ef6c30338119f655002a18b", null ],
    [ "Set", "class_wave.html#a41927e0d6c2c1369267a4ff3909c5821", null ],
    [ "Set", "class_wave.html#a5133fdeb68b88ffe10cee5a97e1b75fb", null ],
    [ "height", "class_wave.html#a9832cd4986904aeccd703d48167ca6bb", null ],
    [ "size", "class_wave.html#ae814f070193b031b176f04b62cd89b9d", null ],
    [ "width", "class_wave.html#a0b40086ca0809c880dfb7c359d51d84a", null ]
];