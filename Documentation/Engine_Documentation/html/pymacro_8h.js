var pymacro_8h =
[
    [ "_Py_ALIGN_DOWN", "pymacro_8h.html#ad2f587a91b69f192312631bd207364f1", null ],
    [ "_Py_ALIGN_UP", "pymacro_8h.html#a9f13a61cae6af3f6bb72c1f0d913ef5e", null ],
    [ "_Py_IS_ALIGNED", "pymacro_8h.html#a5955619fde2fb8fbde6e9cf642a02c7c", null ],
    [ "_Py_SIZE_ROUND_DOWN", "pymacro_8h.html#a8d9842099c0e6cecb696cd80f9f27a2d", null ],
    [ "_Py_SIZE_ROUND_UP", "pymacro_8h.html#aa6cb788e9c409347812db99c455c971a", null ],
    [ "_Py_XSTRINGIFY", "pymacro_8h.html#a545bbb273e8b2cd95db0b62967b1440a", null ],
    [ "Py_ABS", "pymacro_8h.html#a02a252f7d7f70a846977c773bfcbf7a2", null ],
    [ "Py_ARRAY_LENGTH", "pymacro_8h.html#acd4de5900c789ba51814288242091d29", null ],
    [ "Py_BUILD_ASSERT", "pymacro_8h.html#a484867ac5261f679fcc0669e63fde1fd", null ],
    [ "Py_BUILD_ASSERT_EXPR", "pymacro_8h.html#a97ec8dce858b9594ad5dbc61703de71c", null ],
    [ "Py_CHARMASK", "pymacro_8h.html#adb7a5513dbd53a5afce3e1e657583e06", null ],
    [ "Py_MAX", "pymacro_8h.html#afbdd760ba6bfa42522b3543ee34ef56c", null ],
    [ "Py_MEMBER_SIZE", "pymacro_8h.html#ae3890854c1fef144a010fad88e9b994e", null ],
    [ "Py_MIN", "pymacro_8h.html#a51968ce527e1712abac47f7cfe5eae8d", null ],
    [ "Py_STRINGIFY", "pymacro_8h.html#a478031098895b49b33194c6c3ee50a34", null ],
    [ "Py_UNREACHABLE", "pymacro_8h.html#af3efd77161e71026f35ea2de82db0bde", null ],
    [ "Py_UNUSED", "pymacro_8h.html#ad23fa9ae2faa7c84ea150e5f6fad0224", null ],
    [ "PyDoc_STR", "pymacro_8h.html#a955f358851bb49e2f1283b29757fe0e0", null ],
    [ "PyDoc_STRVAR", "pymacro_8h.html#acd6df347688600918b79b72d422ac31a", null ],
    [ "PyDoc_VAR", "pymacro_8h.html#aa2db3f1f66818616529a581735e7caeb", null ]
];