var structphysx_1_1_px_contact_pair_point =
[
    [ "impulse", "structphysx_1_1_px_contact_pair_point.html#ad1b3b99a4f29399c2bc5a756025e0db7", null ],
    [ "internalFaceIndex0", "structphysx_1_1_px_contact_pair_point.html#a1942f1498927f6045041457167bb723d", null ],
    [ "internalFaceIndex1", "structphysx_1_1_px_contact_pair_point.html#a6ed9312931f7b21fa025091ca2af42b8", null ],
    [ "normal", "structphysx_1_1_px_contact_pair_point.html#adfb57b7958604c0deb1c45fdcc846d9c", null ],
    [ "position", "structphysx_1_1_px_contact_pair_point.html#a887a8c3e7c7b90b00d5c92a294155d2d", null ],
    [ "separation", "structphysx_1_1_px_contact_pair_point.html#a9e0e28264683483a387c31068a634e63", null ]
];