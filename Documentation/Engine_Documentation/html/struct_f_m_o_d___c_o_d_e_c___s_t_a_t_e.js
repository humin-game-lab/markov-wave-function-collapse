var struct_f_m_o_d___c_o_d_e_c___s_t_a_t_e =
[
    [ "filehandle", "struct_f_m_o_d___c_o_d_e_c___s_t_a_t_e.html#af70df892eb7bc2cb3b29854536fad747", null ],
    [ "fileread", "struct_f_m_o_d___c_o_d_e_c___s_t_a_t_e.html#a938fd270ea867d6c29c406381793ee6d", null ],
    [ "fileseek", "struct_f_m_o_d___c_o_d_e_c___s_t_a_t_e.html#a3c814d627ae700dbd21bd626d663f73e", null ],
    [ "filesize", "struct_f_m_o_d___c_o_d_e_c___s_t_a_t_e.html#af314546d98e746687bfeaae8a59f792c", null ],
    [ "metadata", "struct_f_m_o_d___c_o_d_e_c___s_t_a_t_e.html#a0724fc5682690a32814d3301aef59810", null ],
    [ "numsubsounds", "struct_f_m_o_d___c_o_d_e_c___s_t_a_t_e.html#af9c17a02d9b967fa0ececf991a654dfd", null ],
    [ "plugindata", "struct_f_m_o_d___c_o_d_e_c___s_t_a_t_e.html#a8201fa8b60e1cf571ac0d66e678b3928", null ],
    [ "waveformat", "struct_f_m_o_d___c_o_d_e_c___s_t_a_t_e.html#a785115172e17e38c3baf764e8f67b962", null ],
    [ "waveformatversion", "struct_f_m_o_d___c_o_d_e_c___s_t_a_t_e.html#aaabe04329018e5e8d98c7a15c523df8b", null ]
];