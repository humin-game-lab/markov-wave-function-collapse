var _px_batch_query_desc_8h =
[
    [ "PxBatchQueryStatus", "structphysx_1_1_px_batch_query_status.html", "structphysx_1_1_px_batch_query_status" ],
    [ "PxBatchQueryResult", "structphysx_1_1_px_batch_query_result.html", "structphysx_1_1_px_batch_query_result" ],
    [ "PxBatchQueryMemory", "structphysx_1_1_px_batch_query_memory.html", "structphysx_1_1_px_batch_query_memory" ],
    [ "PxBatchQueryDesc", "classphysx_1_1_px_batch_query_desc.html", "classphysx_1_1_px_batch_query_desc" ],
    [ "PxOverlapQueryResult", "_px_batch_query_desc_8h.html#a272afbde177b486c6f1953c5c4325f52", null ],
    [ "PxRaycastQueryResult", "_px_batch_query_desc_8h.html#aaf3f2643b515f6af94e7dbf756a9dd9c", null ],
    [ "PxSweepQueryResult", "_px_batch_query_desc_8h.html#a9ce9c91c34a9acbe2e26f62db95cd047", null ]
];