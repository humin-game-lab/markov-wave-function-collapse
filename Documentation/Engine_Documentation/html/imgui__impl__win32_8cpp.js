var imgui__impl__win32_8cpp =
[
    [ "DBT_DEVNODES_CHANGED", "imgui__impl__win32_8cpp.html#a454522b0cfb3a366cc14230e62be3e94", null ],
    [ "MAP_ANALOG", "imgui__impl__win32_8cpp.html#adb5084b87f1cecae595f6a70b5da263a", null ],
    [ "MAP_BUTTON", "imgui__impl__win32_8cpp.html#ac0e4756c36e2ab0b0b13061a722d7dd1", null ],
    [ "WIN32_LEAN_AND_MEAN", "imgui__impl__win32_8cpp.html#ac7bef5d85e3dcd73eef56ad39ffc84a9", null ],
    [ "WM_MOUSEHWHEEL", "imgui__impl__win32_8cpp.html#a7b2918d422b3a1fe86a641f74818bb55", null ],
    [ "ImGui_ImplWin32_Init", "imgui__impl__win32_8cpp.html#a4221d0dc7d4977eb30970e1f5c2cca98", null ],
    [ "ImGui_ImplWin32_NewFrame", "imgui__impl__win32_8cpp.html#ade4f55b90fdbd75a562d894d33f1909e", null ],
    [ "ImGui_ImplWin32_Shutdown", "imgui__impl__win32_8cpp.html#a4eddd1969cc460312197f2df3dbc992a", null ],
    [ "ImGui_ImplWin32_WndProcHandler", "imgui__impl__win32_8cpp.html#a25447e4d5d2cabecf54cb9a5c84e7cde", null ]
];