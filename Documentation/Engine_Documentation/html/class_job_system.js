var class_job_system =
[
    [ "AddFinishedJobForCategory", "class_job_system.html#a5e6798fd5a8444c231373c7c610d8d91", null ],
    [ "AddJobForCategory", "class_job_system.html#a819230f10c0d8f069e7689f7fc4fbd10", null ],
    [ "ProcessCategory", "class_job_system.html#ac966f96c658a8c26beccf2685bdcc072", null ],
    [ "ProcessCategoryForTimeInMS", "class_job_system.html#a959e57bc82cb15ead95cf2525fc66d87", null ],
    [ "ProcessFinishJobsForCategory", "class_job_system.html#add7ce2414abcef3dbe5e2c4178dea107", null ],
    [ "Run", "class_job_system.html#aecb6b9615739c2d0f6f83424f46bbd27", null ],
    [ "Shutdown", "class_job_system.html#a5c49929f8349ebe50f01f822ed969962", null ],
    [ "Startup", "class_job_system.html#a81ab87a24f113f37c5cae0d73c070a6e", null ]
];