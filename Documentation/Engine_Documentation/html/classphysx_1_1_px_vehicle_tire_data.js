var classphysx_1_1_px_vehicle_tire_data =
[
    [ "PxVehicleTireData", "classphysx_1_1_px_vehicle_tire_data.html#a5f4e15ff57263d12af99c1fe18512eaf", null ],
    [ "getFrictionVsSlipGraphRecipx1Minusx0", "classphysx_1_1_px_vehicle_tire_data.html#ab5998822878f1bedc90751be8cd0919a", null ],
    [ "getFrictionVsSlipGraphRecipx2Minusx1", "classphysx_1_1_px_vehicle_tire_data.html#a020bae7e98ab0ae02f445f6dffb45020", null ],
    [ "getRecipLongitudinalStiffnessPerUnitGravity", "classphysx_1_1_px_vehicle_tire_data.html#a04c0205326e3ff731b330342a0e3d877", null ],
    [ "PxVehicleWheels4SimData", "classphysx_1_1_px_vehicle_tire_data.html#a4c2a2bf5a268389976a6f3c6ffeb1806", null ],
    [ "mCamberStiffnessPerUnitGravity", "classphysx_1_1_px_vehicle_tire_data.html#a5073ac4d4974326d1bddf0e82f18f138", null ],
    [ "mFrictionVsSlipGraph", "classphysx_1_1_px_vehicle_tire_data.html#a989bfe6ada484f8bafd56825e15da396", null ],
    [ "mLatStiffX", "classphysx_1_1_px_vehicle_tire_data.html#a2d7bd54e0ba46655e1c0acc746568e35", null ],
    [ "mLatStiffY", "classphysx_1_1_px_vehicle_tire_data.html#ae1f5c03095ab6d41356e62f29d72db45", null ],
    [ "mLongitudinalStiffnessPerUnitGravity", "classphysx_1_1_px_vehicle_tire_data.html#ab2880ec2fa462926966387c9471866ea", null ],
    [ "mType", "classphysx_1_1_px_vehicle_tire_data.html#aa43e34533ae7bba64b1dd801bca85953", null ]
];