var structphysx_1_1_px_pruning_structure_type =
[
    [ "Enum", "structphysx_1_1_px_pruning_structure_type.html#a726793bb8900a198e0f9d9507acfc4ca", [
      [ "eNONE", "structphysx_1_1_px_pruning_structure_type.html#a726793bb8900a198e0f9d9507acfc4caa0096d4e593de515f4a3f2f8e2b6a4610", null ],
      [ "eDYNAMIC_AABB_TREE", "structphysx_1_1_px_pruning_structure_type.html#a726793bb8900a198e0f9d9507acfc4caa45118cbcdbdc1e9dfdff464b457b5305", null ],
      [ "eSTATIC_AABB_TREE", "structphysx_1_1_px_pruning_structure_type.html#a726793bb8900a198e0f9d9507acfc4caa5a8e1ab7b50aea1320651436c7622ff2", null ],
      [ "eLAST", "structphysx_1_1_px_pruning_structure_type.html#a726793bb8900a198e0f9d9507acfc4caa3bfd7940a3bea110d754079857cdf049", null ]
    ] ]
];