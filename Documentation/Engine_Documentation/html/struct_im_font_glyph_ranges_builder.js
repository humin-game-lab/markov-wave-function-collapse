var struct_im_font_glyph_ranges_builder =
[
    [ "ImFontGlyphRangesBuilder", "struct_im_font_glyph_ranges_builder.html#aaee7673423cb7e29d43ea3901628c870", null ],
    [ "AddChar", "struct_im_font_glyph_ranges_builder.html#a6d3f5e3e377a73f4f4324c4cf98600dd", null ],
    [ "AddRanges", "struct_im_font_glyph_ranges_builder.html#ac28bc574d4d34d3a2889cda34470ae71", null ],
    [ "AddText", "struct_im_font_glyph_ranges_builder.html#ade9770bde0f63b4630df30402f3619cf", null ],
    [ "BuildRanges", "struct_im_font_glyph_ranges_builder.html#abc11a683e1b345299c42abd8b6c422a5", null ],
    [ "GetBit", "struct_im_font_glyph_ranges_builder.html#a6143564d6b8399ca4e1dfc17e688114e", null ],
    [ "SetBit", "struct_im_font_glyph_ranges_builder.html#a36c59006df968d10c8d2b24b8011ec81", null ],
    [ "UsedChars", "struct_im_font_glyph_ranges_builder.html#ab62ce804c8040670f6adaa4ce1b3b27b", null ]
];