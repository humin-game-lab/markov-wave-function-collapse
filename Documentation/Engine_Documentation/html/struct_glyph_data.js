var struct_glyph_data =
[
    [ "m_aspectRatio", "struct_glyph_data.html#a97c98a1c6ecf60cae9818a02b3a65541", null ],
    [ "m_paddedWidthAfterGlyph", "struct_glyph_data.html#a7995a7b88b774b476654445891eb4f57", null ],
    [ "m_paddedWidthAtGlyph", "struct_glyph_data.html#a6db6e637efada628b2c7741786789b27", null ],
    [ "m_paddedWidthBeforeGlyph", "struct_glyph_data.html#af31da762e452f31da521b610ff988a61", null ],
    [ "m_texCoordMaxs", "struct_glyph_data.html#a50f646bf1ecbb2d6ec3dc6fa149bc21f", null ],
    [ "m_texCoordMins", "struct_glyph_data.html#ad555d0e1b9727ecf3449f82282378839", null ]
];