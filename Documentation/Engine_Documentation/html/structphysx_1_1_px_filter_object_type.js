var structphysx_1_1_px_filter_object_type =
[
    [ "Enum", "structphysx_1_1_px_filter_object_type.html#ac6b0a7e9801f1a591b42a773eda8526b", [
      [ "eRIGID_STATIC", "structphysx_1_1_px_filter_object_type.html#ac6b0a7e9801f1a591b42a773eda8526bab0afbdc1c699ed953933db3be595a58b", null ],
      [ "eRIGID_DYNAMIC", "structphysx_1_1_px_filter_object_type.html#ac6b0a7e9801f1a591b42a773eda8526ba832d8e72f509ef9452c7f7f1583387a7", null ],
      [ "eARTICULATION", "structphysx_1_1_px_filter_object_type.html#ac6b0a7e9801f1a591b42a773eda8526ba317f474c7a5a2a66f1b8513bb7ff6d1d", null ],
      [ "eMAX_TYPE_COUNT", "structphysx_1_1_px_filter_object_type.html#ac6b0a7e9801f1a591b42a773eda8526ba50a26cda8f26c909eb66a1e12d78d139", null ],
      [ "eUNDEFINED", "structphysx_1_1_px_filter_object_type.html#ac6b0a7e9801f1a591b42a773eda8526bad6231974bfe12cf1f9a10e9c21fa32a8", null ]
    ] ]
];