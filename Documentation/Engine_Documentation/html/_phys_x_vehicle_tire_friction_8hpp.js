var _phys_x_vehicle_tire_friction_8hpp =
[
    [ "VEHICLE_TIREFRICTION_H", "_phys_x_vehicle_tire_friction_8hpp.html#a7f243de188f0873f759316a0c74b3edd", null ],
    [ "SURFACE_TYPE_TARMAC", "_phys_x_vehicle_tire_friction_8hpp.html#ac5537d29f6f1586081d490f9de67e93facf41e971302aec13787224cff0860844", null ],
    [ "MAX_NUM_SURFACE_TYPES", "_phys_x_vehicle_tire_friction_8hpp.html#ac5537d29f6f1586081d490f9de67e93fa043a9ee7c4a74006aa7476f56413ea4a", null ],
    [ "TIRE_TYPE_NORMAL", "_phys_x_vehicle_tire_friction_8hpp.html#ab12ccf2a22f0c9962fc1353be414351cac648a9b3765048ca9cd3f026313203ee", null ],
    [ "TIRE_TYPE_WORN", "_phys_x_vehicle_tire_friction_8hpp.html#ab12ccf2a22f0c9962fc1353be414351ca8e0981dd2a8b6279976109dce02164af", null ],
    [ "MAX_NUM_TIRE_TYPES", "_phys_x_vehicle_tire_friction_8hpp.html#ab12ccf2a22f0c9962fc1353be414351ca9ec3ede57f215e864fd9eb90385079f1", null ],
    [ "createFrictionPairs", "_phys_x_vehicle_tire_friction_8hpp.html#a441dcd8df5b5f6710485b5e2fe6b3dff", null ]
];