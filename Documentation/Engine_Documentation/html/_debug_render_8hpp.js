var _debug_render_8hpp =
[
    [ "DebugRenderOptionsT", "struct_debug_render_options_t.html", "struct_debug_render_options_t" ],
    [ "DebugRender", "class_debug_render.html", "class_debug_render" ],
    [ "eDebugRenderMode", "_debug_render_8hpp.html#a5e43d297068757ac6b6d0a3d870468c4", [
      [ "DEBUG_RENDER_USE_DEPTH", "_debug_render_8hpp.html#a5e43d297068757ac6b6d0a3d870468c4a0e77a26f8c3b165b43797261649e1015", null ],
      [ "DEBUG_RENDER_ALWAYS", "_debug_render_8hpp.html#a5e43d297068757ac6b6d0a3d870468c4aef558ff15dd8bb5776e37f8742930fdf", null ],
      [ "DEBUG_RENDER_XRAY", "_debug_render_8hpp.html#a5e43d297068757ac6b6d0a3d870468c4abbcd025e0af72f868b9ff8ac0a2578dc", null ]
    ] ],
    [ "eDebugRenderSpace", "_debug_render_8hpp.html#a96abb8880db469915e9d28fb27cf30d3", [
      [ "DEBUG_RENDER_SCREEN", "_debug_render_8hpp.html#a96abb8880db469915e9d28fb27cf30d3a5231edfc7a7519def8225c09b3f37d41", null ],
      [ "DEBUG_RENDER_WORLD", "_debug_render_8hpp.html#a96abb8880db469915e9d28fb27cf30d3a9e9ef45215d4e653c7a8084ebf75061b", null ]
    ] ]
];