var _material_8hpp =
[
    [ "Material", "class_material.html", "class_material" ],
    [ "eTextureSlots", "_material_8hpp.html#a333402f9d1695d41903ed561bf4e4b2e", [
      [ "DIFFUSE_SLOT", "_material_8hpp.html#a333402f9d1695d41903ed561bf4e4b2eae66bc7db86132f2f7b880b7589e2bd5a", null ],
      [ "NORMAL_SLOT", "_material_8hpp.html#a333402f9d1695d41903ed561bf4e4b2eab0bf29cc56463c39a8163e29698fa359", null ],
      [ "EMISSIVE_SLOT", "_material_8hpp.html#a333402f9d1695d41903ed561bf4e4b2ea425d6c1602b8176e44ed7157c054edbc", null ],
      [ "SPECULAR_SLOT", "_material_8hpp.html#a333402f9d1695d41903ed561bf4e4b2eab4d69f744904a1265ddb0c7fb125d0ea", null ]
    ] ]
];