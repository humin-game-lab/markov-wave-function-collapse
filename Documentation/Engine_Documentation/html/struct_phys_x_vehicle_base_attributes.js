var struct_phys_x_vehicle_base_attributes =
[
    [ "SetChassisCMOffset", "struct_phys_x_vehicle_base_attributes.html#ae6af68001cebe44aec2ce6b23d176e44", null ],
    [ "SetChassisMOI", "struct_phys_x_vehicle_base_attributes.html#ac7769c96a807666158db6096d3afa29f", null ],
    [ "SetWheelMOI", "struct_phys_x_vehicle_base_attributes.html#a836bcde941dd3c949d82a4f6ec63be0a", null ],
    [ "m_chassisCMOffset", "struct_phys_x_vehicle_base_attributes.html#a3b5fc24b752ec88f42cf3deede598444", null ],
    [ "m_chassisDims", "struct_phys_x_vehicle_base_attributes.html#a504bccd2bfa1f9116419716779801244", null ],
    [ "m_chassisMass", "struct_phys_x_vehicle_base_attributes.html#a3e6d10d1f870e46008a4f780df98917d", null ],
    [ "m_chassisMOI", "struct_phys_x_vehicle_base_attributes.html#a38b9f33c96aaeb3d0b0518b2282b7403", null ],
    [ "m_nbWheels", "struct_phys_x_vehicle_base_attributes.html#a13f5da6c5ffd87da12f63cafc1684784", null ],
    [ "m_wheelMass", "struct_phys_x_vehicle_base_attributes.html#acd9a89c743351bf09f063f9421eb094f", null ],
    [ "m_wheelMOI", "struct_phys_x_vehicle_base_attributes.html#a9a94e5f0b071aa78ca77e36231565985", null ],
    [ "m_wheelRadius", "struct_phys_x_vehicle_base_attributes.html#a33834cd41323c968c333a8b8240b2121", null ],
    [ "m_wheelWidth", "struct_phys_x_vehicle_base_attributes.html#ae18f1ca26d535647ea4fb48923fd9427", null ]
];