var descrobject_8h =
[
    [ "PyGetSetDef", "struct_py_get_set_def.html", "struct_py_get_set_def" ],
    [ "wrapperbase", "structwrapperbase.html", "structwrapperbase" ],
    [ "PyDescrObject", "struct_py_descr_object.html", "struct_py_descr_object" ],
    [ "PyMethodDescrObject", "struct_py_method_descr_object.html", "struct_py_method_descr_object" ],
    [ "PyMemberDescrObject", "struct_py_member_descr_object.html", "struct_py_member_descr_object" ],
    [ "PyGetSetDescrObject", "struct_py_get_set_descr_object.html", "struct_py_get_set_descr_object" ],
    [ "PyWrapperDescrObject", "struct_py_wrapper_descr_object.html", "struct_py_wrapper_descr_object" ],
    [ "PyDescr_COMMON", "descrobject_8h.html#ad6657abf0a8b1122cdcd1c1d9625a65d", null ],
    [ "PyDescr_IsData", "descrobject_8h.html#a67772c19d805c1c792b7075ccb4f81cb", null ],
    [ "PyDescr_NAME", "descrobject_8h.html#a7b007021fc924eb74eccb0bb05d99ad1", null ],
    [ "PyDescr_TYPE", "descrobject_8h.html#a0d2bc679367576eb4071b8c3bfe13edc", null ],
    [ "PyWrapperFlag_KEYWORDS", "descrobject_8h.html#a807a348a304599d36e358aeaa19d4731", null ],
    [ "getter", "descrobject_8h.html#ac44beb2d92298f4cdf6b605b6d9ecb25", null ],
    [ "PyGetSetDef", "descrobject_8h.html#a309018ba76a0008429bc88d566f2559a", null ],
    [ "setter", "descrobject_8h.html#a92c8202514fe030a3d6b691da347af29", null ],
    [ "wrapperfunc", "descrobject_8h.html#aa0427daa24e34913e3ab0a4cd794ee9b", null ],
    [ "wrapperfunc_kwds", "descrobject_8h.html#ad3e5330d306a8e4f7f9bc410d207d085", null ],
    [ "PyAPI_DATA", "descrobject_8h.html#a98e9af98b169859293051b7898afa189", null ],
    [ "PyAPI_FUNC", "descrobject_8h.html#a7012edcd95055cf2fba9f8e2e84da920", null ],
    [ "kwnames", "descrobject_8h.html#adc4cbcd1a279235b18b8250cd00d8941", null ],
    [ "nargs", "descrobject_8h.html#a8c8299938e1c937fe8f5872ee521b549", null ],
    [ "stack", "descrobject_8h.html#a4de2e38133c7a6bdc2251ceeb5577c99", null ]
];