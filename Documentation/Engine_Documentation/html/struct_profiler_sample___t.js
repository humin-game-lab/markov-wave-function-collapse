var struct_profiler_sample___t =
[
    [ "AddChild", "struct_profiler_sample___t.html#a65199fda6aad9ae32f73ba7626d1a942", null ],
    [ "m_allocationSizeInBytes", "struct_profiler_sample___t.html#a747ce16a33828e3b6d3f976a894477c3", null ],
    [ "m_allocCount", "struct_profiler_sample___t.html#acc611f61b637dbe35aaa25ef05726f62", null ],
    [ "m_endTime", "struct_profiler_sample___t.html#a08c4aa79a35397dd6f3714ef28986d56", null ],
    [ "m_freeCount", "struct_profiler_sample___t.html#a4e16fe3225a9c9f3cd7c5822aa4ac674", null ],
    [ "m_freeSizeInBytes", "struct_profiler_sample___t.html#a37a197c1db5d1fe3ff7b07d88c7e572f", null ],
    [ "m_label", "struct_profiler_sample___t.html#ab5124c4a2b07072ab839b6c3a3afc5aa", null ],
    [ "m_lastChild", "struct_profiler_sample___t.html#ade7a808cb195200cf1b60aa27a5e8a89", null ],
    [ "m_parent", "struct_profiler_sample___t.html#a795309e050d87c0669571bea3b3f7837", null ],
    [ "m_prevSibling", "struct_profiler_sample___t.html#a31ab931dc8f0f0fe1cc0fadccb921e60", null ],
    [ "m_refCount", "struct_profiler_sample___t.html#a416f324dca372052aaf99f8e584c63ab", null ],
    [ "m_startTime", "struct_profiler_sample___t.html#a00b1fdeda72cfbd385e159738264804f", null ],
    [ "m_threadID", "struct_profiler_sample___t.html#ab5f026ee1f080ad6322917292bc47930", null ]
];