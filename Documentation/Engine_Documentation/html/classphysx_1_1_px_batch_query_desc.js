var classphysx_1_1_px_batch_query_desc =
[
    [ "PxBatchQueryDesc", "classphysx_1_1_px_batch_query_desc.html#aa49c9bba3307aae5e4b3f9a5fc9c4e2b", null ],
    [ "isValid", "classphysx_1_1_px_batch_query_desc.html#aa0808d927a52a2def48666e4b32dbdb9", null ],
    [ "filterShaderData", "classphysx_1_1_px_batch_query_desc.html#a1d3d637739e9bb6f87b7fa609a870696", null ],
    [ "filterShaderDataSize", "classphysx_1_1_px_batch_query_desc.html#a381e64e31725aeb637da2678a40e0f65", null ],
    [ "postFilterShader", "classphysx_1_1_px_batch_query_desc.html#afb3b3c2f86ec4dd148c48b0376f06af5", null ],
    [ "preFilterShader", "classphysx_1_1_px_batch_query_desc.html#a9863d6264f5b0fcc3f4c8e09d39314e8", null ],
    [ "queryMemory", "classphysx_1_1_px_batch_query_desc.html#ab1b96e4c6f4f686eb4138cae47e2e234", null ]
];