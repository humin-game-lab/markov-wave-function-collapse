var class_render_buffer =
[
    [ "RenderBuffer", "class_render_buffer.html#a750d7013dd9bb5f04854af8f99e8a0d4", null ],
    [ "~RenderBuffer", "class_render_buffer.html#a19e7ab6e01523fffea6d5415510889d6", null ],
    [ "CopyCPUToGPU", "class_render_buffer.html#aec4f6e014ba39e6bd9ab4577dac89075", null ],
    [ "CreateBuffer", "class_render_buffer.html#ab3784556fca4c9990e13b776b60a5a44", null ],
    [ "GetSize", "class_render_buffer.html#ac93a745563a6c3c9579b9d74e196aae3", null ],
    [ "IsDynamic", "class_render_buffer.html#a1a907d0e36ee0fa53d5586980bf9346a", null ],
    [ "IsStatic", "class_render_buffer.html#ab34a584faef0cc04b65aa4559686b8d2", null ],
    [ "UniformBuffer", "class_render_buffer.html#a6b98e21b377a82f5f2bb427cb547ba9f", null ],
    [ "m_bufferSize", "class_render_buffer.html#aa72985d4c48b9b89b6c17775b5b2b293", null ],
    [ "m_bufferUsage", "class_render_buffer.html#acb2f02717f07dd720663d7b63f077bd6", null ],
    [ "m_elementSize", "class_render_buffer.html#af3d44837491998804d776f9b016815b1", null ],
    [ "m_handle", "class_render_buffer.html#a8c76e23b8dc2b6f17b07611c3f722f2d", null ],
    [ "m_memoryUsage", "class_render_buffer.html#a10eee09c53535f25f4c583d10c9b38e3", null ],
    [ "m_owningRenderContext", "class_render_buffer.html#af59cb56616c1bb1f504697d29756327e", null ]
];