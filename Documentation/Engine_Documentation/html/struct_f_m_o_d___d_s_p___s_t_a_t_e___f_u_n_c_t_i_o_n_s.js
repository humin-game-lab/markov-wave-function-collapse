var struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s =
[
    [ "alloc", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#a0b9708923c5cda2ae8f86be00058efd3", null ],
    [ "dft", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#afce6ccff50f94e42e4a063e5f1e7bad1", null ],
    [ "free", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#a3f4f99382ef4311bf3f8f1c6ab327aa6", null ],
    [ "getblocksize", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#ae315fe02d6e6b960caac6f760fc60e8b", null ],
    [ "getclock", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#a530f7914a9b19f04ea7842df9fe96264", null ],
    [ "getlistenerattributes", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#a45fbaa1aec82408a7a2664e380fbe491", null ],
    [ "getsamplerate", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#af6857eb3e2a94b3e5071593f87e611dd", null ],
    [ "getspeakermode", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#a39054ca8099a6a01a27b58db39a475a5", null ],
    [ "getuserdata", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#af14008496ac6a0b0f64f9f6e5b046b95", null ],
    [ "log", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#a17c9426031d2f5ef3400ae54294a509a", null ],
    [ "pan", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#a65719dd7581083df251076e16331bd72", null ],
    [ "realloc", "struct_f_m_o_d___d_s_p___s_t_a_t_e___f_u_n_c_t_i_o_n_s.html#a03e7097b5f380a2b0acac7f0a4412e9d", null ]
];