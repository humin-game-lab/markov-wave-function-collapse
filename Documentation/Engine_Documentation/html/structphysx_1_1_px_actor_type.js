var structphysx_1_1_px_actor_type =
[
    [ "Enum", "structphysx_1_1_px_actor_type.html#aa12d1dbb97bda016f592de47e67e3963", [
      [ "eRIGID_STATIC", "structphysx_1_1_px_actor_type.html#aa12d1dbb97bda016f592de47e67e3963af6135b2f8b765c8bbaf46fb3c8394de7", null ],
      [ "eRIGID_DYNAMIC", "structphysx_1_1_px_actor_type.html#aa12d1dbb97bda016f592de47e67e3963afeb0cea276df187f0fa50ca46ec53c10", null ],
      [ "eARTICULATION_LINK", "structphysx_1_1_px_actor_type.html#aa12d1dbb97bda016f592de47e67e3963a593e5578937ce90984a99510f1ee0a31", null ],
      [ "eACTOR_COUNT", "structphysx_1_1_px_actor_type.html#aa12d1dbb97bda016f592de47e67e3963a36be26992b9dc4279e70c23b2acd5f65", null ],
      [ "eACTOR_FORCE_DWORD", "structphysx_1_1_px_actor_type.html#aa12d1dbb97bda016f592de47e67e3963a9b782947b14d3dcea760b9e7576eea84", null ]
    ] ]
];