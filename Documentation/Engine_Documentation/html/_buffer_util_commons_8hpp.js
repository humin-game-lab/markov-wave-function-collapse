var _buffer_util_commons_8hpp =
[
    [ "PLATFORM_IS_BIG_ENDIAN", "_buffer_util_commons_8hpp.html#a72dfe18fa345b1e5882c3e9c42e56c42", null ],
    [ "PLATFORM_IS_LITTLE_ENDIAN", "_buffer_util_commons_8hpp.html#a70cdc2961c4e178e613d23a851e57f5d", null ],
    [ "Buffer", "_buffer_util_commons_8hpp.html#a8f502862992b828150656db48c9ce5fc", null ],
    [ "eBufferEndianness", "_buffer_util_commons_8hpp.html#acf99b6968d48ab6cfb4334384f7b275e", [
      [ "BUFFER_NATIVE", "_buffer_util_commons_8hpp.html#acf99b6968d48ab6cfb4334384f7b275ea0d2c0f602f99be8342f4cd2405c0e266", null ],
      [ "BUFFER_LITTLE_ENDIAN", "_buffer_util_commons_8hpp.html#acf99b6968d48ab6cfb4334384f7b275eaeb883ea2ba751dba4dd79ce8e9470a2a", null ],
      [ "BUFFER_BIG_ENDIAN", "_buffer_util_commons_8hpp.html#acf99b6968d48ab6cfb4334384f7b275ea890e3c9d4ad2e43b9037fce487ad4ae7", null ]
    ] ],
    [ "Reverse2BytesInPlace", "_buffer_util_commons_8hpp.html#a887c6f8340454422e2c2b9abdebae459", null ],
    [ "Reverse4BytesInPlace", "_buffer_util_commons_8hpp.html#a4758dd2c265a40b3ff8c29a319a765ff", null ],
    [ "Reverse8BytesInPlace", "_buffer_util_commons_8hpp.html#a867f4b30084f60ecb6c4ffedb98a1b60", null ],
    [ "ReverseBytesInPlacePtrSizeT", "_buffer_util_commons_8hpp.html#a9e189b3473d159d4cac097b8ddbb75a8", null ]
];