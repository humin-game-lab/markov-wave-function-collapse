var class_texture =
[
    [ "Texture", "class_texture.html#a1c26ed37a85ca40269fc557875b2f7b1", null ],
    [ "~Texture", "class_texture.html#a09c4bcb7462f64c1d20fa69dba3cee8a", null ],
    [ "CreateTextureView", "class_texture.html#a5b0a48c2f81c3bd1909b3caa5cf2e56b", null ],
    [ "FreeHandles", "class_texture.html#a98fd5ac271d0da605f5e772005a80ee6", null ],
    [ "GetTextureID", "class_texture.html#a4ef2b75b6377be7d4115a31f5584ab81", null ],
    [ "RenderContext", "class_texture.html#adbc71e71bb875e994e20b698201b22a5", null ],
    [ "m_dimensions", "class_texture.html#a37bd945d5eba272e2b3792a71aad781b", null ],
    [ "m_handle", "class_texture.html#a566ff649da5f7684b0e88d1ebeae3636", null ],
    [ "m_memoryUsage", "class_texture.html#a70b7cb17a432a8ae89497a212a34c65a", null ],
    [ "m_owner", "class_texture.html#ad8a838ca3ec719f7369d359c49c8a7ca", null ],
    [ "m_textureUsage", "class_texture.html#a1c9f98e645a59f49a542d9402beb1882", null ]
];