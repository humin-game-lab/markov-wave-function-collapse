var structphysx_1_1_px_t_g_s_solver_contact_desc =
[
    [ "axisConstraintCount", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#a412b64149ccedeb582d10a709855cb20", null ],
    [ "contactForces", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#ac78da16e2c4bfc684ac1f698c9b63d54", null ],
    [ "contacts", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#ae8fe21964a5d9878b5361c331b1eeaf4", null ],
    [ "disableStrongFriction", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#a7c3670e70cd8ec4fc35cf17d92c7b3b0", null ],
    [ "frictionCount", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#aa7d9e9295738e3860c3e459d7c83ee10", null ],
    [ "frictionPtr", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#abb66a5cec7910c796abc280d28ee3d70", null ],
    [ "hasForceThresholds", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#ad5d033c5ee53e0fb3054b789a5c00c51", null ],
    [ "hasMaxImpulse", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#a7755864d49b00ed87c5ed8120b794593", null ],
    [ "maxCCDSeparation", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#acfbf9b2eb503c9676ced87792e22f103", null ],
    [ "maxImpulse", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#a01ed6abb3641362d655fa9517656a9c9", null ],
    [ "minTorsionalPatchRadius", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#a21be73df6012622d60e0a824c6d09e9f", null ],
    [ "numContactPatches", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#affdca5e6285eca4cc98fb372391e72d4", null ],
    [ "numContacts", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#a9a2cf0082b2d75a6dd76d1c4a9e2b113", null ],
    [ "numFrictionPatches", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#ac4ff5c19b8b17fd21778fdcfd9bc4bf9", null ],
    [ "restDistance", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#a12739a9c9bea0315626d0dbb910abf78", null ],
    [ "shapeInteraction", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#a93622cbf8ddd38220f409c5edbc0010f", null ],
    [ "startContactPatchIndex", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#a85ba73c5573975a6aadae3cbb89c893d", null ],
    [ "startFrictionPatchIndex", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#aa333e6af7de106816922f63a2f7c1ecf", null ],
    [ "torsionalPatchRadius", "structphysx_1_1_px_t_g_s_solver_contact_desc.html#a29e2e868c595e79410e7ebca21b85047", null ]
];