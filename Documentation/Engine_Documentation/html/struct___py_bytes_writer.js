var struct___py_bytes_writer =
[
    [ "allocated", "struct___py_bytes_writer.html#acd4f527c2441f162eb2be714871ae368", null ],
    [ "buffer", "struct___py_bytes_writer.html#a4f63cd9da46d5e94f656db117886c1d4", null ],
    [ "min_size", "struct___py_bytes_writer.html#ad4a9d1df7565425866b42047204f1eef", null ],
    [ "overallocate", "struct___py_bytes_writer.html#aa2a6b5841df61f34f1c67b14ee97e61a", null ],
    [ "small_buffer", "struct___py_bytes_writer.html#a283280dd6592ccc7169fa7ab47bca4c7", null ],
    [ "use_bytearray", "struct___py_bytes_writer.html#a20e4cc461166ff8baba60f5bb4c9131f", null ],
    [ "use_small_buffer", "struct___py_bytes_writer.html#a15e02050e33dd6f283cdf72edc56c22f", null ]
];