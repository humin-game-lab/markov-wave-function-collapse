var class_sprite_defenition =
[
    [ "SpriteDefenition", "class_sprite_defenition.html#ae9164cfdf950a3aeddb28c9f08508d24", null ],
    [ "SpriteDefenition", "class_sprite_defenition.html#aacce27efaa91838f5d05b11320ae0049", null ],
    [ "~SpriteDefenition", "class_sprite_defenition.html#aba663be13981edb21d0631f7c763d39b", null ],
    [ "GetPivot", "class_sprite_defenition.html#a75f7b80889fa4d329cc6884563a5461b", null ],
    [ "GetTexture", "class_sprite_defenition.html#ab2dfc7a5f087b776a671cd06b7275918", null ],
    [ "GetUVs", "class_sprite_defenition.html#a11b5b3876c28d6b8815b6163d8297cc3", null ],
    [ "SetPivot", "class_sprite_defenition.html#aa477ece76bbc37da729e8b289792c456", null ],
    [ "SetUVs", "class_sprite_defenition.html#af50ffdff17e99e65b205db0a0e89c022", null ]
];