var struct_s_tri_info =
[
    [ "AssignedGroup", "struct_s_tri_info.html#a1523db21019cc92771f597b0e9a262e3", null ],
    [ "FaceNeighbors", "struct_s_tri_info.html#af1abc839ec82ee2b9d97628fd1552aeb", null ],
    [ "fMagS", "struct_s_tri_info.html#a9676744ebb5e88e67627a804ee565e30", null ],
    [ "fMagT", "struct_s_tri_info.html#aebb924e8ff55d43e9b9434bc159aa49d", null ],
    [ "iFlag", "struct_s_tri_info.html#adb6a99e53e7d4375bf474d0d30d9b082", null ],
    [ "iOrgFaceNumber", "struct_s_tri_info.html#aa10d0c9d22a6d524f0e567aa48598a40", null ],
    [ "iTSpacesOffs", "struct_s_tri_info.html#aa0731183c236bd8732e8d89fd0192f59", null ],
    [ "vert_num", "struct_s_tri_info.html#a9d586c901c5245d86d80f0b747f54081", null ],
    [ "vOs", "struct_s_tri_info.html#a23f6899281074dbb44811994bccbd7f5", null ],
    [ "vOt", "struct_s_tri_info.html#a02be7d576b6f5ea30969805e167ba3c1", null ]
];