var struct_light_t =
[
    [ "color", "struct_light_t.html#a85cdc571dfd9a25d9c40318807bc93a0", null ],
    [ "diffuseAttenuation", "struct_light_t.html#aaafa891440d897a6d7badf2718b678d6", null ],
    [ "direction", "struct_light_t.html#ac2e0549cefd0ae71e273d3b862d0b154", null ],
    [ "isDirection", "struct_light_t.html#a36b67a3abfb73f73c2ee4d0ff8aaf2bb", null ],
    [ "pad00", "struct_light_t.html#a8543e34ab26470166696c7bf02817111", null ],
    [ "pad10", "struct_light_t.html#a56355c87d7c301f7bff8db5a9edab49c", null ],
    [ "pad20", "struct_light_t.html#a94b15aedc3bc16effba23569e41e728d", null ],
    [ "position", "struct_light_t.html#a626ad109955f6532c10cda3c9b0f8f9a", null ],
    [ "specularAttenuation", "struct_light_t.html#a7fb15ced70addba22590249484b2958e", null ]
];