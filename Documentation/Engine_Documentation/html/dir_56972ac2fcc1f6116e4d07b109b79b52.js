var dir_56972ac2fcc1f6116e4d07b109b79b52 =
[
    [ "Job.cpp", "_job_8cpp.html", null ],
    [ "Job.hpp", "_job_8hpp.html", [
      [ "Job", "class_job.html", "class_job" ]
    ] ],
    [ "JobCategory.cpp", "_job_category_8cpp.html", null ],
    [ "JobCategory.hpp", "_job_category_8hpp.html", [
      [ "JobCategory", "class_job_category.html", "class_job_category" ]
    ] ],
    [ "JobSystem.cpp", "_job_system_8cpp.html", "_job_system_8cpp" ],
    [ "JobSystem.hpp", "_job_system_8hpp.html", "_job_system_8hpp" ],
    [ "JobTypes.hpp", "_job_types_8hpp.html", "_job_types_8hpp" ],
    [ "MadleBrotJob.cpp", "_madle_brot_job_8cpp.html", null ],
    [ "MadleBrotJob.hpp", "_madle_brot_job_8hpp.html", [
      [ "MandleBrotJob", "class_mandle_brot_job.html", "class_mandle_brot_job" ],
      [ "UpdateTextureRowJob", "class_update_texture_row_job.html", "class_update_texture_row_job" ]
    ] ],
    [ "ScreenShotJob.cpp", "_screen_shot_job_8cpp.html", null ],
    [ "ScreenShotJob.hpp", "_screen_shot_job_8hpp.html", [
      [ "ScreenShotJob", "class_screen_shot_job.html", "class_screen_shot_job" ]
    ] ],
    [ "WriteImageToFileJob.hpp", "_write_image_to_file_job_8hpp.html", [
      [ "WriteImageToFileJob", "class_write_image_to_file_job.html", "class_write_image_to_file_job" ]
    ] ]
];