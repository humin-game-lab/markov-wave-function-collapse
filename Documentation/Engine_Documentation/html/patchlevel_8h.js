var patchlevel_8h =
[
    [ "PY_MAJOR_VERSION", "patchlevel_8h.html#ad5f6136575682abd82ac34babb4e1b48", null ],
    [ "PY_MICRO_VERSION", "patchlevel_8h.html#a81dee48e13f97f0a690110afddcbaea8", null ],
    [ "PY_MINOR_VERSION", "patchlevel_8h.html#a0c77faaab3dbe19710f708352b20f226", null ],
    [ "PY_RELEASE_LEVEL", "patchlevel_8h.html#a4c58760362a63ebe61fc04cf6eb78bd3", null ],
    [ "PY_RELEASE_LEVEL_ALPHA", "patchlevel_8h.html#a1ac0d21eb816de74ed58c1f900ceb14a", null ],
    [ "PY_RELEASE_LEVEL_BETA", "patchlevel_8h.html#af24bbf60740a7dba96df444cb625c605", null ],
    [ "PY_RELEASE_LEVEL_FINAL", "patchlevel_8h.html#ae1fd3fdcc3ac37fe0ef0d92434c40edc", null ],
    [ "PY_RELEASE_LEVEL_GAMMA", "patchlevel_8h.html#aa211657faa8445ba59a1f2381cbc2bfa", null ],
    [ "PY_RELEASE_SERIAL", "patchlevel_8h.html#a466dd8ea47c68ae9a26b59b3efb60ee8", null ],
    [ "PY_VERSION", "patchlevel_8h.html#afa284c9dff3dfafb112749ecb35f07a3", null ],
    [ "PY_VERSION_HEX", "patchlevel_8h.html#a8b78c3c8542acafa7b3f246c32b2ee53", null ]
];