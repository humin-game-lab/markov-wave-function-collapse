var structphysx_1_1_px_hit_flag =
[
    [ "Enum", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56e", [
      [ "ePOSITION", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56eaba505ebc5a40e050d5a1bd832a139bf3", null ],
      [ "eNORMAL", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56ea1c637da8662cbbed16d2fd60e9bb4bac", null ],
      [ "eUV", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56ea91b0216af4a48fbd906828ba4d3b34ba", null ],
      [ "eASSUME_NO_INITIAL_OVERLAP", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56ea93b553576a08fb94bafb3748616358d6", null ],
      [ "eMESH_MULTIPLE", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56eaf54e431f890e7c26634325e7dee1373c", null ],
      [ "eMESH_ANY", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56eac590511469b73f922ef8d51738c990cb", null ],
      [ "eMESH_BOTH_SIDES", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56ea63ec713f10d90f3116e8ca8308edeb69", null ],
      [ "ePRECISE_SWEEP", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56eab9afc7e28088e019ffc23f04a50c1f08", null ],
      [ "eMTD", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56eaf0bb41a98d7b8a5304736e5ff34b09ca", null ],
      [ "eFACE_INDEX", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56ea1efe3eaf872c31c332909cd64976087e", null ],
      [ "eDEFAULT", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56eabd4afbb21fb0f95d56165e6a605c6da7", null ],
      [ "eMODIFIABLE_FLAGS", "structphysx_1_1_px_hit_flag.html#af54b32086533131a0b42400f12e6d56ea07428266ded719863aff06c144f2a62c", null ]
    ] ]
];