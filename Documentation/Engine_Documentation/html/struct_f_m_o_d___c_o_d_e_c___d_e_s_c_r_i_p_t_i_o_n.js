var struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n =
[
    [ "close", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#af3804c959e5e4d9e3cf03d4a2eaf7ed8", null ],
    [ "defaultasstream", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#ac79461b38a89c34d2ec2e609736df941", null ],
    [ "getlength", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#a4f7b851d2ec478240bc572f7e4318306", null ],
    [ "getposition", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#aca4b4ae61caf677c7a6fbaed48e024ac", null ],
    [ "getwaveformat", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#ac0c601819e7806447a718b0d595e1189", null ],
    [ "name", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#a3d2d1cb50c8d3fcee03e465753a9adb0", null ],
    [ "open", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#ab4bc946139795ab97b95f8c7cb1e43b9", null ],
    [ "read", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#abc7836a9cbaecca1c4d5f6b318b0370c", null ],
    [ "setposition", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#aa7fd03795bbeb2599cf28aabe6c23bd7", null ],
    [ "soundcreate", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#aec9ca5133371b75986e6fbcbf0b017fc", null ],
    [ "timeunits", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#a17d05f38ea3ea759d20d463aa8a8ca9f", null ],
    [ "version", "struct_f_m_o_d___c_o_d_e_c___d_e_s_c_r_i_p_t_i_o_n.html#a8dfb836ca79931ee6ca39c12bd8aad3f", null ]
];