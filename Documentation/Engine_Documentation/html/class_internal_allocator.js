var class_internal_allocator =
[
    [ "Allocate", "class_internal_allocator.html#a8de6a4cf074bdf7ba0374b8e5e6a7430", null ],
    [ "Create", "class_internal_allocator.html#ab23561ae9b780dc0c5471741e668be21", null ],
    [ "Destroy", "class_internal_allocator.html#a0da9dc75744d842912e5885ac9ff5076", null ],
    [ "Free", "class_internal_allocator.html#a80b8326babc8187f85e29d22ea9f1fb7", null ]
];