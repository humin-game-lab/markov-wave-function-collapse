var struct___py_unicode_writer =
[
    [ "buffer", "struct___py_unicode_writer.html#a5bcc85de8f76d8196df8a3c505308c43", null ],
    [ "data", "struct___py_unicode_writer.html#a76841c44766ba1dd8edac4090dd49719", null ],
    [ "kind", "struct___py_unicode_writer.html#aff601d1b8b3ff02098c054df62519975", null ],
    [ "maxchar", "struct___py_unicode_writer.html#a87830f0cdce1296dde1c74fd1d023b47", null ],
    [ "min_char", "struct___py_unicode_writer.html#ac337475e706576ffc96d46015570f52c", null ],
    [ "min_length", "struct___py_unicode_writer.html#aba90b9400b7eac0c23e9bb91afa88d27", null ],
    [ "overallocate", "struct___py_unicode_writer.html#a8568e42ffbeaa7587bc59d0a47b899ba", null ],
    [ "pos", "struct___py_unicode_writer.html#ae0784032b2a208e5330cd5cfd462b378", null ],
    [ "readonly", "struct___py_unicode_writer.html#a3db581c84815a5c32279e117d958a72e", null ],
    [ "size", "struct___py_unicode_writer.html#a85019c080568bc2e8b2be77bea1a3272", null ]
];