var _physics_types_8hpp =
[
    [ "eSimulationType", "_physics_types_8hpp.html#ae50738196e8b5bf3bd690095734575b8", [
      [ "TYPE_UNKOWN", "_physics_types_8hpp.html#ae50738196e8b5bf3bd690095734575b8a364078429032d44717bd58bcf0a03d7c", null ],
      [ "STATIC_SIMULATION", "_physics_types_8hpp.html#ae50738196e8b5bf3bd690095734575b8a104081a81c4ad64b53531feda149b7c2", null ],
      [ "DYNAMIC_SIMULATION", "_physics_types_8hpp.html#ae50738196e8b5bf3bd690095734575b8aefc651c19272802532de90d05d0361c3", null ],
      [ "NUM_SIMULATION_TYPES", "_physics_types_8hpp.html#ae50738196e8b5bf3bd690095734575b8a6c028e68c2e4757a8569c73935bcd8d9", null ]
    ] ]
];