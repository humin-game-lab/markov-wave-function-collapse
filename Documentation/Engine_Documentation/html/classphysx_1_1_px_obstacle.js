var classphysx_1_1_px_obstacle =
[
    [ "PxObstacle", "classphysx_1_1_px_obstacle.html#a60f44177efb9b3d4371f5cf55de057fa", null ],
    [ "getType", "classphysx_1_1_px_obstacle.html#a9159309f82605a9adfec12520e4b54c4", null ],
    [ "mPos", "classphysx_1_1_px_obstacle.html#a4531f82d613c6a01d2848e46b84b118a", null ],
    [ "mRot", "classphysx_1_1_px_obstacle.html#a2b25eb75fa4eaccea431b77a5a0da3bd", null ],
    [ "mType", "classphysx_1_1_px_obstacle.html#a5b4aac6aeb4045d837b718f3a4a4405d", null ],
    [ "mUserData", "classphysx_1_1_px_obstacle.html#a7318f612f699dc85d7fb012af9847159", null ]
];