var class_analog_joy_stick =
[
    [ "AnalogJoyStick", "class_analog_joy_stick.html#a3416412a81600fafd7b9833dc6a54382", null ],
    [ "GetAngleDegrees", "class_analog_joy_stick.html#ae0bc4178452f2c3cf4f1db40d0dcc8a1", null ],
    [ "GetInnerDeadZoneFraction", "class_analog_joy_stick.html#a625a7e98563ae9f8cc6edc6da3b61758", null ],
    [ "GetMagnitude", "class_analog_joy_stick.html#afdac22f02d901aeca04e832d6c410373", null ],
    [ "GetOuterDeadZoneFraction", "class_analog_joy_stick.html#a2bb04462010426d62e34ad2fde097f89", null ],
    [ "GetPosition", "class_analog_joy_stick.html#a4d706a04d1049b1eba5b0bdd506c45c2", null ],
    [ "GetRawPosition", "class_analog_joy_stick.html#afa336b11a698b9c52fd381afecf8e1ac", null ],
    [ "Reset", "class_analog_joy_stick.html#ac6fa410e7c738e6cc358894b6b8ffd66", null ],
    [ "UpdatePosition", "class_analog_joy_stick.html#a38a80fbe2aaf9db9da62cc3699d857ed", null ],
    [ "XboxController", "class_analog_joy_stick.html#abef57003462d7b0058d40e8fc06d5fe7", null ]
];