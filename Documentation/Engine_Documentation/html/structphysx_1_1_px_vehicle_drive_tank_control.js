var structphysx_1_1_px_vehicle_drive_tank_control =
[
    [ "Enum", "structphysx_1_1_px_vehicle_drive_tank_control.html#a6884c1af645287923b46b472f24ccc7c", [
      [ "eANALOG_INPUT_ACCEL", "structphysx_1_1_px_vehicle_drive_tank_control.html#a6884c1af645287923b46b472f24ccc7ca98d53a5ad97ecec4a1f185a0784088a2", null ],
      [ "eANALOG_INPUT_BRAKE_LEFT", "structphysx_1_1_px_vehicle_drive_tank_control.html#a6884c1af645287923b46b472f24ccc7ca0bea929e6661fb12b3f140f8a937c5a2", null ],
      [ "eANALOG_INPUT_BRAKE_RIGHT", "structphysx_1_1_px_vehicle_drive_tank_control.html#a6884c1af645287923b46b472f24ccc7ca9d9de9fa47231ca686feaa435b4d7a7e", null ],
      [ "eANALOG_INPUT_THRUST_LEFT", "structphysx_1_1_px_vehicle_drive_tank_control.html#a6884c1af645287923b46b472f24ccc7cac9c3aaf37887b53bc617e6e7f6994c03", null ],
      [ "eANALOG_INPUT_THRUST_RIGHT", "structphysx_1_1_px_vehicle_drive_tank_control.html#a6884c1af645287923b46b472f24ccc7ca75a5d79d31b8e4586696e8cd1bed0071", null ],
      [ "eMAX_NB_DRIVETANK_ANALOG_INPUTS", "structphysx_1_1_px_vehicle_drive_tank_control.html#a6884c1af645287923b46b472f24ccc7ca8408bb20ea20cfe3b48d8bd7a336a77e", null ]
    ] ]
];