var classvehicle_1_1_vehicle_scene_query_data =
[
    [ "VehicleSceneQueryData", "classvehicle_1_1_vehicle_scene_query_data.html#ac524cde7345a45cdc953955396d77a71", null ],
    [ "~VehicleSceneQueryData", "classvehicle_1_1_vehicle_scene_query_data.html#a5932e2bfbdc4af002493f8d6ae6f2816", null ],
    [ "free", "classvehicle_1_1_vehicle_scene_query_data.html#aa174154e48d572351b746461c45c9327", null ],
    [ "getQueryResultBufferSize", "classvehicle_1_1_vehicle_scene_query_data.html#acab017437fa3724fd0bb5483b68f9c17", null ],
    [ "getRaycastQueryResultBuffer", "classvehicle_1_1_vehicle_scene_query_data.html#ab5831cd77b40d36942f8001ff48d7454", null ],
    [ "getSweepQueryResultBuffer", "classvehicle_1_1_vehicle_scene_query_data.html#a6417079832d6c19e26ca0dfc7dda4dc1", null ]
];