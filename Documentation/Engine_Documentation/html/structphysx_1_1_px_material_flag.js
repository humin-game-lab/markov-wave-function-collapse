var structphysx_1_1_px_material_flag =
[
    [ "Enum", "structphysx_1_1_px_material_flag.html#abaf435801e383a9ee3baa6e96fc2fd36", [
      [ "eDISABLE_FRICTION", "structphysx_1_1_px_material_flag.html#abaf435801e383a9ee3baa6e96fc2fd36afb139d70549fdf20b4b84ecd5be2e620", null ],
      [ "eDISABLE_STRONG_FRICTION", "structphysx_1_1_px_material_flag.html#abaf435801e383a9ee3baa6e96fc2fd36a3a561cafbed42f04417574b2a171ef7a", null ],
      [ "eIMPROVED_PATCH_FRICTION", "structphysx_1_1_px_material_flag.html#abaf435801e383a9ee3baa6e96fc2fd36afd02743bd49d36c6f7788f81c2df5d4f", null ]
    ] ]
];