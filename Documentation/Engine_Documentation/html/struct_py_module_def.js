var struct_py_module_def =
[
    [ "m_base", "struct_py_module_def.html#a017d219acf20287393795e7d4b29ab63", null ],
    [ "m_clear", "struct_py_module_def.html#a33c586c65ce380d0aeb1680798465435", null ],
    [ "m_doc", "struct_py_module_def.html#a99a9a7746b553b7d404623235ab8e59c", null ],
    [ "m_free", "struct_py_module_def.html#a52995b482648e59bdc9bf5ddf543092e", null ],
    [ "m_methods", "struct_py_module_def.html#a85c882ff2a40531cab2319a9f93e9b0d", null ],
    [ "m_name", "struct_py_module_def.html#aaa4851e8eff2117539c4dab6fa97506e", null ],
    [ "m_size", "struct_py_module_def.html#a975ebd7e8c6582bed62d4e0297d6df2d", null ],
    [ "m_slots", "struct_py_module_def.html#ad1095d072cd3ef6f81a9a06100d804a3", null ],
    [ "m_traverse", "struct_py_module_def.html#a0922503bf8a1e912dffd61ca95607478", null ]
];