var classphysx_1_1_px_box_controller_desc =
[
    [ "PxBoxControllerDesc", "classphysx_1_1_px_box_controller_desc.html#a1741c90125f8cf722fcea6fa3d8e352e", null ],
    [ "~PxBoxControllerDesc", "classphysx_1_1_px_box_controller_desc.html#af6e0408c362b4daf99c8b762a677440d", null ],
    [ "PxBoxControllerDesc", "classphysx_1_1_px_box_controller_desc.html#ad9bce25c572036129b858ef1d19be394", null ],
    [ "copy", "classphysx_1_1_px_box_controller_desc.html#ae71f5aa2e94123598cd2e7c9d7e0c59b", null ],
    [ "isValid", "classphysx_1_1_px_box_controller_desc.html#a5f7cdbc6ed338d1ca0c732cfc7965069", null ],
    [ "operator=", "classphysx_1_1_px_box_controller_desc.html#aa86d5f76fe6eb7482cf6781efaae5c1b", null ],
    [ "setToDefault", "classphysx_1_1_px_box_controller_desc.html#a000beaa4bf69ca1f2c5e633b4bd3f7a7", null ],
    [ "halfForwardExtent", "classphysx_1_1_px_box_controller_desc.html#acfe5423bb0b869398f4c534aa668750b", null ],
    [ "halfHeight", "classphysx_1_1_px_box_controller_desc.html#aa91c683422288718c003812af0b46ca1", null ],
    [ "halfSideExtent", "classphysx_1_1_px_box_controller_desc.html#a7ca8c16c5ff8c54623edd6c62cc93663", null ]
];