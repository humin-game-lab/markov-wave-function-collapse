var _px_serial_framework_8h =
[
    [ "PxProcessPxBaseCallback", "classphysx_1_1_px_process_px_base_callback.html", "classphysx_1_1_px_process_px_base_callback" ],
    [ "PxSerializationContext", "classphysx_1_1_px_serialization_context.html", "classphysx_1_1_px_serialization_context" ],
    [ "PxDeserializationContext", "classphysx_1_1_px_deserialization_context.html", "classphysx_1_1_px_deserialization_context" ],
    [ "PxSerializationRegistry", "classphysx_1_1_px_serialization_registry.html", "classphysx_1_1_px_serialization_registry" ],
    [ "PX_MAKE_FOURCC", "_px_serial_framework_8h.html#abbba9e3f699e63445326263daec187e8", null ],
    [ "PX_SERIAL_ALIGN", "_px_serial_framework_8h.html#a2fda99afe264006a8ef8a699aa1328aa", null ],
    [ "PX_SERIAL_FILE_ALIGN", "_px_serial_framework_8h.html#a8ca0e9fc5693e347b3ee5735fc637fc2", null ],
    [ "PX_SERIAL_OBJECT_ID_INVALID", "_px_serial_framework_8h.html#a70054743832670870a1ce9f619907c77", null ],
    [ "PX_SERIAL_REF_KIND_MATERIAL_IDX", "_px_serial_framework_8h.html#a27914dcd6d0448671eebfeca012cd0b5", null ],
    [ "PX_SERIAL_REF_KIND_PTR_TYPE_BIT", "_px_serial_framework_8h.html#aa59252c69aa987bd926da9bbf5092367", null ],
    [ "PX_SERIAL_REF_KIND_PXBASE", "_px_serial_framework_8h.html#a8cd51251f2e11c3b6598b96f32e400f8", null ],
    [ "PxBinaryMetaDataCallback", "_px_serial_framework_8h.html#a72e5639d9da5530895a95fc6173da9ea", null ],
    [ "PxSerialObjectId", "_px_serial_framework_8h.html#a698d52cb9f2c5f0a869c2a08cc0d4be2", null ]
];