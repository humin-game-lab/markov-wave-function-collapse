var structvehicle_1_1_vehicle_desc =
[
    [ "VehicleDesc", "structvehicle_1_1_vehicle_desc.html#aa0855d319b0c5bcff636ae7cf1357890", null ],
    [ "actorUserData", "structvehicle_1_1_vehicle_desc.html#aa983a9d9b662a524c7c8e651e35ab1e3", null ],
    [ "chassisCMOffset", "structvehicle_1_1_vehicle_desc.html#a269ed6616921a07a70b2faa1fba1a01d", null ],
    [ "chassisDims", "structvehicle_1_1_vehicle_desc.html#a3a9ff9aba74c432029a521a1dec346ab", null ],
    [ "chassisMass", "structvehicle_1_1_vehicle_desc.html#a9d5336721bbe51b8828877a8bca3057a", null ],
    [ "chassisMaterial", "structvehicle_1_1_vehicle_desc.html#a3c64003771965619d2b5de5f5d624869", null ],
    [ "chassisMOI", "structvehicle_1_1_vehicle_desc.html#ad1cb14eb2e6f41f394d3f80e0b3c9224", null ],
    [ "chassisSimFilterData", "structvehicle_1_1_vehicle_desc.html#afc839b7b7910ff343ae2dccc9013fcdb", null ],
    [ "numWheels", "structvehicle_1_1_vehicle_desc.html#ada790186e194def4fe8375fcd18c6cb5", null ],
    [ "shapeUserDatas", "structvehicle_1_1_vehicle_desc.html#a1fd9dac6acf78bf42f162dc2b857e6a3", null ],
    [ "wheelMass", "structvehicle_1_1_vehicle_desc.html#a304329cb8c65599af30b4e0c6dada440", null ],
    [ "wheelMaterial", "structvehicle_1_1_vehicle_desc.html#ae15f1644b77440f7cf0a67369889ac7e", null ],
    [ "wheelMOI", "structvehicle_1_1_vehicle_desc.html#affd709da37fe34ac17e92c4953235936", null ],
    [ "wheelRadius", "structvehicle_1_1_vehicle_desc.html#a84e6944ba44d414901480d1857fb18da", null ],
    [ "wheelSimFilterData", "structvehicle_1_1_vehicle_desc.html#a763f03f09befc1ec3e7e969fc1b380bd", null ],
    [ "wheelWidth", "structvehicle_1_1_vehicle_desc.html#a84bc6233b1c84cf1de3e0e079742ddc0", null ]
];