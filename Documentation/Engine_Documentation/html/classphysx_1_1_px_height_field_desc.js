var classphysx_1_1_px_height_field_desc =
[
    [ "PxHeightFieldDesc", "classphysx_1_1_px_height_field_desc.html#a4cf7633adb266765fe0ceb99c2b17dce", null ],
    [ "isValid", "classphysx_1_1_px_height_field_desc.html#aaa380333a88f586c29fb4fa4f6004774", null ],
    [ "setToDefault", "classphysx_1_1_px_height_field_desc.html#ae5bdf1bbfb9d841d8da70e827f3ec46d", null ],
    [ "convexEdgeThreshold", "classphysx_1_1_px_height_field_desc.html#aff2d1fec433f35bc9e786dcb2f43df32", null ],
    [ "flags", "classphysx_1_1_px_height_field_desc.html#aebadf0e4a7d1ad7e62a1d2c9fe47caa3", null ],
    [ "format", "classphysx_1_1_px_height_field_desc.html#afe44492882567dbd8c58c3ecfbb8b116", null ],
    [ "nbColumns", "classphysx_1_1_px_height_field_desc.html#a306db167bbd3ba8f16db9bcfe788a049", null ],
    [ "nbRows", "classphysx_1_1_px_height_field_desc.html#aa447202ce5534753551bb8bf63f90b78", null ],
    [ "samples", "classphysx_1_1_px_height_field_desc.html#ace1d1f933550fb592919f2f6f6d80023", null ]
];