var class_clock =
[
    [ "Clock", "class_clock.html#adbc370eb6b5f8d01645cf440188160a8", null ],
    [ "Clock", "class_clock.html#a8ec012b3828e039d26717c59c437f8b5", null ],
    [ "~Clock", "class_clock.html#afc976ce68fa85e15cc06f9ed47bddb7c", null ],
    [ "Dilate", "class_clock.html#a85149e0dfd338ab73e90c56b2c88b19a", null ],
    [ "ForcePause", "class_clock.html#a66d2adbf724038e7b2f9bee9372f3682", null ],
    [ "ForceResume", "class_clock.html#a4bfe62629e5ff2b223c2720f5915ec63", null ],
    [ "ForceStep", "class_clock.html#a0849010f7b70c5d58bf97270b191726b", null ],
    [ "GetFrameTime", "class_clock.html#ae89795816afe2506d988812d90e2df4e", null ],
    [ "IsPaused", "class_clock.html#a5407bede0bc49300b0b46458d0afceb0", null ],
    [ "Pause", "class_clock.html#adec6529c8f0fa8e9e848ef46d4c087e8", null ],
    [ "Resume", "class_clock.html#aef881ddb88e4d9ff9ba29c157d91b3be", null ],
    [ "SetFrameLimit", "class_clock.html#a0c9b71aee2b327ffcc91c8c13dc80ffa", null ],
    [ "SetParent", "class_clock.html#a15617c7aabe0797c6ff9b92cd38b0990", null ],
    [ "Step", "class_clock.html#ad287b558940b7024bf58e9001f48e89c", null ]
];