var pytime_8h =
[
    [ "_Py_clock_info_t", "struct___py__clock__info__t.html", "struct___py__clock__info__t" ],
    [ "_PYTIME_FROMSECONDS", "pytime_8h.html#ad9aefcf6762cb07ea4130e6cdef71228", null ],
    [ "_PyTime_MAX", "pytime_8h.html#a774b27906288ff859ed7eb7b86843c67", null ],
    [ "_PyTime_MIN", "pytime_8h.html#a74dd8f25d61a0cb67d437af57216e95f", null ],
    [ "_PyTime_t", "pytime_8h.html#adadbab23f0263d23f524274de48c34de", null ],
    [ "_PyTime_round_t", "pytime_8h.html#a7543c6b628da31e8f28d7e0e0a8988a2", [
      [ "_PyTime_ROUND_FLOOR", "pytime_8h.html#a7543c6b628da31e8f28d7e0e0a8988a2ad0e98d1b5924f57180034ec60fcd72b1", null ],
      [ "_PyTime_ROUND_CEILING", "pytime_8h.html#a7543c6b628da31e8f28d7e0e0a8988a2a8fa6573b6630b0b536fb26d0b9d881ca", null ],
      [ "_PyTime_ROUND_HALF_EVEN", "pytime_8h.html#a7543c6b628da31e8f28d7e0e0a8988a2ab39dc9e215ced39e9fd7584966d4406e", null ],
      [ "_PyTime_ROUND_UP", "pytime_8h.html#a7543c6b628da31e8f28d7e0e0a8988a2a87b4081a13e24f1d3fb0b3484b4aa259", null ],
      [ "_PyTime_ROUND_TIMEOUT", "pytime_8h.html#a7543c6b628da31e8f28d7e0e0a8988a2ad853b82e6d65b1e121dfee48ea4cdbc2", null ]
    ] ],
    [ "PyAPI_FUNC", "pytime_8h.html#ae235a26f0172cdea212df83c5aaeba74", null ],
    [ "PyAPI_FUNC", "pytime_8h.html#a393f767278a3f18bbbf830d7e8980ecd", null ],
    [ "PyAPI_FUNC", "pytime_8h.html#a18c0f3b3436dd76f90b3f837a53f93b2", null ],
    [ "PyAPI_FUNC", "pytime_8h.html#aa6e34963897c13dd3606f10c22cb7b4d", null ],
    [ "PyAPI_FUNC", "pytime_8h.html#a5740b210e292debc05184925dde28642", null ],
    [ "_PyTime_round_t", "pytime_8h.html#a7543c6b628da31e8f28d7e0e0a8988a2", null ],
    [ "div", "pytime_8h.html#a903a8b4215eb707721ebfee441d2615a", null ],
    [ "info", "pytime_8h.html#a34642418fa09e972671cdee2e6689ae3", null ],
    [ "mul", "pytime_8h.html#ac1811801af76610ecda1c989ab5bf46a", null ],
    [ "nsec", "pytime_8h.html#a9385d701d594f116cfd55a1272eeeaf4", null ],
    [ "obj", "pytime_8h.html#aaa12580403a2cc24c96324b4c5715889", null ],
    [ "round", "pytime_8h.html#a4575fc59a800838007f1bcc6a7616841", null ],
    [ "sec", "pytime_8h.html#a59edbaa8b9aa0356323cbe1992a543c1", null ],
    [ "secs", "pytime_8h.html#aa087562defe0b84529d3bfc93efb31bc", null ],
    [ "tm", "pytime_8h.html#a779ff21f60f1f27c431aecfee3522bba", null ],
    [ "tv", "pytime_8h.html#a74c9a35092b9fecdf2c4b09ee7ac2ddd", null ],
    [ "us", "pytime_8h.html#a81e758d80fce612c56f030b00828cf2d", null ],
    [ "usec", "pytime_8h.html#a4ce2070b31babbc650fc4b98f21ef131", null ]
];