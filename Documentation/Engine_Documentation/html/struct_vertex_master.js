var struct_vertex_master =
[
    [ "m_biTangent", "struct_vertex_master.html#a2ba4f5e459eac07f172410c8d5c4cb79", null ],
    [ "m_color", "struct_vertex_master.html#a9f58919da9895bbfc27e65ac50583dc7", null ],
    [ "m_normal", "struct_vertex_master.html#a4722bff07e10b1dd166b1ba485a73b6c", null ],
    [ "m_position", "struct_vertex_master.html#a0f7949404d167e64311fa8e2393c449f", null ],
    [ "m_tangent", "struct_vertex_master.html#a5e3cc299a5c76922a73c2a94b0a678b7", null ],
    [ "m_uv", "struct_vertex_master.html#aaa49b4050920a93aaebf3710a6505c51", null ]
];