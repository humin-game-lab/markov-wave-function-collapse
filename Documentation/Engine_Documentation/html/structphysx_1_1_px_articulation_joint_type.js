var structphysx_1_1_px_articulation_joint_type =
[
    [ "Enum", "structphysx_1_1_px_articulation_joint_type.html#a19799f4481dd84dbbcc4041579286097", [
      [ "ePRISMATIC", "structphysx_1_1_px_articulation_joint_type.html#a19799f4481dd84dbbcc4041579286097a16c0a5194dd2333ee76d6cfb5725411f", null ],
      [ "eREVOLUTE", "structphysx_1_1_px_articulation_joint_type.html#a19799f4481dd84dbbcc4041579286097a5105cd90b53b491b5bab481b48e918ce", null ],
      [ "eSPHERICAL", "structphysx_1_1_px_articulation_joint_type.html#a19799f4481dd84dbbcc4041579286097aa7295ac7ca1fd2f87a72871be72b3380", null ],
      [ "eFIX", "structphysx_1_1_px_articulation_joint_type.html#a19799f4481dd84dbbcc4041579286097a92eace2c78f5bb2b7ddcd0cfa2d40c34", null ],
      [ "eUNDEFINED", "structphysx_1_1_px_articulation_joint_type.html#a19799f4481dd84dbbcc4041579286097a03027d3c9b2aee48ebba0bd1cb0b5a44", null ]
    ] ]
];