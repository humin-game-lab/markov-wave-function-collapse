var structphysx_1_1_px_shape_flag =
[
    [ "Enum", "structphysx_1_1_px_shape_flag.html#aac506b3a4f26296802b7a1fccc7f0b08", [
      [ "eSIMULATION_SHAPE", "structphysx_1_1_px_shape_flag.html#aac506b3a4f26296802b7a1fccc7f0b08a32bb781b23dcc0732539ee31400fb949", null ],
      [ "eSCENE_QUERY_SHAPE", "structphysx_1_1_px_shape_flag.html#aac506b3a4f26296802b7a1fccc7f0b08a87c7cc674921dc98bed59317b9e88bd1", null ],
      [ "eTRIGGER_SHAPE", "structphysx_1_1_px_shape_flag.html#aac506b3a4f26296802b7a1fccc7f0b08ac9f1efbc2463d592d020a558635208b9", null ],
      [ "eVISUALIZATION", "structphysx_1_1_px_shape_flag.html#aac506b3a4f26296802b7a1fccc7f0b08a3d4310d373d6e8cde8a09c36b07b5380", null ]
    ] ]
];