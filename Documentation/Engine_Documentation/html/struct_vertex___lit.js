var struct_vertex___lit =
[
    [ "Vertex_Lit", "struct_vertex___lit.html#a3f05c344ecb534dffc7f83b04e7c4beb", null ],
    [ "Vertex_Lit", "struct_vertex___lit.html#acc92962a1bc1aa682b2aacb2f69dd2b8", null ],
    [ "~Vertex_Lit", "struct_vertex___lit.html#a7a722bfa5758d7f3a7cf5565aa16899f", null ],
    [ "m_biTangent", "struct_vertex___lit.html#a7f35b7e17ef8ef498d8f9b32a934b2a4", null ],
    [ "m_color", "struct_vertex___lit.html#a44a640506f5cf13ebb7861744bfaf932", null ],
    [ "m_normal", "struct_vertex___lit.html#a13d4901a1c421a33c6c5b69877f82446", null ],
    [ "m_position", "struct_vertex___lit.html#ab3b34b1420cd8a5fa89a45653f7bb21d", null ],
    [ "m_tangent", "struct_vertex___lit.html#a69ee9bf6a3dee3d49ebf807d25cf88d9", null ],
    [ "m_uv", "struct_vertex___lit.html#a4c72500aaead6b4d165df31453ee9625", null ]
];