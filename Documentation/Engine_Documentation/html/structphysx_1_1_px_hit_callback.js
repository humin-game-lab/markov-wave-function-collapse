var structphysx_1_1_px_hit_callback =
[
    [ "PxHitCallback", "structphysx_1_1_px_hit_callback.html#ad986be2d48af7ec1bf42e7bf332621e4", null ],
    [ "~PxHitCallback", "structphysx_1_1_px_hit_callback.html#a1d2f12389dbb1922798fd50dd49b2275", null ],
    [ "hasAnyHits", "structphysx_1_1_px_hit_callback.html#aad42354655042654c7318aeb4d53ece2", null ],
    [ "processTouches", "structphysx_1_1_px_hit_callback.html#a20377b4e14371796c2a0cadb40452d77", null ],
    [ "block", "structphysx_1_1_px_hit_callback.html#a4225042d168769d28ad9ec8fcfdd1ec5", null ],
    [ "hasBlock", "structphysx_1_1_px_hit_callback.html#a14a8ca1e8b031e49cae14654b741d1c7", null ],
    [ "maxNbTouches", "structphysx_1_1_px_hit_callback.html#a43505db5e6dde1d907d500bd3a22a26f", null ],
    [ "nbTouches", "structphysx_1_1_px_hit_callback.html#aa1b51efca19b7a11d8d4b6faf5888431", null ],
    [ "touches", "structphysx_1_1_px_hit_callback.html#ac12921e72f6aa31b858261d90d7a811f", null ]
];