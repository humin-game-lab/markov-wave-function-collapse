var class_named_strings =
[
    [ "NamedStrings", "class_named_strings.html#ad1f738653c6230a1b8f844f7a2a004ba", null ],
    [ "NamedStrings", "class_named_strings.html#aa04843683799369c6a1dffb128e77fc7", null ],
    [ "~NamedStrings", "class_named_strings.html#a38f44f926849d7e7db3f1d38c4108daa", null ],
    [ "GetNamedStringSize", "class_named_strings.html#a8dbc8fceb9cce0537497a5470409e50c", null ],
    [ "GetValue", "class_named_strings.html#a250bdf56f35a3419fd9fe9814564f902", null ],
    [ "GetValue", "class_named_strings.html#ad9638d84aa31806d9721e4dffbe341ff", null ],
    [ "GetValue", "class_named_strings.html#aa6f87254d9b034203df04af3b8a9381f", null ],
    [ "GetValue", "class_named_strings.html#a926b69bd0870e34b55a80c5db2f53d32", null ],
    [ "GetValue", "class_named_strings.html#ae7d7f13c215a3a5bb381374e9a3eaf29", null ],
    [ "GetValue", "class_named_strings.html#a3cb35a739f8e34ae4fc098f765f04f37", null ],
    [ "GetValue", "class_named_strings.html#aa0dc453d8f8c3ea2b366e8b7dc0197b0", null ],
    [ "GetValue", "class_named_strings.html#a29d688cc8adcaeffe92573b8d4749c1d", null ],
    [ "GetValue", "class_named_strings.html#a284e2c91d334c6023c84738721a92255", null ],
    [ "GetValue", "class_named_strings.html#a4df0b078385528d7fbe0d55a9af66cbe", null ],
    [ "PopulateFromXmlElementAttributes", "class_named_strings.html#a0e63c4e3dc364bef4c906448a6b0e5b4", null ],
    [ "SetValue", "class_named_strings.html#a949b89af253285f671d15ae436e69b4e", null ]
];