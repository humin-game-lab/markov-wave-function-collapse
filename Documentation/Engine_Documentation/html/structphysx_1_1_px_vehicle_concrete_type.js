var structphysx_1_1_px_vehicle_concrete_type =
[
    [ "Enum", "structphysx_1_1_px_vehicle_concrete_type.html#afc3e5195311939f6eacdda4ef3dc8b22", [
      [ "eVehicleNoDrive", "structphysx_1_1_px_vehicle_concrete_type.html#afc3e5195311939f6eacdda4ef3dc8b22a9ef678118742d6bb03a0f9f07bed40ab", null ],
      [ "eVehicleDrive4W", "structphysx_1_1_px_vehicle_concrete_type.html#afc3e5195311939f6eacdda4ef3dc8b22ae677e95d6c4e71386c8048c0e1f005e7", null ],
      [ "eVehicleDriveNW", "structphysx_1_1_px_vehicle_concrete_type.html#afc3e5195311939f6eacdda4ef3dc8b22adab728e452d9164f746a8b733bdd9fbe", null ],
      [ "eVehicleDriveTank", "structphysx_1_1_px_vehicle_concrete_type.html#afc3e5195311939f6eacdda4ef3dc8b22a3ef4709b325193908e30853ba93bdffc", null ]
    ] ]
];