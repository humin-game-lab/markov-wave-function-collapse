var structphysx_1_1_px_articulation_axis =
[
    [ "Enum", "structphysx_1_1_px_articulation_axis.html#ae59eb440b663d678b2aa594090a70d26", [
      [ "eTWIST", "structphysx_1_1_px_articulation_axis.html#ae59eb440b663d678b2aa594090a70d26a32a688a72ed34a0bf1eba14839fc9867", null ],
      [ "eSWING1", "structphysx_1_1_px_articulation_axis.html#ae59eb440b663d678b2aa594090a70d26a82236a51b1417360032b9f4280ac0352", null ],
      [ "eSWING2", "structphysx_1_1_px_articulation_axis.html#ae59eb440b663d678b2aa594090a70d26a1d3d1222dda8a97326123a9ce056eb5c", null ],
      [ "eX", "structphysx_1_1_px_articulation_axis.html#ae59eb440b663d678b2aa594090a70d26aca24c1ccd944e9d39c9fb1d3075d4c0a", null ],
      [ "eY", "structphysx_1_1_px_articulation_axis.html#ae59eb440b663d678b2aa594090a70d26a8b3588752d706e21dc91bc6cbc77da19", null ],
      [ "eZ", "structphysx_1_1_px_articulation_axis.html#ae59eb440b663d678b2aa594090a70d26adaa93b4c77800e1b3eec8aa4f16decf2", null ],
      [ "eCOUNT", "structphysx_1_1_px_articulation_axis.html#ae59eb440b663d678b2aa594090a70d26afb869aa1e069ce1e6bcc92fa1d946fee", null ]
    ] ]
];