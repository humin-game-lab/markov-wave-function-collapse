var classphysx_1_1_px_vehicle_engine_data =
[
    [ "eMAX_NB_ENGINE_TORQUE_CURVE_ENTRIES", "classphysx_1_1_px_vehicle_engine_data.html#af48718bccecaf420dc1bbe5eec19bba7a712cc67124c259ec32971bc730b41cef", null ],
    [ "PxVehicleEngineData", "classphysx_1_1_px_vehicle_engine_data.html#a8f5e2a867f2b1a73e7394fc13a1b3f27", null ],
    [ "PxVehicleEngineData", "classphysx_1_1_px_vehicle_engine_data.html#a57139c8dd1c1819e7ec2823c158bc75f", null ],
    [ "getRecipMaxOmega", "classphysx_1_1_px_vehicle_engine_data.html#ae22c4826411a15d6289fd48501c2a65e", null ],
    [ "getRecipMOI", "classphysx_1_1_px_vehicle_engine_data.html#a8d95675d57215465ccb4e312f8310dd1", null ],
    [ "PxVehicleDriveSimData", "classphysx_1_1_px_vehicle_engine_data.html#aa2f8773ce851c65e3c7d31b8991ea8f8", null ],
    [ "mDampingRateFullThrottle", "classphysx_1_1_px_vehicle_engine_data.html#a9c773ff96a3759d339d40528c9c920c4", null ],
    [ "mDampingRateZeroThrottleClutchDisengaged", "classphysx_1_1_px_vehicle_engine_data.html#ab5f3a82d74f975bdac8c34c90873ec1c", null ],
    [ "mDampingRateZeroThrottleClutchEngaged", "classphysx_1_1_px_vehicle_engine_data.html#a4d6c9b3a0e79d3038469b17eb9df3c27", null ],
    [ "mMaxOmega", "classphysx_1_1_px_vehicle_engine_data.html#a2c1c6a98a4308e00748c7e012e281a87", null ],
    [ "mMOI", "classphysx_1_1_px_vehicle_engine_data.html#abbe03fffa8edcbc03dda0bd674f5624f", null ],
    [ "mPeakTorque", "classphysx_1_1_px_vehicle_engine_data.html#a378a15f11634e299eb8f68bdd1c8f5e0", null ],
    [ "mTorqueCurve", "classphysx_1_1_px_vehicle_engine_data.html#ac5549df700f494e216ffe2f549aec98b", null ]
];