var structphysx_1_1_px_controller_state =
[
    [ "collisionFlags", "structphysx_1_1_px_controller_state.html#a8417f85ca4c1cc4c160bf72d1c4a33d2", null ],
    [ "deltaXP", "structphysx_1_1_px_controller_state.html#a122ccb9be22a3661c91f8f16a2e7f3cb", null ],
    [ "isMovingUp", "structphysx_1_1_px_controller_state.html#a320a9e530793688ebc0ec9a628536e89", null ],
    [ "standOnAnotherCCT", "structphysx_1_1_px_controller_state.html#a78c4d4147d9f4ef614ff949e56e1ec43", null ],
    [ "standOnObstacle", "structphysx_1_1_px_controller_state.html#a6baf08698e3f895485dfd846e37d92a7", null ],
    [ "touchedActor", "structphysx_1_1_px_controller_state.html#accf56fc4bfaf784c9ec1d0edc4026e41", null ],
    [ "touchedObstacleHandle", "structphysx_1_1_px_controller_state.html#a59381edf42cd8e3cebc78b4bd5436e60", null ],
    [ "touchedShape", "structphysx_1_1_px_controller_state.html#acae6a4aa9f2c29ec3256982361394bc0", null ]
];