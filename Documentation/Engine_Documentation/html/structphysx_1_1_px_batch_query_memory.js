var structphysx_1_1_px_batch_query_memory =
[
    [ "PxBatchQueryMemory", "structphysx_1_1_px_batch_query_memory.html#ae66430e2490e64f6f29d6dc04aa89b77", null ],
    [ "getMaxOverlapsPerExecute", "structphysx_1_1_px_batch_query_memory.html#a1b3a4b2e5d3bc5aeb0f3b7870b2ba75c", null ],
    [ "getMaxRaycastsPerExecute", "structphysx_1_1_px_batch_query_memory.html#ad4c3c244e6f765b6b564a05a0e2be5d6", null ],
    [ "getMaxSweepsPerExecute", "structphysx_1_1_px_batch_query_memory.html#a4448c3198ffbe596574193d8dd92ed28", null ],
    [ "overlapResultBufferSize", "structphysx_1_1_px_batch_query_memory.html#aa2f877dec9776869a78932499d7dfa31", null ],
    [ "overlapTouchBufferSize", "structphysx_1_1_px_batch_query_memory.html#aeeae4420d8b3caf8394f1c93973cf295", null ],
    [ "raycastResultBufferSize", "structphysx_1_1_px_batch_query_memory.html#af81384d050cb0ca0dba3657cd510a4d4", null ],
    [ "raycastTouchBufferSize", "structphysx_1_1_px_batch_query_memory.html#a1144a329d9fc5dea1e8d5dfa7d31b40f", null ],
    [ "sweepResultBufferSize", "structphysx_1_1_px_batch_query_memory.html#a14990082d950298b289a48a1253656f0", null ],
    [ "sweepTouchBufferSize", "structphysx_1_1_px_batch_query_memory.html#a01afa0c9b39a2cade1cd6ef661c1ce82", null ],
    [ "userOverlapResultBuffer", "structphysx_1_1_px_batch_query_memory.html#a82fd7a098f92a66e53ac6fb64f0ffd96", null ],
    [ "userOverlapTouchBuffer", "structphysx_1_1_px_batch_query_memory.html#aca74dcb8661c4a2b9cd0c8468e005481", null ],
    [ "userRaycastResultBuffer", "structphysx_1_1_px_batch_query_memory.html#a4814f0bd7d5a3a4e2c795c99e4fe8160", null ],
    [ "userRaycastTouchBuffer", "structphysx_1_1_px_batch_query_memory.html#a29d425dea5f465b176979f2b4403f68d", null ],
    [ "userSweepResultBuffer", "structphysx_1_1_px_batch_query_memory.html#abe7cc396cab2bba7e308749fe63be3a1", null ],
    [ "userSweepTouchBuffer", "structphysx_1_1_px_batch_query_memory.html#a1fde1dda5e89fc3addfb9337a2677b7c", null ]
];