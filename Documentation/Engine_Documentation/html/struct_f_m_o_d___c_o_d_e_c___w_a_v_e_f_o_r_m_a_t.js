var struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t =
[
    [ "channelmask", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#a5ef3ccd8b66efab89177fb7f02de8a3e", null ],
    [ "channelorder", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#a95f5ba661ea00c1a6ab06092f9bc9fac", null ],
    [ "channels", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#aed37ce9c60c7cafb4b1335285a97d890", null ],
    [ "format", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#aced3a8f677c8663353851db51c43f581", null ],
    [ "frequency", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#a6a54a1b79daa79dc605d9145f04aaddb", null ],
    [ "lengthbytes", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#a2e9d8c283bfc9b69e9f6a22c04507e62", null ],
    [ "lengthpcm", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#a0cccdeb82b13ac9342c24401c5a50f5f", null ],
    [ "loopend", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#af6fe45ead433ef4bdb8d3e74859900f2", null ],
    [ "loopstart", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#ae795e36327ba4772301f09501b2a8cc0", null ],
    [ "mode", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#a1f23ba212c380e08e9d0b83f16a00320", null ],
    [ "name", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#a45338299febb69572613d96e9df5529c", null ],
    [ "pcmblocksize", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#a770e18ecf83808c1b3e74d784ccb846a", null ],
    [ "peakvolume", "struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#a4bf4a9aeb4628e4eec7ff239453bb4e7", null ]
];