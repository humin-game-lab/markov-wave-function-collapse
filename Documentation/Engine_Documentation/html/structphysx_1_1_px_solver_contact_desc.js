var structphysx_1_1_px_solver_contact_desc =
[
    [ "PX_ALIGN", "structphysx_1_1_px_solver_contact_desc.html#a417bed18aea27a3ea9ca848fc90430e2", null ],
    [ "axisConstraintCount", "structphysx_1_1_px_solver_contact_desc.html#a7da1a36301435dbb20b6a71f4d23705f", null ],
    [ "contactForces", "structphysx_1_1_px_solver_contact_desc.html#a89e1ada96d224655b19d21d9cf46325d", null ],
    [ "contacts", "structphysx_1_1_px_solver_contact_desc.html#ae073429079bb103c8ad2bd68d5e5ff44", null ],
    [ "disableStrongFriction", "structphysx_1_1_px_solver_contact_desc.html#a267cff1a69d2ecfbb45182376d648add", null ],
    [ "frictionCount", "structphysx_1_1_px_solver_contact_desc.html#ad9189cb1f691cd6d3a8d6fb53bfdeaba", null ],
    [ "frictionPtr", "structphysx_1_1_px_solver_contact_desc.html#a270afd1954fb946c3fba68b8a2d8097d", null ],
    [ "hasForceThresholds", "structphysx_1_1_px_solver_contact_desc.html#abfaee416e19b9a9632dd228bbfcaf55f", null ],
    [ "hasMaxImpulse", "structphysx_1_1_px_solver_contact_desc.html#a6481253aba3465c61179a890f7c28850", null ],
    [ "maxCCDSeparation", "structphysx_1_1_px_solver_contact_desc.html#a6f7f2d81e3638656fe4bc2c6ca7514b9", null ],
    [ "numContactPatches", "structphysx_1_1_px_solver_contact_desc.html#aa74c24be8ee8dffeea7bb12e09c17e62", null ],
    [ "numContacts", "structphysx_1_1_px_solver_contact_desc.html#a717ed5333b291acecca303ab89a306f5", null ],
    [ "numFrictionPatches", "structphysx_1_1_px_solver_contact_desc.html#ad21f2f0c7e93a62c4a821c2612097fea", null ],
    [ "pad", "structphysx_1_1_px_solver_contact_desc.html#a4e3009b6b42f86ccad3fb4ce065aafab", null ],
    [ "restDistance", "structphysx_1_1_px_solver_contact_desc.html#a28a18783cabdcbb26ca3b2924f95bbb0", null ],
    [ "startContactPatchIndex", "structphysx_1_1_px_solver_contact_desc.html#a73e4dc426dc9cd83690036d448f2f7ce", null ],
    [ "startFrictionPatchIndex", "structphysx_1_1_px_solver_contact_desc.html#a455aaec8b8ee9b18584ed5c0e3d4643d", null ]
];