var structphysx_1_1_px_controller_stats =
[
    [ "nbFullUpdates", "structphysx_1_1_px_controller_stats.html#a04b4da8006df6ffa17a92639494454d9", null ],
    [ "nbIterations", "structphysx_1_1_px_controller_stats.html#a981503971b958ad27c809b9010af61c8", null ],
    [ "nbPartialUpdates", "structphysx_1_1_px_controller_stats.html#a0080de93ce72331c19cace0c5b7bc669", null ],
    [ "nbTessellation", "structphysx_1_1_px_controller_stats.html#a9a1bef2e514cd0f8095c8a00d381951c", null ]
];