var structphysx_1_1_px_convex_mesh_cooking_result =
[
    [ "Enum", "structphysx_1_1_px_convex_mesh_cooking_result.html#ac00bd08f804c233bde64159eb5bfbeb7", [
      [ "eSUCCESS", "structphysx_1_1_px_convex_mesh_cooking_result.html#ac00bd08f804c233bde64159eb5bfbeb7a6b594d16bdbbfbc5b40977b52b2a48d2", null ],
      [ "eZERO_AREA_TEST_FAILED", "structphysx_1_1_px_convex_mesh_cooking_result.html#ac00bd08f804c233bde64159eb5bfbeb7aad82fb1b21a3818dd4195c9ba08557b9", null ],
      [ "ePOLYGONS_LIMIT_REACHED", "structphysx_1_1_px_convex_mesh_cooking_result.html#ac00bd08f804c233bde64159eb5bfbeb7a0334134ec9034429ffe7b8d9922147af", null ],
      [ "eFAILURE", "structphysx_1_1_px_convex_mesh_cooking_result.html#ac00bd08f804c233bde64159eb5bfbeb7a896daa7d705cd10b3201be251dc9512b", null ]
    ] ]
];