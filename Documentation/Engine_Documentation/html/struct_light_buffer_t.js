var struct_light_buffer_t =
[
    [ "ambient", "struct_light_buffer_t.html#a924a92ffdea52cfcfe3f273c612cec44", null ],
    [ "emissiveFactor", "struct_light_buffer_t.html#a610a84644148946bf17dee65f8a1c9b1", null ],
    [ "lights", "struct_light_buffer_t.html#abbc8ab1eae569cb883f71f2b23729033", null ],
    [ "pad", "struct_light_buffer_t.html#a95defd5703085b42a8b1b353d5d26018", null ],
    [ "specFactor", "struct_light_buffer_t.html#a553fb163dc37f5fe6247c931cf3e0910", null ],
    [ "specPower", "struct_light_buffer_t.html#a27734e140b4d41744f08925faae7fc9f", null ]
];