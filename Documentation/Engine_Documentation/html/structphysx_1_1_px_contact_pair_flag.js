var structphysx_1_1_px_contact_pair_flag =
[
    [ "Enum", "structphysx_1_1_px_contact_pair_flag.html#acf0fe10d1f8258a6b06078f4f7ff7466", [
      [ "eREMOVED_SHAPE_0", "structphysx_1_1_px_contact_pair_flag.html#acf0fe10d1f8258a6b06078f4f7ff7466a0e35d1c2db62356f37a6cd1c4ef5db49", null ],
      [ "eREMOVED_SHAPE_1", "structphysx_1_1_px_contact_pair_flag.html#acf0fe10d1f8258a6b06078f4f7ff7466ae4b76bfd2b27f6d8d6120be78128c835", null ],
      [ "eACTOR_PAIR_HAS_FIRST_TOUCH", "structphysx_1_1_px_contact_pair_flag.html#acf0fe10d1f8258a6b06078f4f7ff7466a0bd024f1a3969ae57d861f87263482d5", null ],
      [ "eACTOR_PAIR_LOST_TOUCH", "structphysx_1_1_px_contact_pair_flag.html#acf0fe10d1f8258a6b06078f4f7ff7466a4b0284e187c710fdee21d79a77bba8d9", null ],
      [ "eINTERNAL_HAS_IMPULSES", "structphysx_1_1_px_contact_pair_flag.html#acf0fe10d1f8258a6b06078f4f7ff7466a7fa87095ec2c4a8e3ebaf96f9644438c", null ],
      [ "eINTERNAL_CONTACTS_ARE_FLIPPED", "structphysx_1_1_px_contact_pair_flag.html#acf0fe10d1f8258a6b06078f4f7ff7466ade405f8d5bb2e3889e4a9fba0463f1f6", null ]
    ] ]
];