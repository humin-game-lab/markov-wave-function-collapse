var class_text_properties =
[
    [ "TextProperties", "class_text_properties.html#aa21336809261eb5d0972fdf4c4f97e4f", null ],
    [ "TextProperties", "class_text_properties.html#a7adc0274118f5b1c1a1bcb823729ba63", null ],
    [ "~TextProperties", "class_text_properties.html#a502cbdb8ef4ded93f0d2feaeb58d350b", null ],
    [ "m_endPosition", "class_text_properties.html#adac98fea07ffb50912732cb236179830", null ],
    [ "m_fontHeight", "class_text_properties.html#aaef21253779e370c68194a8e9bbddadb", null ],
    [ "m_isBillboarded", "class_text_properties.html#a02d4ae88b101cecb965f0c99856b583a", null ],
    [ "m_pivot", "class_text_properties.html#ac8f2c2ddc3513b16aff76634e283eab5", null ],
    [ "m_position", "class_text_properties.html#aa587b46deb04cbc9352a4878a226203d", null ],
    [ "m_startPosition", "class_text_properties.html#a0a03c96d77e4171dd465db1480a649b4", null ],
    [ "m_string", "class_text_properties.html#a1615027dc0292843a003d27319bf9d3f", null ]
];