var structphysx_1_1_px_batch_query_result =
[
    [ "getAnyHit", "structphysx_1_1_px_batch_query_result.html#a8da20ae2f1a1062562328e216c14a892", null ],
    [ "getNbAnyHits", "structphysx_1_1_px_batch_query_result.html#a191a7c36b4f0de3b9fcea66c09019766", null ],
    [ "block", "structphysx_1_1_px_batch_query_result.html#ac6d249f1cfba3a68c2e09c353b4cdb77", null ],
    [ "hasBlock", "structphysx_1_1_px_batch_query_result.html#a76eec24254202dde66ece15322d8d77c", null ],
    [ "nbTouches", "structphysx_1_1_px_batch_query_result.html#a10b1bd4c6f76bec83cd2b6ea4fd90747", null ],
    [ "pad", "structphysx_1_1_px_batch_query_result.html#adcdc4e19a8453e065b70f503da6201eb", null ],
    [ "queryStatus", "structphysx_1_1_px_batch_query_result.html#a57a7d2366dcbe91c02ef438cc29c727d", null ],
    [ "touches", "structphysx_1_1_px_batch_query_result.html#a5d3376153bdab32d2775749a3ab37744", null ],
    [ "userData", "structphysx_1_1_px_batch_query_result.html#a9e7e8c524012315f0ca03a1cd9ab77ff", null ]
];