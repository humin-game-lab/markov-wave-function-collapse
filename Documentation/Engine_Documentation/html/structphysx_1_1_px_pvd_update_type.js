var structphysx_1_1_px_pvd_update_type =
[
    [ "Enum", "structphysx_1_1_px_pvd_update_type.html#a6ced9700c311b7e212c3c8c739a6b293", [
      [ "CREATE_INSTANCE", "structphysx_1_1_px_pvd_update_type.html#a6ced9700c311b7e212c3c8c739a6b293a242ef8e213c01a13d250115aebaeb41f", null ],
      [ "RELEASE_INSTANCE", "structphysx_1_1_px_pvd_update_type.html#a6ced9700c311b7e212c3c8c739a6b293a111d8b070a2759670f741cd9b88265ba", null ],
      [ "UPDATE_ALL_PROPERTIES", "structphysx_1_1_px_pvd_update_type.html#a6ced9700c311b7e212c3c8c739a6b293a1ba2877cb53782df910b12081d3bdb49", null ],
      [ "UPDATE_SIM_PROPERTIES", "structphysx_1_1_px_pvd_update_type.html#a6ced9700c311b7e212c3c8c739a6b293a3022aceb9c46dd16a3253b32554c1a24", null ]
    ] ]
];