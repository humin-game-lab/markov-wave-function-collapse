var structphysx_1_1_px_mesh_preprocessing_flag =
[
    [ "Enum", "structphysx_1_1_px_mesh_preprocessing_flag.html#af99b608b7cc91b6368f85b9b2b18477d", [
      [ "eWELD_VERTICES", "structphysx_1_1_px_mesh_preprocessing_flag.html#af99b608b7cc91b6368f85b9b2b18477dacb4036303328c2c917b064421e6b262c", null ],
      [ "eDISABLE_CLEAN_MESH", "structphysx_1_1_px_mesh_preprocessing_flag.html#af99b608b7cc91b6368f85b9b2b18477da8269d0f1cbf33a77053a7449c8fd2e17", null ],
      [ "eDISABLE_ACTIVE_EDGES_PRECOMPUTE", "structphysx_1_1_px_mesh_preprocessing_flag.html#af99b608b7cc91b6368f85b9b2b18477da61c8a1d6f3881e302a68be743b574415", null ],
      [ "eFORCE_32BIT_INDICES", "structphysx_1_1_px_mesh_preprocessing_flag.html#af99b608b7cc91b6368f85b9b2b18477da0b3395c780b20b57601325be77e2ac63", null ]
    ] ]
];