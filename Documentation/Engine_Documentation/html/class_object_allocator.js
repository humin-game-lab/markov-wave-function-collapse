var class_object_allocator =
[
    [ "Allocate", "class_object_allocator.html#a3272d40688aa40971d1c61ac36144861", null ],
    [ "Create", "class_object_allocator.html#a94fcaf2620d03725f5945217b8d64b92", null ],
    [ "Deinitialize", "class_object_allocator.html#ad3b896c7191c0392a74a7b92e266cf5a", null ],
    [ "Destroy", "class_object_allocator.html#abf564c2fd040551c86c74d9d00a1dd38", null ],
    [ "Free", "class_object_allocator.html#aecb7854185c4ada3375ef5d1593986db", null ],
    [ "Initialize", "class_object_allocator.html#af95e77a6312e53e04a963b532778c25a", null ]
];