/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ProdigyGameEngine", "index.html", [
    [ "Prodigy Engine", "md__code__submodule__engine__r_e_a_d_m_e.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", "namespacemembers_func" ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", "functions_eval" ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", "globals_vars" ],
        [ "Typedefs", "globals_type.html", "globals_type" ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_a_a_b_b2_8cpp.html",
"_depth_stencil_target_view_8cpp_source.html",
"_frustum_8hpp.html#a3be99da70179abe4e0f1612de23248c0",
"_math_utils_8hpp.html#aa23630d51274c11f5d1584bdc32ec708",
"_physics_types_8hpp_source.html",
"_px_convex_mesh_ext_8h_source.html",
"_px_meta_data_8h.html#a03a3549588909e6ca0a491af76146757",
"_px_vec2_8h.html",
"_python-ast_8h.html#a7a809c8cc37c63bb2efd03c0e16a917e",
"_renderer_types_8hpp.html#ad66c770ffb139d7d4fdb4bb9d57c695daad7059e12b5652c8d65d230aa1b4d63f",
"_vertex_utils_8hpp.html#ad136ad559bd38aa75944d8e36f0417f3",
"bytes__methods_8h.html#aa8a00e9134522690101aacab84dbab0a",
"class_buffer_read_utils.html#abcd0b7f4e38f4679e4120cda897fb3d8",
"class_dev_console.html#a858c804d05acc186c42d6ddd4b59da43",
"class_f_m_o_d_1_1_sound.html#ae5af50e5bdaec640dceb9a797c75fa99",
"class_log_system.html",
"class_phys_x_simulation_event_callbacks.html",
"class_render_context.html#a8f1fa0c402a5f7127b4691e0dbb41ffe",
"class_texture.html#a4ef2b75b6377be7d4115a31f5584ab81",
"classphysx_1_1_px_articulation_base.html#ad92d21e458c4aefdbb059d0230c9c14c",
"classphysx_1_1_px_bounds3.html#a4de0f5f36a845b74110530be75d33b84",
"classphysx_1_1_px_controller_desc.html#a25944f718244bf6e723886e0501a74c3",
"classphysx_1_1_px_flags.html#a1c05cce13ba07d8b7b4bda69d429d018",
"classphysx_1_1_px_mat33.html#a97321cba6c51112b94dcc77093ef368e",
"classphysx_1_1_px_pvd_scene_client.html#ace24bd6f99211eb348b875392add5670",
"classphysx_1_1_px_scene.html#a966b51fad4ddbf9128de548b0717a41c",
"classphysx_1_1_px_simulation_statistics.html#ae5734639714fdd65b7a8897965f72cdb",
"classphysx_1_1_px_vec4.html#a506d1dbc27482668802477b4695dbc6e",
"classphysx_1_1_px_vehicle_drive_tank.html#ac455ec1c5f55e56279370805997e4e57",
"classphysx_1_1_px_vehicle_wheels_sim_data.html#a9b24c5e8b135b67fae109d3e69f6347a",
"classtinyxml2_1_1_x_m_l_element.html#a14d38aa4b5e18a46274a27425188a6a1",
"code_8h.html#a97802102d8f13ad3295498951c112163",
"dynamic__annotations_8h.html#a256593e2dba68cef52adcc1281a84c99",
"fmod_8h.html#a351daa35a9a6c3d386acf71af0c19ac5",
"fmod_8h.html#ada5c2ef8fbfd05794141be991fd16fa8",
"fmod__common_8h.html#a8d2dca75f723bf7598670db2f22f615f",
"fmod__common_8h.html#ae85bc3a4b6ab1439c2c8db164fb746ef",
"fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3af81c96556744c3f4773d752e0357a0d0",
"fmod__output_8h.html#aa8cda02dcbdc494b1981e287c30f60a7",
"globals_p.html",
"group__foundation.html#ga382d2b60e6161c1dcb3801bbf8e1b4a3",
"imgui_8h.html#a08000421b9cc13757430efe54178ae0f",
"imgui_8h.html#a560f32a1c291007d1b4ec8974ff0de31ad6dd6b8e6069c2343becf0c98590afe6",
"imgui_8h.html#a9795f730b4043a98b6254738d86efcdc",
"imgui_8h.html#af186ce1bc2026ef29148030fccfbd2d9",
"imgui__internal_8h.html#a772a57072bdc1dbcb826d3acb3c00cfca0ba47c4a344a8043f6764f169d983462",
"import_8h.html#a9d09c53ab5dbf8d667025660fe3a4bc6",
"metagrammar_8h.html#a62520fa177b138ad76b75745c4a00c51",
"object_8h.html#a49e32a5d54974273e7d2150170cc8196",
"opcode_8h.html#a82ccb3df0e9d090d03fd6e9706977a44",
"pyconfig_8h.html#af3141627a0587e6154db8a3a9c23ef8d",
"pystate_8h.html#a01bc4292e34451bbb883601cca79481d",
"stb__image_8h.html#af4f17acd30945a75901fdc022f90575f",
"struct__frame.html#a92f0794bfa51e632b26c92e197051051",
"struct__typeobject.html#aff367b8344e94586326576ec274b625d",
"struct_f_m_o_d___c_o_d_e_c___w_a_v_e_f_o_r_m_a_t.html#a4bf4a9aeb4628e4eec7ff239453bb4e7",
"struct_float_range.html#ab089d0bdf7696ed15a7c28a627538f49",
"struct_im_font_build_src_data.html#a7edd0193194ca109bfa5042369861935",
"struct_im_gui_i_o.html",
"struct_im_gui_storage.html#ac5beee31a59b3f5294b41992717be7bf",
"struct_im_gui_window_temp_data.html#a95e11283d6f15aff01b8370267edd61c",
"struct_my_document.html",
"struct_py_import_error_object.html#a0ef77bd6a01c2698e17de6a726e33cdc",
"struct_s_t_b___textedit_state.html#a527319df94e0fe262548fb48bebf3dea",
"structasdl__int__seq.html#ae8d9dcdc0b90d0f395c650783adb095f",
"structphysx_1_1_px_broad_phase_type.html#ac80bf98cad52e689308c4e2b367b8a2fa0f33b5a72b3504e9b5910a647c3f7994",
"structphysx_1_1_px_controller_collision_flag.html",
"structphysx_1_1_px_filter_object_type.html#ac6b0a7e9801f1a591b42a773eda8526bad6231974bfe12cf1f9a10e9c21fa32a8",
"structphysx_1_1_px_query_cache.html",
"structphysx_1_1_px_t_g_s_solver_constraint_prep_desc.html#ad228869673ddaf778a14058d8a6d8a5b",
"structphysx_1_1immediate_1_1_px_link_data.html#a315d40b59e195ae858e167c52b035eb1",
"sysmodule_8h.html#ae43034138c788c5f74572acb696ca9e2",
"unicodeobject_8h.html#a3dfe0496a28d54c76467ee77d67078ab"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';