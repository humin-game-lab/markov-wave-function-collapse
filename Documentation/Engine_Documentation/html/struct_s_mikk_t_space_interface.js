var struct_s_mikk_t_space_interface =
[
    [ "m_getNormal", "struct_s_mikk_t_space_interface.html#a0c5a7381c82970ce5716ddf795dc7a7d", null ],
    [ "m_getNumFaces", "struct_s_mikk_t_space_interface.html#a9847a5a1c29192eeb911db6b9b738ba7", null ],
    [ "m_getNumVerticesOfFace", "struct_s_mikk_t_space_interface.html#a9a6987d56432d6d563da38eef1638ca6", null ],
    [ "m_getPosition", "struct_s_mikk_t_space_interface.html#a13ddaf791c4e7c5fc4d8c1a90bce7c4c", null ],
    [ "m_getTexCoord", "struct_s_mikk_t_space_interface.html#a33420fe6a9b28ecb6750ec04d0bbf9f1", null ],
    [ "m_setTSpace", "struct_s_mikk_t_space_interface.html#a678991cc40cff62c31d71d9497c69879", null ],
    [ "m_setTSpaceBasic", "struct_s_mikk_t_space_interface.html#a8d00d25357740b6862a7b7ce1fddf2f0", null ]
];