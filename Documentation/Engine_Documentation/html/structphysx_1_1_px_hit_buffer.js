var structphysx_1_1_px_hit_buffer =
[
    [ "PxHitBuffer", "structphysx_1_1_px_hit_buffer.html#aab6e2290a2fd3b093c2216ebeae3e471", null ],
    [ "~PxHitBuffer", "structphysx_1_1_px_hit_buffer.html#ae096c301450c770d735785ba103bfaea", null ],
    [ "getAnyHit", "structphysx_1_1_px_hit_buffer.html#a50a7599c5c6c5792366a5b6fcf0b8ecc", null ],
    [ "getMaxNbTouches", "structphysx_1_1_px_hit_buffer.html#ab45db1ba4dca948dd732cc00d73f4751", null ],
    [ "getNbAnyHits", "structphysx_1_1_px_hit_buffer.html#aea64cc00ace68035126a9d161a2b05b4", null ],
    [ "getNbTouches", "structphysx_1_1_px_hit_buffer.html#a1780edab5435920a4e77474a20447f2c", null ],
    [ "getTouch", "structphysx_1_1_px_hit_buffer.html#a46cdf51fa34fc2c08a5452ee80d4b2f8", null ],
    [ "getTouches", "structphysx_1_1_px_hit_buffer.html#a0b1fed7c08a1cac5ade716fbbd429f18", null ],
    [ "processTouches", "structphysx_1_1_px_hit_buffer.html#aa80c82d70289185415d3160f1b1e1dff", null ]
];