var structphysx_1_1_px_convex_flag =
[
    [ "Enum", "structphysx_1_1_px_convex_flag.html#a90b3c67ca51c9757220b42a2a59ccbc9", [
      [ "e16_BIT_INDICES", "structphysx_1_1_px_convex_flag.html#a90b3c67ca51c9757220b42a2a59ccbc9a42c5ab2bd5db039bbb4d2bf73c7aa7fa", null ],
      [ "eCOMPUTE_CONVEX", "structphysx_1_1_px_convex_flag.html#a90b3c67ca51c9757220b42a2a59ccbc9a059aee08b70d4811a90154be2fda4bfa", null ],
      [ "eCHECK_ZERO_AREA_TRIANGLES", "structphysx_1_1_px_convex_flag.html#a90b3c67ca51c9757220b42a2a59ccbc9a6b4c082db2c5d985853ea696ae2be0e5", null ],
      [ "eQUANTIZE_INPUT", "structphysx_1_1_px_convex_flag.html#a90b3c67ca51c9757220b42a2a59ccbc9ac6691a3535f7200b73ae4989a1ce2a39", null ],
      [ "eDISABLE_MESH_VALIDATION", "structphysx_1_1_px_convex_flag.html#a90b3c67ca51c9757220b42a2a59ccbc9a2a7196f261aeee4bdf14d9354e98bd9d", null ],
      [ "ePLANE_SHIFTING", "structphysx_1_1_px_convex_flag.html#a90b3c67ca51c9757220b42a2a59ccbc9a1ef8cd0372ecbd538bd51f1ce5ae3c32", null ],
      [ "eFAST_INERTIA_COMPUTATION", "structphysx_1_1_px_convex_flag.html#a90b3c67ca51c9757220b42a2a59ccbc9a355a75c3fde0eaf439a7beec4911d21b", null ],
      [ "eGPU_COMPATIBLE", "structphysx_1_1_px_convex_flag.html#a90b3c67ca51c9757220b42a2a59ccbc9acdea44ab3c2462184381e4d1c1c3707d", null ],
      [ "eSHIFT_VERTICES", "structphysx_1_1_px_convex_flag.html#a90b3c67ca51c9757220b42a2a59ccbc9a214b28e9ba7ba43a6b76aa8ba33bafa8", null ]
    ] ]
];