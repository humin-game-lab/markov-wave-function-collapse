var structphysx_1_1_px_contact_pair_header =
[
    [ "PxContactPairHeader", "structphysx_1_1_px_contact_pair_header.html#aec1f2d72f8de15a52e13f49d00d9542d", null ],
    [ "actors", "structphysx_1_1_px_contact_pair_header.html#a81b365cf775b67bd9a5c0cb0de11b7d3", null ],
    [ "extraDataStream", "structphysx_1_1_px_contact_pair_header.html#a02fbffe985ae821db91b7a7e68d61c59", null ],
    [ "extraDataStreamSize", "structphysx_1_1_px_contact_pair_header.html#a85511ce0cf6c77dbe35e3debaf776d4e", null ],
    [ "flags", "structphysx_1_1_px_contact_pair_header.html#a9860b397d65e2dfc61aa02232b21c30a", null ],
    [ "nbPairs", "structphysx_1_1_px_contact_pair_header.html#aa2dd0fb3d8991addc001fa06170fb88b", null ],
    [ "pairs", "structphysx_1_1_px_contact_pair_header.html#af013c626a7603b8afc712058f19ed880", null ]
];