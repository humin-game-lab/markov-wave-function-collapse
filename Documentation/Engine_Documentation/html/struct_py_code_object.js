var struct_py_code_object =
[
    [ "co_argcount", "struct_py_code_object.html#adc0dadab6a4224c47cfccb332b2bd0b3", null ],
    [ "co_cell2arg", "struct_py_code_object.html#a8c26993d7022e487752d9e387bb125ad", null ],
    [ "co_cellvars", "struct_py_code_object.html#ab08cf541a92d6fc56af2cc1419816e23", null ],
    [ "co_code", "struct_py_code_object.html#a6f3c6ad3ccefda60ce7033c7375c17b0", null ],
    [ "co_consts", "struct_py_code_object.html#a4a19803042153ef763c58ab0980eb775", null ],
    [ "co_extra", "struct_py_code_object.html#a591f7a349f6de6df019aea28c3edddf2", null ],
    [ "co_filename", "struct_py_code_object.html#a15d8ea3dbc511eccdafc0cc0743632aa", null ],
    [ "co_firstlineno", "struct_py_code_object.html#a068116d5abe3d1803aec12f31a638234", null ],
    [ "co_flags", "struct_py_code_object.html#a678b33effc331fb223622fd7aea8fac3", null ],
    [ "co_freevars", "struct_py_code_object.html#a89e2b8f2d3e34ce6b6a866da47c5ddbb", null ],
    [ "co_kwonlyargcount", "struct_py_code_object.html#a98955a4296f5c023291d467f81d50a1f", null ],
    [ "co_lnotab", "struct_py_code_object.html#ac99c01e278a9cabed14aac6713f18bf6", null ],
    [ "co_name", "struct_py_code_object.html#a10bd7a8a3f23047cdb6eb10e032176bf", null ],
    [ "co_names", "struct_py_code_object.html#a830824fae12a5ef716896fa78a84adf2", null ],
    [ "co_nlocals", "struct_py_code_object.html#a9365db6f0ac59f02a907b6bc41924f4c", null ],
    [ "co_stacksize", "struct_py_code_object.html#ab171343772772be7a4911c28f96e415f", null ],
    [ "co_varnames", "struct_py_code_object.html#aff29dff0e2f511c562f3bc9a322321d0", null ],
    [ "co_weakreflist", "struct_py_code_object.html#a0240509a49ab73c5e157dc75c2ae88d7", null ],
    [ "co_zombieframe", "struct_py_code_object.html#ac08ff386d0019fdf3a6b455ca1e53ff9", null ]
];