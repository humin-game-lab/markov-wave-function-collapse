var _profiler_enums_8hpp =
[
    [ "ReportSortMode", "_profiler_enums_8hpp.html#a419b755613092b4c3c83584994532a58", [
      [ "SORT_BY_NONE", "_profiler_enums_8hpp.html#a419b755613092b4c3c83584994532a58ae0e4125ddc38ea550a3d04846deaae04", null ],
      [ "SORT_BY_TOTAL_TIME", "_profiler_enums_8hpp.html#a419b755613092b4c3c83584994532a58a56d2cfc4b89c3b0673d989aa976ac4b0", null ],
      [ "SORT_BY_SELF_TIME", "_profiler_enums_8hpp.html#a419b755613092b4c3c83584994532a58a3720a532a651f85f732aa64bd15ae049", null ],
      [ "NUM_SORT_MODES", "_profiler_enums_8hpp.html#a419b755613092b4c3c83584994532a58a7f3cfed51d3f07ceb3722f4ca5da2056", null ]
    ] ],
    [ "ReportType", "_profiler_enums_8hpp.html#ae4f179a72db5cca021a37ec4c6fe4c68", [
      [ "REPORT_TYPE_FLAT", "_profiler_enums_8hpp.html#ae4f179a72db5cca021a37ec4c6fe4c68a7f8296df19c0ff0826d971892232b9ce", null ],
      [ "REPORT_TYPE_TREE", "_profiler_enums_8hpp.html#ae4f179a72db5cca021a37ec4c6fe4c68ab544705fed282d5b44ac2feb07a9a5a0", null ],
      [ "NUM_REPORT_TYPES", "_profiler_enums_8hpp.html#ae4f179a72db5cca021a37ec4c6fe4c68a5dc299d8ce74362acba21eb8fc77aa3c", null ]
    ] ]
];