var structphysx_1_1_px_geometry_type =
[
    [ "Enum", "structphysx_1_1_px_geometry_type.html#a97a4db6d146f69095893d808ca757173", [
      [ "eSPHERE", "structphysx_1_1_px_geometry_type.html#a97a4db6d146f69095893d808ca757173abfbb5980aed9e30021fc8897085ac6c9", null ],
      [ "ePLANE", "structphysx_1_1_px_geometry_type.html#a97a4db6d146f69095893d808ca757173aa614bfb6a9850ad20d6e78cbbe357dd0", null ],
      [ "eCAPSULE", "structphysx_1_1_px_geometry_type.html#a97a4db6d146f69095893d808ca757173a553789bf0b16769b391e0c2414a720f3", null ],
      [ "eBOX", "structphysx_1_1_px_geometry_type.html#a97a4db6d146f69095893d808ca757173afc82a0eb779385ac6da5c979af0f41cb", null ],
      [ "eCONVEXMESH", "structphysx_1_1_px_geometry_type.html#a97a4db6d146f69095893d808ca757173ac883d7fd4d9bbc23359d60cfb68c2bed", null ],
      [ "eTRIANGLEMESH", "structphysx_1_1_px_geometry_type.html#a97a4db6d146f69095893d808ca757173a28083ab8f17a99e5d384b1cf4db4e65a", null ],
      [ "eHEIGHTFIELD", "structphysx_1_1_px_geometry_type.html#a97a4db6d146f69095893d808ca757173a31fad23db8cca7c644e52dbcd2d9a8f0", null ],
      [ "eGEOMETRY_COUNT", "structphysx_1_1_px_geometry_type.html#a97a4db6d146f69095893d808ca757173a7a793d019a7252c48944983feb0dc965", null ],
      [ "eINVALID", "structphysx_1_1_px_geometry_type.html#a97a4db6d146f69095893d808ca757173af00278b28b653fcbef0dd935a1d6affb", null ]
    ] ]
];