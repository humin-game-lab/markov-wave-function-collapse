var fmod__dsp__effects_8h =
[
    [ "FMOD_DSP_MULTIBAND_EQ", "fmod__dsp__effects_8h.html#abd607fb7de4a4d37b2aff46578ed659c", null ],
    [ "FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE", "fmod__dsp__effects_8h.html#a2037d7a27fb80a5e72af167085856597", null ],
    [ "FMOD_DSP_CHANNELMIX", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0", [
      [ "FMOD_DSP_CHANNELMIX_OUTPUTGROUPING", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a536705d26626c342ed41509b9ce85c44", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH0", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a16ab16f7b8554e2b2e8ed182db96b565", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH1", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0aba55e47fee7b5a053ea428c45b17a985", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH2", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a3de06a6378ff1ab9804f7aafa3ee1cbb", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH3", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a6d85ec33e34cc0d166f65695839186bf", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH4", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a7de55e6ad43304a1d58fbee7791ed3e0", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH5", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a780180e2d3b53d1b6c8896d3212ce9d2", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH6", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0ad365ec8097bdc11cdf82e973b19bd090", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH7", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0ad2ea892abbd9e0d15108ec4299965c35", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH8", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a4f4cb0a5a4bcf5f0d4ac76d3d45ac5d3", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH9", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a99a60249751e8628300fe48534857c1a", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH10", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0abd6dfb90d16b4587a9e86541379e8907", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH11", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a1346b3e6e7a1ccc1aa342fc8f37ffd50", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH12", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a1dd982bc60baa950f01d4608d1d23c8e", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH13", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a0d6fa408af036e304bf6d36ed7b5b7ee", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH14", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a518aba4c844dec7e0dfd6dcf95f240fe", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH15", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0aa1e2f90f96b79e12e600ee8b61632c82", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH16", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a98171ffe1e7a4ba2105837ea93ac0870", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH17", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a6a0fdf1bd4408eccc54b31b5747b105c", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH18", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a05ea6cb03196f2d96fc1b4d00593c7ee", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH19", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0ac1ef5023b42589c3307985866ad3dfd1", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH20", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0ab021c7db8a34a5dd6af4c4c895d62cbf", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH21", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a27f5b63d0beac8686fda5b70a2ac35f5", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH22", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0ae9deecbb91d7b12dffa2c474ae7e4472", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH23", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a2cef954cf3809999fbaf7805bc615b51", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH24", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0ab9590ed1807fe2943e9fd9ac564fd737", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH25", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0adab6eecaadeaf7e8d44cd784905d40f4", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH26", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0ad89f3e5d8860ca526c92f64e4f0f6bed", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH27", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a2fcdf5e14d867a2a40cc4aa59766c4f5", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH28", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a450c815af81c65fa2ed51aaa07829613", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH29", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a0fe92ffe8e8e27295b242d669453da8a", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH30", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0a7dca6def990d1643e8422286c6d72cad", null ],
      [ "FMOD_DSP_CHANNELMIX_GAIN_CH31", "fmod__dsp__effects_8h.html#adf24c01419abd311158495f3b5ea1bd0ae6c71a03a89cc5b05ae8e3cf356f7dd6", null ]
    ] ],
    [ "FMOD_DSP_CHANNELMIX_OUTPUT", "fmod__dsp__effects_8h.html#a847d6b6923b1f030939ce3f08341d5fd", [
      [ "FMOD_DSP_CHANNELMIX_OUTPUT_DEFAULT", "fmod__dsp__effects_8h.html#a847d6b6923b1f030939ce3f08341d5fda251c685187d1d6ac9bd1ea6a30fd601c", null ],
      [ "FMOD_DSP_CHANNELMIX_OUTPUT_ALLMONO", "fmod__dsp__effects_8h.html#a847d6b6923b1f030939ce3f08341d5fda2b228d3319f862571cf504fc8d6fd73c", null ],
      [ "FMOD_DSP_CHANNELMIX_OUTPUT_ALLSTEREO", "fmod__dsp__effects_8h.html#a847d6b6923b1f030939ce3f08341d5fda5e5bdbaf401b67cd3e18633c8288eb18", null ],
      [ "FMOD_DSP_CHANNELMIX_OUTPUT_ALLQUAD", "fmod__dsp__effects_8h.html#a847d6b6923b1f030939ce3f08341d5fda1766de55989b424af8f9d2eaa90d0885", null ],
      [ "FMOD_DSP_CHANNELMIX_OUTPUT_ALL5POINT1", "fmod__dsp__effects_8h.html#a847d6b6923b1f030939ce3f08341d5fda4c05d4f197287c6bb617209bcf9f2e1b", null ],
      [ "FMOD_DSP_CHANNELMIX_OUTPUT_ALL7POINT1", "fmod__dsp__effects_8h.html#a847d6b6923b1f030939ce3f08341d5fdabababdf89ba55464cb2a6ee29b542167", null ],
      [ "FMOD_DSP_CHANNELMIX_OUTPUT_ALLLFE", "fmod__dsp__effects_8h.html#a847d6b6923b1f030939ce3f08341d5fda98b57a6fbf7d7ffec1ce891039289540", null ]
    ] ],
    [ "FMOD_DSP_CHORUS", "fmod__dsp__effects_8h.html#ae7bfa7c447d4a0638f046e63754109c2", [
      [ "FMOD_DSP_CHORUS_MIX", "fmod__dsp__effects_8h.html#ae7bfa7c447d4a0638f046e63754109c2a3e581bd8e7b3b34c8c81141921addac8", null ],
      [ "FMOD_DSP_CHORUS_RATE", "fmod__dsp__effects_8h.html#ae7bfa7c447d4a0638f046e63754109c2aa7ba45aa4cd5c2cc3a7e5003b4c78c88", null ],
      [ "FMOD_DSP_CHORUS_DEPTH", "fmod__dsp__effects_8h.html#ae7bfa7c447d4a0638f046e63754109c2a1104ed8acc4ed4218b4bd512e175f285", null ]
    ] ],
    [ "FMOD_DSP_COMPRESSOR", "fmod__dsp__effects_8h.html#aee1246974010b4e20a9f8bbd92acfff1", [
      [ "FMOD_DSP_COMPRESSOR_THRESHOLD", "fmod__dsp__effects_8h.html#aee1246974010b4e20a9f8bbd92acfff1a9d3d58e72a332e1dc684fc40c85c28e1", null ],
      [ "FMOD_DSP_COMPRESSOR_RATIO", "fmod__dsp__effects_8h.html#aee1246974010b4e20a9f8bbd92acfff1a642c1c8c9236c59a22237da4ed4e7117", null ],
      [ "FMOD_DSP_COMPRESSOR_ATTACK", "fmod__dsp__effects_8h.html#aee1246974010b4e20a9f8bbd92acfff1ab9e48b1bf1d2e3d9bf081a4c24839b1e", null ],
      [ "FMOD_DSP_COMPRESSOR_RELEASE", "fmod__dsp__effects_8h.html#aee1246974010b4e20a9f8bbd92acfff1a8b094449c7f0d540dc5cf86c469c1eca", null ],
      [ "FMOD_DSP_COMPRESSOR_GAINMAKEUP", "fmod__dsp__effects_8h.html#aee1246974010b4e20a9f8bbd92acfff1a3c56c7371d5d1a6be48a123ef8d59178", null ],
      [ "FMOD_DSP_COMPRESSOR_USESIDECHAIN", "fmod__dsp__effects_8h.html#aee1246974010b4e20a9f8bbd92acfff1a15da3608b1715a69cf623ab282547544", null ],
      [ "FMOD_DSP_COMPRESSOR_LINKED", "fmod__dsp__effects_8h.html#aee1246974010b4e20a9f8bbd92acfff1ad285385ac899cee3518b645bfe9dee58", null ]
    ] ],
    [ "FMOD_DSP_CONVOLUTION_REVERB", "fmod__dsp__effects_8h.html#ae011400c2a1b8f66d4db7fc4bc5321b0", [
      [ "FMOD_DSP_CONVOLUTION_REVERB_PARAM_IR", "fmod__dsp__effects_8h.html#ae011400c2a1b8f66d4db7fc4bc5321b0a7867b08f666e49ca7be0b82446b9f6f4", null ],
      [ "FMOD_DSP_CONVOLUTION_REVERB_PARAM_WET", "fmod__dsp__effects_8h.html#ae011400c2a1b8f66d4db7fc4bc5321b0a33c65d982c0c78b604326741e5b0fcb5", null ],
      [ "FMOD_DSP_CONVOLUTION_REVERB_PARAM_DRY", "fmod__dsp__effects_8h.html#ae011400c2a1b8f66d4db7fc4bc5321b0a6bf5950b4ab5db6e5e1ec7de6dfe5fee", null ],
      [ "FMOD_DSP_CONVOLUTION_REVERB_PARAM_LINKED", "fmod__dsp__effects_8h.html#ae011400c2a1b8f66d4db7fc4bc5321b0a984fd17d5f3cc4e37ae8038ab4837918", null ]
    ] ],
    [ "FMOD_DSP_DELAY", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4", [
      [ "FMOD_DSP_DELAY_CH0", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4afc1c72489b30fa77c7f1fad2d890bdb3", null ],
      [ "FMOD_DSP_DELAY_CH1", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4a4eb4ca33531fe822e255893c51046dff", null ],
      [ "FMOD_DSP_DELAY_CH2", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4a214d37c3291866d6a6fafca977a36fe6", null ],
      [ "FMOD_DSP_DELAY_CH3", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4a28b429971c8300f57f2c1a3de520e10c", null ],
      [ "FMOD_DSP_DELAY_CH4", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4a143470c8d0ccdbdbca951536bf7c6c92", null ],
      [ "FMOD_DSP_DELAY_CH5", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4aab6c9e840f5c61857827f3e370180eac", null ],
      [ "FMOD_DSP_DELAY_CH6", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4aa7abb5e19b4825ec07fdbcc1f0340b31", null ],
      [ "FMOD_DSP_DELAY_CH7", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4a59753f93211719e0c1ba503a73b4eda0", null ],
      [ "FMOD_DSP_DELAY_CH8", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4a2f27d4d9a068b11523d5ab9f02bb5287", null ],
      [ "FMOD_DSP_DELAY_CH9", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4aeb78920e5c8afb3156fd51e7bb14d55a", null ],
      [ "FMOD_DSP_DELAY_CH10", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4ac03a95672943e003502563253f75546c", null ],
      [ "FMOD_DSP_DELAY_CH11", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4a0650bb4e7a686bcca5287eea0585ced2", null ],
      [ "FMOD_DSP_DELAY_CH12", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4a9510cd5833b7c02e817c2527ecd4700d", null ],
      [ "FMOD_DSP_DELAY_CH13", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4ac53c6d891077cddcb45fcdaddd5a80d3", null ],
      [ "FMOD_DSP_DELAY_CH14", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4aebafb1f7238322d8ed4015bceac2ffe6", null ],
      [ "FMOD_DSP_DELAY_CH15", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4a5fc50227cb24bbd0f8835e3834a2ebde", null ],
      [ "FMOD_DSP_DELAY_MAXDELAY", "fmod__dsp__effects_8h.html#aed214f9373e989acb9ebc0e33ac28ca4af402fbd1583fdbf365b69072e09e0bf5", null ]
    ] ],
    [ "FMOD_DSP_DISTORTION", "fmod__dsp__effects_8h.html#ae2c2a0208771516bc89f7d931136a39f", [
      [ "FMOD_DSP_DISTORTION_LEVEL", "fmod__dsp__effects_8h.html#ae2c2a0208771516bc89f7d931136a39fa6785fcdd773ae75f99f353aafbea0032", null ]
    ] ],
    [ "FMOD_DSP_ECHO", "fmod__dsp__effects_8h.html#af1a7917f6f88c144ee8e046349d17621", [
      [ "FMOD_DSP_ECHO_DELAY", "fmod__dsp__effects_8h.html#af1a7917f6f88c144ee8e046349d17621ac207d53d3a166342c3201489878d6538", null ],
      [ "FMOD_DSP_ECHO_FEEDBACK", "fmod__dsp__effects_8h.html#af1a7917f6f88c144ee8e046349d17621a3554eb5a8bfc03ae5b9686d09cf523e6", null ],
      [ "FMOD_DSP_ECHO_DRYLEVEL", "fmod__dsp__effects_8h.html#af1a7917f6f88c144ee8e046349d17621aa26d2e673a704f6d83f00d48b0f73ed5", null ],
      [ "FMOD_DSP_ECHO_WETLEVEL", "fmod__dsp__effects_8h.html#af1a7917f6f88c144ee8e046349d17621a92b584ee70ae1aed930507db5164882c", null ]
    ] ],
    [ "FMOD_DSP_ENVELOPEFOLLOWER", "fmod__dsp__effects_8h.html#a451ccc15abd4a31a1e68bc46e5383a0f", [
      [ "FMOD_DSP_ENVELOPEFOLLOWER_ATTACK", "fmod__dsp__effects_8h.html#a451ccc15abd4a31a1e68bc46e5383a0faa8985fc3afcbd9837921b8a01480f884", null ],
      [ "FMOD_DSP_ENVELOPEFOLLOWER_RELEASE", "fmod__dsp__effects_8h.html#a451ccc15abd4a31a1e68bc46e5383a0fab0a438484b1465fcc42bb313d652aa3c", null ],
      [ "FMOD_DSP_ENVELOPEFOLLOWER_ENVELOPE", "fmod__dsp__effects_8h.html#a451ccc15abd4a31a1e68bc46e5383a0faad8640654a651ddf27ee0728e37a01c4", null ],
      [ "FMOD_DSP_ENVELOPEFOLLOWER_USESIDECHAIN", "fmod__dsp__effects_8h.html#a451ccc15abd4a31a1e68bc46e5383a0fae7f19fd8fd090b05ec99edfbfb6d8dbe", null ]
    ] ],
    [ "FMOD_DSP_FFT", "fmod__dsp__effects_8h.html#abe26b54425cb5276a37c5007cf34a4b6", [
      [ "FMOD_DSP_FFT_WINDOWSIZE", "fmod__dsp__effects_8h.html#abe26b54425cb5276a37c5007cf34a4b6a0e888e9d7f44578e30fc5d3aa29ed735", null ],
      [ "FMOD_DSP_FFT_WINDOWTYPE", "fmod__dsp__effects_8h.html#abe26b54425cb5276a37c5007cf34a4b6ab23ef67ed4e25693766e03ba2c170203", null ],
      [ "FMOD_DSP_FFT_SPECTRUMDATA", "fmod__dsp__effects_8h.html#abe26b54425cb5276a37c5007cf34a4b6ac6cdce73d585ac97995e7894fa130e54", null ],
      [ "FMOD_DSP_FFT_DOMINANT_FREQ", "fmod__dsp__effects_8h.html#abe26b54425cb5276a37c5007cf34a4b6a3458f5b5baa3aafe0b771c94f8d2e853", null ]
    ] ],
    [ "FMOD_DSP_FFT_WINDOW", "fmod__dsp__effects_8h.html#a367a057220514092d4e981e8c45a998b", [
      [ "FMOD_DSP_FFT_WINDOW_RECT", "fmod__dsp__effects_8h.html#a367a057220514092d4e981e8c45a998ba628ba79e7854d2b4936d384eae4bcff5", null ],
      [ "FMOD_DSP_FFT_WINDOW_TRIANGLE", "fmod__dsp__effects_8h.html#a367a057220514092d4e981e8c45a998ba0d4ef86179530ba1d84461a0da4ee13f", null ],
      [ "FMOD_DSP_FFT_WINDOW_HAMMING", "fmod__dsp__effects_8h.html#a367a057220514092d4e981e8c45a998baaa0acf75621cfa8b0eae4f498829827e", null ],
      [ "FMOD_DSP_FFT_WINDOW_HANNING", "fmod__dsp__effects_8h.html#a367a057220514092d4e981e8c45a998ba2d25d279e2686d455e23f56aa8fb1c0d", null ],
      [ "FMOD_DSP_FFT_WINDOW_BLACKMAN", "fmod__dsp__effects_8h.html#a367a057220514092d4e981e8c45a998ba7893fa2fbdb19cdb399dbd063db5ccbb", null ],
      [ "FMOD_DSP_FFT_WINDOW_BLACKMANHARRIS", "fmod__dsp__effects_8h.html#a367a057220514092d4e981e8c45a998ba5af938662aedc43066ca818e543427f6", null ]
    ] ],
    [ "FMOD_DSP_FLANGE", "fmod__dsp__effects_8h.html#a0b806db4dcc42d5dc2948db03bd0888e", [
      [ "FMOD_DSP_FLANGE_MIX", "fmod__dsp__effects_8h.html#a0b806db4dcc42d5dc2948db03bd0888ead61760810b067417444f3b1733edb252", null ],
      [ "FMOD_DSP_FLANGE_DEPTH", "fmod__dsp__effects_8h.html#a0b806db4dcc42d5dc2948db03bd0888ea69f9f9a3482f8cab2cb2d9bf167b5043", null ],
      [ "FMOD_DSP_FLANGE_RATE", "fmod__dsp__effects_8h.html#a0b806db4dcc42d5dc2948db03bd0888ea05abcb17b4926b1cdcc5ef0dd30fa0c6", null ]
    ] ],
    [ "FMOD_DSP_HIGHPASS", "fmod__dsp__effects_8h.html#ac0ed6e1d9c4ab25a97a45806bebad026", [
      [ "FMOD_DSP_HIGHPASS_CUTOFF", "fmod__dsp__effects_8h.html#ac0ed6e1d9c4ab25a97a45806bebad026a8b1c1518dfd665472a5467570b9808d3", null ],
      [ "FMOD_DSP_HIGHPASS_RESONANCE", "fmod__dsp__effects_8h.html#ac0ed6e1d9c4ab25a97a45806bebad026a4f6db7f0b76538a47c10e99d25370598", null ]
    ] ],
    [ "FMOD_DSP_HIGHPASS_SIMPLE", "fmod__dsp__effects_8h.html#a45a511064aa9ff34e494168de2952671", [
      [ "FMOD_DSP_HIGHPASS_SIMPLE_CUTOFF", "fmod__dsp__effects_8h.html#a45a511064aa9ff34e494168de2952671a02124a23ca8240fa26b5cf61bc65d7a3", null ]
    ] ],
    [ "FMOD_DSP_ITECHO", "fmod__dsp__effects_8h.html#a30604d16248cbf0c6542e247871bd32d", [
      [ "FMOD_DSP_ITECHO_WETDRYMIX", "fmod__dsp__effects_8h.html#a30604d16248cbf0c6542e247871bd32dae1f7acbeff0cbe7f2cb4d702552e53c1", null ],
      [ "FMOD_DSP_ITECHO_FEEDBACK", "fmod__dsp__effects_8h.html#a30604d16248cbf0c6542e247871bd32dae7c5a0ae8f52e4c61ad8ad619fb6763b", null ],
      [ "FMOD_DSP_ITECHO_LEFTDELAY", "fmod__dsp__effects_8h.html#a30604d16248cbf0c6542e247871bd32dada902c66dc51b0f2a8e89b300a72fe26", null ],
      [ "FMOD_DSP_ITECHO_RIGHTDELAY", "fmod__dsp__effects_8h.html#a30604d16248cbf0c6542e247871bd32daee01306878d1aeca328e8a72e4981902", null ],
      [ "FMOD_DSP_ITECHO_PANDELAY", "fmod__dsp__effects_8h.html#a30604d16248cbf0c6542e247871bd32dac5388c31345a0f6b556b925a61bdb820", null ]
    ] ],
    [ "FMOD_DSP_ITLOWPASS", "fmod__dsp__effects_8h.html#a09207ed68e19f4391b8abd0f3dcf20ae", [
      [ "FMOD_DSP_ITLOWPASS_CUTOFF", "fmod__dsp__effects_8h.html#a09207ed68e19f4391b8abd0f3dcf20aea8a663d2002a57768b694b6f238a8aaa7", null ],
      [ "FMOD_DSP_ITLOWPASS_RESONANCE", "fmod__dsp__effects_8h.html#a09207ed68e19f4391b8abd0f3dcf20aeafc514a6c36d7a1a87b8e223afbdaba8f", null ]
    ] ],
    [ "FMOD_DSP_LIMITER", "fmod__dsp__effects_8h.html#a9aab20cf989224369c3c883006ac4629", [
      [ "FMOD_DSP_LIMITER_RELEASETIME", "fmod__dsp__effects_8h.html#a9aab20cf989224369c3c883006ac4629a680fa4550ff331fde2c326d900e8120e", null ],
      [ "FMOD_DSP_LIMITER_CEILING", "fmod__dsp__effects_8h.html#a9aab20cf989224369c3c883006ac4629a3a6b7c985cb9304a940e6a804fa456f5", null ],
      [ "FMOD_DSP_LIMITER_MAXIMIZERGAIN", "fmod__dsp__effects_8h.html#a9aab20cf989224369c3c883006ac4629a6fea681aa50be451ebd073424fb40812", null ],
      [ "FMOD_DSP_LIMITER_MODE", "fmod__dsp__effects_8h.html#a9aab20cf989224369c3c883006ac4629a1789196a75446e3cb5ea7d791cf85f7a", null ]
    ] ],
    [ "FMOD_DSP_LOWPASS", "fmod__dsp__effects_8h.html#ad341dcdfe6e3a946bc3a2d070e49f006", [
      [ "FMOD_DSP_LOWPASS_CUTOFF", "fmod__dsp__effects_8h.html#ad341dcdfe6e3a946bc3a2d070e49f006a33aecb16ae447e98f94dbf0ad3f11e05", null ],
      [ "FMOD_DSP_LOWPASS_RESONANCE", "fmod__dsp__effects_8h.html#ad341dcdfe6e3a946bc3a2d070e49f006ab07637bad3849196d66dcfff6deadeb5", null ]
    ] ],
    [ "FMOD_DSP_LOWPASS_SIMPLE", "fmod__dsp__effects_8h.html#a865229299864dfa430c0cee71de6d517", [
      [ "FMOD_DSP_LOWPASS_SIMPLE_CUTOFF", "fmod__dsp__effects_8h.html#a865229299864dfa430c0cee71de6d517a830e87f58eb28e5c5b09ac1ef4227cac", null ]
    ] ],
    [ "FMOD_DSP_MULTIBAND_EQ", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6", [
      [ "FMOD_DSP_MULTIBAND_EQ_A_FILTER", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a4f6423af157632e9f4bce73edfd495c9", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_A_FREQUENCY", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a64f4574a8e57d58de59fa32b5897c0ad", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_A_Q", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6abc290a1d467aa7e8d40297f1ca7fc9fd", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_A_GAIN", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a234f2159dd169ef433730d4d2c9b4962", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_B_FILTER", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a4118f86b674bcf9ff5146f8793276014", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_B_FREQUENCY", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6abf07080b74dd39d795b34a6124996c2b", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_B_Q", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6af944311cef8f74972ec5fe39c9ec2eb2", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_B_GAIN", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a5d15c87fa654b186a5b6dd9852ed9767", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_C_FILTER", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a95893b37bb86df2f101161cdcb9e6768", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_C_FREQUENCY", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a6d6bb6cb9cd7f62331cb31ff68e058f1", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_C_Q", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a2ef40b196ee0f82db09026b17c3085e1", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_C_GAIN", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a6f2cbcb34cdf0355433d6016147f5bce", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_D_FILTER", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6ac8291d8e45bcad8e6065df9f3e683565", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_D_FREQUENCY", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6ae6f103534a45908b947f9d9ab9f57d24", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_D_Q", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6ae714c265e3dc55c6b911fb41497ee888", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_D_GAIN", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6ae7b9562956db7e68692bf6fb35fae957", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_E_FILTER", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a6a02a27caa5a68c14e28b2d58517d3ff", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_E_FREQUENCY", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a84ddc72493ebe8e0ee24fd05b2964307", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_E_Q", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6aa0d18b19455aacfcb2b9ee35c3625583", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_E_GAIN", "fmod__dsp__effects_8h.html#a8c35baf1f5f5f4c76adad76349bbe5f6a26fc1ef0a27274b5a5d175dbb1f7b896", null ]
    ] ],
    [ "FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45", [
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_DISABLED", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45ab94bb44b82f42dcf3eefdc66250d9e14", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_LOWPASS_12DB", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45aa6c8a59e7869ce8934ff0848b3a67f01", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_LOWPASS_24DB", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45ab456d29aa30c8b4a38bd4ad7d4473db0", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_LOWPASS_48DB", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45a1114da64cddce378f52d593f350fa82e", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_HIGHPASS_12DB", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45ab86dcd63236a59ab52eabb2bb2a5d3c1", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_HIGHPASS_24DB", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45a7ae33aff4dee797750818834478d19df", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_HIGHPASS_48DB", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45a0da0aa784e9b00b404f89165ba796aa3", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_LOWSHELF", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45aeb2dbd41a0afc126ba5b983e59238397", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_HIGHSHELF", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45af4aa146b455026faa3a0fd1a56ee5b13", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_PEAKING", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45a7fdb7bc79c1dc6fb6f45e690764952f4", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_BANDPASS", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45a38bfbfce9249089a32120f6f4ebf09cc", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_NOTCH", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45afa816a1af2f053a299711231e4a369c8", null ],
      [ "FMOD_DSP_MULTIBAND_EQ_FILTER_ALLPASS", "fmod__dsp__effects_8h.html#afe3a0d7ed8b574c5905d2057ef722c45aa6dbfe3d11e56fb1aaf0fce39a008964", null ]
    ] ],
    [ "FMOD_DSP_NORMALIZE", "fmod__dsp__effects_8h.html#a8f17dafccb488b329324d0a1d17e592b", [
      [ "FMOD_DSP_NORMALIZE_FADETIME", "fmod__dsp__effects_8h.html#a8f17dafccb488b329324d0a1d17e592ba8423a9844d6973b3895dbbfc38230042", null ],
      [ "FMOD_DSP_NORMALIZE_THRESHHOLD", "fmod__dsp__effects_8h.html#a8f17dafccb488b329324d0a1d17e592baf5afce8e160c3a43eaf2e38785f8887e", null ],
      [ "FMOD_DSP_NORMALIZE_MAXAMP", "fmod__dsp__effects_8h.html#a8f17dafccb488b329324d0a1d17e592ba91b7acd3b28759687ad629c36f8f1e9d", null ]
    ] ],
    [ "FMOD_DSP_OBJECTPAN", "fmod__dsp__effects_8h.html#aab4ae76b076d58d3cba3c45eb9cbe263", [
      [ "FMOD_DSP_OBJECTPAN_3D_POSITION", "fmod__dsp__effects_8h.html#aab4ae76b076d58d3cba3c45eb9cbe263ac34c5331e7d7374cf21f589636ec67cd", null ],
      [ "FMOD_DSP_OBJECTPAN_3D_ROLLOFF", "fmod__dsp__effects_8h.html#aab4ae76b076d58d3cba3c45eb9cbe263a54a99851fdf9c39217ba5dd4ccd8c9e5", null ],
      [ "FMOD_DSP_OBJECTPAN_3D_MIN_DISTANCE", "fmod__dsp__effects_8h.html#aab4ae76b076d58d3cba3c45eb9cbe263a1025d787b2282bdd61be442af03dc7ac", null ],
      [ "FMOD_DSP_OBJECTPAN_3D_MAX_DISTANCE", "fmod__dsp__effects_8h.html#aab4ae76b076d58d3cba3c45eb9cbe263a918acc53e9554d99b4b97e28d5e67627", null ],
      [ "FMOD_DSP_OBJECTPAN_3D_EXTENT_MODE", "fmod__dsp__effects_8h.html#aab4ae76b076d58d3cba3c45eb9cbe263a17e92ecaeb587d6836347abbf9d0dade", null ],
      [ "FMOD_DSP_OBJECTPAN_3D_SOUND_SIZE", "fmod__dsp__effects_8h.html#aab4ae76b076d58d3cba3c45eb9cbe263a675e716851cfdcca20218cd55c1a476d", null ],
      [ "FMOD_DSP_OBJECTPAN_3D_MIN_EXTENT", "fmod__dsp__effects_8h.html#aab4ae76b076d58d3cba3c45eb9cbe263a68926551959a51552044769381801428", null ],
      [ "FMOD_DSP_OBJECTPAN_OVERALL_GAIN", "fmod__dsp__effects_8h.html#aab4ae76b076d58d3cba3c45eb9cbe263a82954db16cff797cb4de9225c026da8d", null ],
      [ "FMOD_DSP_OBJECTPAN_OUTPUTGAIN", "fmod__dsp__effects_8h.html#aab4ae76b076d58d3cba3c45eb9cbe263af5718eaad1e8abf56de7bb6247fb3a99", null ]
    ] ],
    [ "FMOD_DSP_OSCILLATOR", "fmod__dsp__effects_8h.html#a23c51025928d05a5481fd31c20dffd9e", [
      [ "FMOD_DSP_OSCILLATOR_TYPE", "fmod__dsp__effects_8h.html#a23c51025928d05a5481fd31c20dffd9eac4b63bd61672c497815950ba0502eb7d", null ],
      [ "FMOD_DSP_OSCILLATOR_RATE", "fmod__dsp__effects_8h.html#a23c51025928d05a5481fd31c20dffd9eaad915d26366f7250885cdf1ac6998ea0", null ]
    ] ],
    [ "FMOD_DSP_PAN", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910", [
      [ "FMOD_DSP_PAN_MODE", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910aa9751ebf67fc4254b4907ad35f73c4eb", null ],
      [ "FMOD_DSP_PAN_2D_STEREO_POSITION", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a88ddeef52128268b90d26ac4f4ee9b8f", null ],
      [ "FMOD_DSP_PAN_2D_DIRECTION", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910af22aeb6c75e0026c2045bf8a56c81001", null ],
      [ "FMOD_DSP_PAN_2D_EXTENT", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910aefbfa1a3b86f258c54b129aa33248c4e", null ],
      [ "FMOD_DSP_PAN_2D_ROTATION", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a5ddde94c04485f7998ce7d7c6219f858", null ],
      [ "FMOD_DSP_PAN_2D_LFE_LEVEL", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a18289645e377d2251d3f66d4cce63ef7", null ],
      [ "FMOD_DSP_PAN_2D_STEREO_MODE", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a3e61620df7b3599250e14dfa07d1482d", null ],
      [ "FMOD_DSP_PAN_2D_STEREO_SEPARATION", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910aa28504ad4eeae1e5b6e8a369eb932f0b", null ],
      [ "FMOD_DSP_PAN_2D_STEREO_AXIS", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910ac6be430f8a9592a90e053a12d33e91dd", null ],
      [ "FMOD_DSP_PAN_ENABLED_SPEAKERS", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a4b09a4e35a4743826bbde273d3bebc54", null ],
      [ "FMOD_DSP_PAN_3D_POSITION", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a8224409671d9b1fbcc72f1acb50efd3e", null ],
      [ "FMOD_DSP_PAN_3D_ROLLOFF", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a11d0d2753b8b162c38cc0ae32deef2c5", null ],
      [ "FMOD_DSP_PAN_3D_MIN_DISTANCE", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a84432a27e674de64b57acf79602e842a", null ],
      [ "FMOD_DSP_PAN_3D_MAX_DISTANCE", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a8630322634e6d743cad81db5e2fc4b2a", null ],
      [ "FMOD_DSP_PAN_3D_EXTENT_MODE", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a4b2323241d39d26c539f2a91a249b887", null ],
      [ "FMOD_DSP_PAN_3D_SOUND_SIZE", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a0b0d0ddfd4276c9b4ebfd012fb58c77a", null ],
      [ "FMOD_DSP_PAN_3D_MIN_EXTENT", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a1a72adb44427b145842137af446f9233", null ],
      [ "FMOD_DSP_PAN_3D_PAN_BLEND", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a43b654c08356d2f54ee46bcb469ebc26", null ],
      [ "FMOD_DSP_PAN_LFE_UPMIX_ENABLED", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910ac24b416f064578c1b5d492499ffbde65", null ],
      [ "FMOD_DSP_PAN_OVERALL_GAIN", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a4d18d72106a4eafa5e7e4353c77fe317", null ],
      [ "FMOD_DSP_PAN_SURROUND_SPEAKER_MODE", "fmod__dsp__effects_8h.html#aec433597b557bdcd01522e478858b910a92bf90a84f75af4272f27760d13af6e8", null ]
    ] ],
    [ "FMOD_DSP_PAN_2D_STEREO_MODE_TYPE", "fmod__dsp__effects_8h.html#af10cd2b2d7528a3510dc2013aab2f40a", [
      [ "FMOD_DSP_PAN_2D_STEREO_MODE_DISTRIBUTED", "fmod__dsp__effects_8h.html#af10cd2b2d7528a3510dc2013aab2f40aa883358c12738f1526b3f0c9400cac1a7", null ],
      [ "FMOD_DSP_PAN_2D_STEREO_MODE_DISCRETE", "fmod__dsp__effects_8h.html#af10cd2b2d7528a3510dc2013aab2f40aa2e0d13eccdacbd7b01256695ef1b8786", null ]
    ] ],
    [ "FMOD_DSP_PAN_3D_EXTENT_MODE_TYPE", "fmod__dsp__effects_8h.html#a18b8596e7d24213110318d94809c6638", [
      [ "FMOD_DSP_PAN_3D_EXTENT_MODE_AUTO", "fmod__dsp__effects_8h.html#a18b8596e7d24213110318d94809c6638aa278e4ca888b968b42a15fe5da056b3a", null ],
      [ "FMOD_DSP_PAN_3D_EXTENT_MODE_USER", "fmod__dsp__effects_8h.html#a18b8596e7d24213110318d94809c6638aff3c474a7dc23a87970ab8fdc76a02f1", null ],
      [ "FMOD_DSP_PAN_3D_EXTENT_MODE_OFF", "fmod__dsp__effects_8h.html#a18b8596e7d24213110318d94809c6638af31710331dac28465e663964ca33ff81", null ]
    ] ],
    [ "FMOD_DSP_PAN_3D_ROLLOFF_TYPE", "fmod__dsp__effects_8h.html#afea1a6f23319326773571b33372cd609", [
      [ "FMOD_DSP_PAN_3D_ROLLOFF_LINEARSQUARED", "fmod__dsp__effects_8h.html#afea1a6f23319326773571b33372cd609a5d8a461ee97d0f629f96e552c94e7ad1", null ],
      [ "FMOD_DSP_PAN_3D_ROLLOFF_LINEAR", "fmod__dsp__effects_8h.html#afea1a6f23319326773571b33372cd609a351cccca537250137e563abadf90b958", null ],
      [ "FMOD_DSP_PAN_3D_ROLLOFF_INVERSE", "fmod__dsp__effects_8h.html#afea1a6f23319326773571b33372cd609a2fb15c12356d4ac2e004d0314ac5602d", null ],
      [ "FMOD_DSP_PAN_3D_ROLLOFF_INVERSETAPERED", "fmod__dsp__effects_8h.html#afea1a6f23319326773571b33372cd609a63b4dfbe824941fa24c6701780999de2", null ],
      [ "FMOD_DSP_PAN_3D_ROLLOFF_CUSTOM", "fmod__dsp__effects_8h.html#afea1a6f23319326773571b33372cd609a93ad7353fd70d6eabff459611515cf13", null ]
    ] ],
    [ "FMOD_DSP_PAN_MODE_TYPE", "fmod__dsp__effects_8h.html#ae2d5de9c6e1ca1bd3ad223142c389737", [
      [ "FMOD_DSP_PAN_MODE_MONO", "fmod__dsp__effects_8h.html#ae2d5de9c6e1ca1bd3ad223142c389737a2a62a33db24b8a6086f97f920afba376", null ],
      [ "FMOD_DSP_PAN_MODE_STEREO", "fmod__dsp__effects_8h.html#ae2d5de9c6e1ca1bd3ad223142c389737ae51d7c8747302965440fc50bab868aad", null ],
      [ "FMOD_DSP_PAN_MODE_SURROUND", "fmod__dsp__effects_8h.html#ae2d5de9c6e1ca1bd3ad223142c389737a81c00f277ab1b1420e5ba54251537634", null ]
    ] ],
    [ "FMOD_DSP_PARAMEQ", "fmod__dsp__effects_8h.html#a63e1f7924e0329de7ab0c53fb081aeb3", [
      [ "FMOD_DSP_PARAMEQ_CENTER", "fmod__dsp__effects_8h.html#a63e1f7924e0329de7ab0c53fb081aeb3a5776344a3022a59d8824282b242425f9", null ],
      [ "FMOD_DSP_PARAMEQ_BANDWIDTH", "fmod__dsp__effects_8h.html#a63e1f7924e0329de7ab0c53fb081aeb3ac8830edb5161bc4eb20f6d8afc7a4fa1", null ],
      [ "FMOD_DSP_PARAMEQ_GAIN", "fmod__dsp__effects_8h.html#a63e1f7924e0329de7ab0c53fb081aeb3a04f92ed8472012253db742265a394743", null ]
    ] ],
    [ "FMOD_DSP_PITCHSHIFT", "fmod__dsp__effects_8h.html#afe9bcffb55e5a00aec395d8e800c48c0", [
      [ "FMOD_DSP_PITCHSHIFT_PITCH", "fmod__dsp__effects_8h.html#afe9bcffb55e5a00aec395d8e800c48c0aa046a2faa407441b54ae6fcfd602c2ed", null ],
      [ "FMOD_DSP_PITCHSHIFT_FFTSIZE", "fmod__dsp__effects_8h.html#afe9bcffb55e5a00aec395d8e800c48c0a54c161a2ea84fa161eb64a214b72b010", null ],
      [ "FMOD_DSP_PITCHSHIFT_OVERLAP", "fmod__dsp__effects_8h.html#afe9bcffb55e5a00aec395d8e800c48c0a0818ccc7a63eaac1b96b2ec3ae9bb6e6", null ],
      [ "FMOD_DSP_PITCHSHIFT_MAXCHANNELS", "fmod__dsp__effects_8h.html#afe9bcffb55e5a00aec395d8e800c48c0ad2f58ca553f2ba63d058692b012352f1", null ]
    ] ],
    [ "FMOD_DSP_RETURN", "fmod__dsp__effects_8h.html#a1091d376571f87e10be1d7f98d77ccf7", [
      [ "FMOD_DSP_RETURN_ID", "fmod__dsp__effects_8h.html#a1091d376571f87e10be1d7f98d77ccf7abbe7d6b51ebac374e69bd2cd4548638c", null ],
      [ "FMOD_DSP_RETURN_INPUT_SPEAKER_MODE", "fmod__dsp__effects_8h.html#a1091d376571f87e10be1d7f98d77ccf7a00cd6b8160a0e6489be8a363ab35b878", null ]
    ] ],
    [ "FMOD_DSP_SEND", "fmod__dsp__effects_8h.html#a7a273bfaa1662f3bb47ff3c08f468b8d", [
      [ "FMOD_DSP_SEND_RETURNID", "fmod__dsp__effects_8h.html#a7a273bfaa1662f3bb47ff3c08f468b8da049c8aad50654d84fd24b139e8086f83", null ],
      [ "FMOD_DSP_SEND_LEVEL", "fmod__dsp__effects_8h.html#a7a273bfaa1662f3bb47ff3c08f468b8da3357902f33004800747e2d09d0619c4e", null ]
    ] ],
    [ "FMOD_DSP_SFXREVERB", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3", [
      [ "FMOD_DSP_SFXREVERB_DECAYTIME", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3a93d3beba9de5ed9091e2b925bf912411", null ],
      [ "FMOD_DSP_SFXREVERB_EARLYDELAY", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3aa50f8f9d73e92521f8a9a6e5ccdb3327", null ],
      [ "FMOD_DSP_SFXREVERB_LATEDELAY", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3a06ef13d6f2c7b33610ef421e05a01f84", null ],
      [ "FMOD_DSP_SFXREVERB_HFREFERENCE", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3a879e13f31fbab01eb43f821fc9b2c871", null ],
      [ "FMOD_DSP_SFXREVERB_HFDECAYRATIO", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3a2e18feb234d41ff0b35e9997040891d8", null ],
      [ "FMOD_DSP_SFXREVERB_DIFFUSION", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3a3d7a2bdfeaae06ce2b8e2a97216fa06b", null ],
      [ "FMOD_DSP_SFXREVERB_DENSITY", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3acbaf0daddb765332dde29ca5cc92f927", null ],
      [ "FMOD_DSP_SFXREVERB_LOWSHELFFREQUENCY", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3a0237b4cd6fa422eeaf8d3b4e30da65eb", null ],
      [ "FMOD_DSP_SFXREVERB_LOWSHELFGAIN", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3af81c96556744c3f4773d752e0357a0d0", null ],
      [ "FMOD_DSP_SFXREVERB_HIGHCUT", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3a874c3b8d8bc71951562065b647d1e8d0", null ],
      [ "FMOD_DSP_SFXREVERB_EARLYLATEMIX", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3a3753a3d98e7b1ae1720ea58322859bab", null ],
      [ "FMOD_DSP_SFXREVERB_WETLEVEL", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3a7edb0143a1417724557f5cd14953fa99", null ],
      [ "FMOD_DSP_SFXREVERB_DRYLEVEL", "fmod__dsp__effects_8h.html#a63244cb3b60c944dd5f1db3f7b9354f3ae729678c1002b3d83bc7b43208b3be0a", null ]
    ] ],
    [ "FMOD_DSP_THREE_EQ", "fmod__dsp__effects_8h.html#ad6d396a2fa01381763a825517e38dd60", [
      [ "FMOD_DSP_THREE_EQ_LOWGAIN", "fmod__dsp__effects_8h.html#ad6d396a2fa01381763a825517e38dd60a63a0048481e7d26bf18575fa90ede36f", null ],
      [ "FMOD_DSP_THREE_EQ_MIDGAIN", "fmod__dsp__effects_8h.html#ad6d396a2fa01381763a825517e38dd60a0502f262b0fc03a09d9fc5d877470d36", null ],
      [ "FMOD_DSP_THREE_EQ_HIGHGAIN", "fmod__dsp__effects_8h.html#ad6d396a2fa01381763a825517e38dd60a278c22d8ec6c6919ec9ad4a70c370555", null ],
      [ "FMOD_DSP_THREE_EQ_LOWCROSSOVER", "fmod__dsp__effects_8h.html#ad6d396a2fa01381763a825517e38dd60a7d142bdc245c10cb9305a4b064ea846d", null ],
      [ "FMOD_DSP_THREE_EQ_HIGHCROSSOVER", "fmod__dsp__effects_8h.html#ad6d396a2fa01381763a825517e38dd60abdc98cae340a4263e92e0e5eed3c0e3d", null ],
      [ "FMOD_DSP_THREE_EQ_CROSSOVERSLOPE", "fmod__dsp__effects_8h.html#ad6d396a2fa01381763a825517e38dd60a6aa33c9a05ce03f0f90b5d3c774636ab", null ]
    ] ],
    [ "FMOD_DSP_THREE_EQ_CROSSOVERSLOPE_TYPE", "fmod__dsp__effects_8h.html#a67f45c0064ac2fc0cf1db582178d2237", [
      [ "FMOD_DSP_THREE_EQ_CROSSOVERSLOPE_12DB", "fmod__dsp__effects_8h.html#a67f45c0064ac2fc0cf1db582178d2237a82ed035b885f77d343ae515a4e74b224", null ],
      [ "FMOD_DSP_THREE_EQ_CROSSOVERSLOPE_24DB", "fmod__dsp__effects_8h.html#a67f45c0064ac2fc0cf1db582178d2237ae1d991617ffed8fbd3b76b388b077315", null ],
      [ "FMOD_DSP_THREE_EQ_CROSSOVERSLOPE_48DB", "fmod__dsp__effects_8h.html#a67f45c0064ac2fc0cf1db582178d2237a3cf44027da8a9b5e37e48ebcfd083ffd", null ]
    ] ],
    [ "FMOD_DSP_TRANSCEIVER", "fmod__dsp__effects_8h.html#adc6eb9f989aaf5ba35050046d81dc310", [
      [ "FMOD_DSP_TRANSCEIVER_TRANSMIT", "fmod__dsp__effects_8h.html#adc6eb9f989aaf5ba35050046d81dc310a1a6fd836eaec161332711f8cca2241ec", null ],
      [ "FMOD_DSP_TRANSCEIVER_GAIN", "fmod__dsp__effects_8h.html#adc6eb9f989aaf5ba35050046d81dc310a8e69af330725e5462c83a575955f2b3f", null ],
      [ "FMOD_DSP_TRANSCEIVER_CHANNEL", "fmod__dsp__effects_8h.html#adc6eb9f989aaf5ba35050046d81dc310ab67ea5d6371b8e5c20e840084dd274dd", null ],
      [ "FMOD_DSP_TRANSCEIVER_TRANSMITSPEAKERMODE", "fmod__dsp__effects_8h.html#adc6eb9f989aaf5ba35050046d81dc310ab99d39a40b85404a59b1f3b10ad4c510", null ]
    ] ],
    [ "FMOD_DSP_TRANSCEIVER_SPEAKERMODE", "fmod__dsp__effects_8h.html#a83aabde8ec328c219bc237eb807e184d", [
      [ "FMOD_DSP_TRANSCEIVER_SPEAKERMODE_AUTO", "fmod__dsp__effects_8h.html#a83aabde8ec328c219bc237eb807e184da71e40dff0e2b1d918cb9d903b8661274", null ],
      [ "FMOD_DSP_TRANSCEIVER_SPEAKERMODE_MONO", "fmod__dsp__effects_8h.html#a83aabde8ec328c219bc237eb807e184da266eda79532f44fd0831f5692e41e5fb", null ],
      [ "FMOD_DSP_TRANSCEIVER_SPEAKERMODE_STEREO", "fmod__dsp__effects_8h.html#a83aabde8ec328c219bc237eb807e184dad4bc7723c06d9fb973a113263eb71d3d", null ],
      [ "FMOD_DSP_TRANSCEIVER_SPEAKERMODE_SURROUND", "fmod__dsp__effects_8h.html#a83aabde8ec328c219bc237eb807e184daa3750ef88d39f1c6c306a639afacb674", null ]
    ] ],
    [ "FMOD_DSP_TREMOLO", "fmod__dsp__effects_8h.html#ab0e1520abdad9681a707bba937c6f54d", [
      [ "FMOD_DSP_TREMOLO_FREQUENCY", "fmod__dsp__effects_8h.html#ab0e1520abdad9681a707bba937c6f54dae4620bbc4e0c766e76d89c93a5ab03e4", null ],
      [ "FMOD_DSP_TREMOLO_DEPTH", "fmod__dsp__effects_8h.html#ab0e1520abdad9681a707bba937c6f54da3de153a0f22c652c3c65fde8cee952e5", null ],
      [ "FMOD_DSP_TREMOLO_SHAPE", "fmod__dsp__effects_8h.html#ab0e1520abdad9681a707bba937c6f54dac624860df358e27dbe3f6e629ec753ef", null ],
      [ "FMOD_DSP_TREMOLO_SKEW", "fmod__dsp__effects_8h.html#ab0e1520abdad9681a707bba937c6f54daf0f8173c15fc2c61e12865789339b943", null ],
      [ "FMOD_DSP_TREMOLO_DUTY", "fmod__dsp__effects_8h.html#ab0e1520abdad9681a707bba937c6f54daf7b7ae4b0bf2da2b2b6e4bea895cdc49", null ],
      [ "FMOD_DSP_TREMOLO_SQUARE", "fmod__dsp__effects_8h.html#ab0e1520abdad9681a707bba937c6f54da2372379c1b6bf89bad19b2e994fd2a24", null ],
      [ "FMOD_DSP_TREMOLO_PHASE", "fmod__dsp__effects_8h.html#ab0e1520abdad9681a707bba937c6f54dad0ac92732969196fd1fa0c2e82d4afa4", null ],
      [ "FMOD_DSP_TREMOLO_SPREAD", "fmod__dsp__effects_8h.html#ab0e1520abdad9681a707bba937c6f54da394eb976c7598e0785c64b7d76283c0c", null ]
    ] ],
    [ "FMOD_DSP_TYPE", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3ae", [
      [ "FMOD_DSP_TYPE_UNKNOWN", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeaa64261adc7a64721bc4dbb5195ab76bb", null ],
      [ "FMOD_DSP_TYPE_MIXER", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea8b3f0e15ab677ccfbac18015241cbf53", null ],
      [ "FMOD_DSP_TYPE_OSCILLATOR", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aead92f2bf742699d311c2174785939bb55", null ],
      [ "FMOD_DSP_TYPE_LOWPASS", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea62fc813692ebc72e42f3c83b3cbb2694", null ],
      [ "FMOD_DSP_TYPE_ITLOWPASS", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeae229b40c3449172cd6b695b4bf1fb726", null ],
      [ "FMOD_DSP_TYPE_HIGHPASS", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeaecdacfccc3c85a61ea302b7c40608e4d", null ],
      [ "FMOD_DSP_TYPE_ECHO", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea00c667886510ba6e0efa030ef3599e06", null ],
      [ "FMOD_DSP_TYPE_FADER", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeaef774494c5d13ee930ef4a9d7b7fd3ad", null ],
      [ "FMOD_DSP_TYPE_FLANGE", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeab434d848ceda56235d444e60aec8584f", null ],
      [ "FMOD_DSP_TYPE_DISTORTION", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea3cbbbb41a6a218f22088da20aa4924e5", null ],
      [ "FMOD_DSP_TYPE_NORMALIZE", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea520018ccfa0c306458f896877e728fdf", null ],
      [ "FMOD_DSP_TYPE_LIMITER", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeaa6ca0b8cf06b0e42f652d984efa64c73", null ],
      [ "FMOD_DSP_TYPE_PARAMEQ", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea170f2d66fae089b8a99855a1ef854bc9", null ],
      [ "FMOD_DSP_TYPE_PITCHSHIFT", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeaeb5f500b3a0f4dc04ec9f6737220d8e9", null ],
      [ "FMOD_DSP_TYPE_CHORUS", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea64cd0f0e705b60351fea9f272df15296", null ],
      [ "FMOD_DSP_TYPE_VSTPLUGIN", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeae3a9e46db15de2876c4699b604abd737", null ],
      [ "FMOD_DSP_TYPE_WINAMPPLUGIN", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aead786512053317d61fbe8e70bac4d2b99", null ],
      [ "FMOD_DSP_TYPE_ITECHO", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea99fbf49cfbb17a593ace9194a5ccd3ab", null ],
      [ "FMOD_DSP_TYPE_COMPRESSOR", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea4c478cfceb6b82fccdc67c88e4b59208", null ],
      [ "FMOD_DSP_TYPE_SFXREVERB", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea42b0259f8b3be1248fda5f43a7c83fbe", null ],
      [ "FMOD_DSP_TYPE_LOWPASS_SIMPLE", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeadbd5d3a9316bc79a0ab793b7d822356d", null ],
      [ "FMOD_DSP_TYPE_DELAY", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea756245900cc236fb154b79f8ab869bea", null ],
      [ "FMOD_DSP_TYPE_TREMOLO", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea67c4a67025bd7477be5ae502eff203d1", null ],
      [ "FMOD_DSP_TYPE_LADSPAPLUGIN", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea5c27a6be6edc1f499b1909eed3cf6c60", null ],
      [ "FMOD_DSP_TYPE_SEND", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea56a8536914091e562b055cd2fa50bea9", null ],
      [ "FMOD_DSP_TYPE_RETURN", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeacbfae950b4fa665cc5d2050ce5e093f1", null ],
      [ "FMOD_DSP_TYPE_HIGHPASS_SIMPLE", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeaea61994ea80ec7ae3c16d829d8592b87", null ],
      [ "FMOD_DSP_TYPE_PAN", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea519218e0f6e5c7da626ff04f8996fdcf", null ],
      [ "FMOD_DSP_TYPE_THREE_EQ", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeaf358af45b9919d708a417ef8b133a579", null ],
      [ "FMOD_DSP_TYPE_FFT", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeadb350e470c3f29f35b2b2559ebba1381", null ],
      [ "FMOD_DSP_TYPE_LOUDNESS_METER", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea085b6a046a9463f5eb1050a8df29ef10", null ],
      [ "FMOD_DSP_TYPE_ENVELOPEFOLLOWER", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea253785a9cd482477db80f723105f362e", null ],
      [ "FMOD_DSP_TYPE_CONVOLUTIONREVERB", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea79e0099efb2e135d3857ce1722ba720e", null ],
      [ "FMOD_DSP_TYPE_CHANNELMIX", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea2adeccf941bdb31c1f9dbe2d98532b3d", null ],
      [ "FMOD_DSP_TYPE_TRANSCEIVER", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeab07a2eb06b8ef64207f3e2c62a4f5bc5", null ],
      [ "FMOD_DSP_TYPE_OBJECTPAN", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aeaf15da8e2d8c3f0ddf259d51ce23399d3", null ],
      [ "FMOD_DSP_TYPE_MULTIBAND_EQ", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea1ced49d474fcf0b04aae187443e99641", null ],
      [ "FMOD_DSP_TYPE_MAX", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea7f0fdf8eab1cd0beafafb323f7575823", null ],
      [ "FMOD_DSP_TYPE_FORCEINT", "fmod__dsp__effects_8h.html#a3bffdef0fa956bf454fa078bfdfab3aea18406eb46b87418bd15bee709c2c2939", null ]
    ] ]
];