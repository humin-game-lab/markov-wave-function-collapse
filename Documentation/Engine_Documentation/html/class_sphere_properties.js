var class_sphere_properties =
[
    [ "SphereProperties", "class_sphere_properties.html#a42e54b3c7fd47fad2f16bccef8f150ad", null ],
    [ "~SphereProperties", "class_sphere_properties.html#a3e2bc04050d35029564da5d5b33ab153", null ],
    [ "m_center", "class_sphere_properties.html#a973bbbbc5972778c2d47fb0b599af376", null ],
    [ "m_radius", "class_sphere_properties.html#ae5ea4bcca44b0ea22c8ebf721d8085e0", null ],
    [ "m_texture", "class_sphere_properties.html#ac4e6db4555718e6e434c2cf0c2e7b8dd", null ]
];