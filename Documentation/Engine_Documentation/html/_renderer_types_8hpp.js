var _renderer_types_8hpp =
[
    [ "FrameBufferT", "struct_frame_buffer_t.html", "struct_frame_buffer_t" ],
    [ "CameraBufferT", "struct_camera_buffer_t.html", "struct_camera_buffer_t" ],
    [ "ModelBufferT", "struct_model_buffer_t.html", "struct_model_buffer_t" ],
    [ "LightT", "struct_light_t.html", "struct_light_t" ],
    [ "LightBufferT", "struct_light_buffer_t.html", "struct_light_buffer_t" ],
    [ "TonemapBufferT", "struct_tonemap_buffer_t.html", "struct_tonemap_buffer_t" ],
    [ "MAX_LIGHTS", "_renderer_types_8hpp.html#a737b417f16ea225cf15e8d4c23df53dc", null ],
    [ "eRenderBufferUsageBits", "_renderer_types_8hpp.html#a1cebcc84383b92f851bc4a2252888083", null ],
    [ "eTextureUsageBits", "_renderer_types_8hpp.html#a563719f83213057c41941e7dd2b79c55", null ],
    [ "eBlendFactor", "_renderer_types_8hpp.html#a9a3b2ced85f732736ff9f1789889823c", [
      [ "FACTOR_SOURCE_ALPHA", "_renderer_types_8hpp.html#a9a3b2ced85f732736ff9f1789889823ca3feefdcd465131a85d591bb40de8b9c3", null ],
      [ "FACTOR_INV_SOURCE_ALPHA", "_renderer_types_8hpp.html#a9a3b2ced85f732736ff9f1789889823cabdde19074e3601cb83b7ffecfdb451c6", null ],
      [ "FACTOR_INV_DEST_ALPHA", "_renderer_types_8hpp.html#a9a3b2ced85f732736ff9f1789889823caf782d7c09f78cde0db34658f911cf0ad", null ],
      [ "FACTOR_ONE", "_renderer_types_8hpp.html#a9a3b2ced85f732736ff9f1789889823ca4b3e99f2c24ab714d7c3a88a0c918805", null ],
      [ "FACTOR_ZERO", "_renderer_types_8hpp.html#a9a3b2ced85f732736ff9f1789889823ca793b7c01225ef40b6c63cb8cfb595897", null ]
    ] ],
    [ "eBlendMode", "_renderer_types_8hpp.html#afab98869539061c1117bef9261a78f88", [
      [ "BLEND_MODE_OPAQUE", "_renderer_types_8hpp.html#afab98869539061c1117bef9261a78f88a54737986c91ac3c39e1c9d5c4e30d014", null ],
      [ "BLEND_MODE_ALPHA", "_renderer_types_8hpp.html#afab98869539061c1117bef9261a78f88a8b36d4fd221d10b45c2eb68d20caead0", null ],
      [ "BLEND_MODE_ADDTIVE", "_renderer_types_8hpp.html#afab98869539061c1117bef9261a78f88a48bd8ea0223daef2e0f2aecf33a357b5", null ],
      [ "BLEND_MODE_CUSTOM", "_renderer_types_8hpp.html#afab98869539061c1117bef9261a78f88a7d40e11e9924a1cf8a39778f64238116", null ]
    ] ],
    [ "eBlendOperation", "_renderer_types_8hpp.html#a58724ab669841c545d0e6b0c3cf6bcc1", [
      [ "BLEND_OP_ADD", "_renderer_types_8hpp.html#a58724ab669841c545d0e6b0c3cf6bcc1a1c83d093afe62eb6efa8216099197276", null ],
      [ "BLEND_OP_MAX", "_renderer_types_8hpp.html#a58724ab669841c545d0e6b0c3cf6bcc1a4cbf5378c0859e768ca737dd4f6e8469", null ],
      [ "BLEND_OP_LEQUAL", "_renderer_types_8hpp.html#a58724ab669841c545d0e6b0c3cf6bcc1aa611b37f29e59f29c0f5e116727fcdfa", null ],
      [ "BLEND_OP_GREATER", "_renderer_types_8hpp.html#a58724ab669841c545d0e6b0c3cf6bcc1adf5912c8c1b5ddb530127fcac5a451b1", null ],
      [ "BLEND_OP_DEFAULT", "_renderer_types_8hpp.html#a58724ab669841c545d0e6b0c3cf6bcc1a4640ddda6de414da7f750e47ae763ccf", null ]
    ] ],
    [ "eCompareOp", "_renderer_types_8hpp.html#a7341fa863314eebac19f3dda541de01f", [
      [ "COMPARE_NEVER", "_renderer_types_8hpp.html#a7341fa863314eebac19f3dda541de01fae94e861cb3a2fddda41e082224c10a55", null ],
      [ "COMPARE_ALWAYS", "_renderer_types_8hpp.html#a7341fa863314eebac19f3dda541de01fa1ea862f965981d9daba5e6db10f98155", null ],
      [ "COMPARE_EQUAL", "_renderer_types_8hpp.html#a7341fa863314eebac19f3dda541de01fa4bccf19eed6a7720020ab7463ab9a157", null ],
      [ "COMPARE_NOTEQUAL", "_renderer_types_8hpp.html#a7341fa863314eebac19f3dda541de01fa1b14169a59a4aed6394de6ba1057da17", null ],
      [ "COMPARE_LESS", "_renderer_types_8hpp.html#a7341fa863314eebac19f3dda541de01fa6f38af87e6cf1b297ef14ad4a9dad5f5", null ],
      [ "COMPARE_LEQUAL", "_renderer_types_8hpp.html#a7341fa863314eebac19f3dda541de01fa49157a1ba0b44182f6bdc59aa6d7de97", null ],
      [ "COMPARE_GREATER", "_renderer_types_8hpp.html#a7341fa863314eebac19f3dda541de01fa79f6fe5c61461830b9a34046a32d9b5b", null ],
      [ "COMPARE_GEQUAL", "_renderer_types_8hpp.html#a7341fa863314eebac19f3dda541de01fa3b410786ac9acd6a0cf808aaa768d814", null ]
    ] ],
    [ "eCoreUniformSlot", "_renderer_types_8hpp.html#abd4e15cd4b87b17c1a82d0a76870674b", [
      [ "UNIFORM_SLOT_FRAME", "_renderer_types_8hpp.html#abd4e15cd4b87b17c1a82d0a76870674ba50164e3a173d854b351e689aa07d6916", null ],
      [ "UNIFORM_SLOT_CAMERA", "_renderer_types_8hpp.html#abd4e15cd4b87b17c1a82d0a76870674ba70f7897a0d79eff9e6f14084d76bc67e", null ],
      [ "UNIFORM_SLOT_MODEL_MATRIX", "_renderer_types_8hpp.html#abd4e15cd4b87b17c1a82d0a76870674baf70369a3295578bb966a49da9c00a044", null ],
      [ "UNIFORM_SLOT_LIGHT", "_renderer_types_8hpp.html#abd4e15cd4b87b17c1a82d0a76870674baa7cdf046da87c2ea32d77775cd47cdc4", null ]
    ] ],
    [ "eDataFormat", "_renderer_types_8hpp.html#a9e9161283117406c7d3219a9649c6500", [
      [ "DF_NULL", "_renderer_types_8hpp.html#a9e9161283117406c7d3219a9649c6500a37cd5118b881ef01d391dbe2824e3b39", null ],
      [ "DF_FLOAT", "_renderer_types_8hpp.html#a9e9161283117406c7d3219a9649c6500a3b030cbd8dd7268e0cef5f6822cb9193", null ],
      [ "DF_VEC2", "_renderer_types_8hpp.html#a9e9161283117406c7d3219a9649c6500a982de3e2f4bee1c262fff5233a63a859", null ],
      [ "DF_VEC3", "_renderer_types_8hpp.html#a9e9161283117406c7d3219a9649c6500ae1af2ceb73934a51ef8177c977244f0c", null ],
      [ "DF_RGBA32", "_renderer_types_8hpp.html#a9e9161283117406c7d3219a9649c6500a62f6a4f99a9bf14fdfa7aa0a38bc2cf4", null ]
    ] ],
    [ "eGPUMemoryUsage", "_renderer_types_8hpp.html#a42363368b9d45e5c74658b12c60733a4", [
      [ "GPU_MEMORY_USAGE_GPU", "_renderer_types_8hpp.html#a42363368b9d45e5c74658b12c60733a4a4cfdfaa20eb9836e913a52283b15937e", null ],
      [ "GPU_MEMORY_USAGE_STATIC", "_renderer_types_8hpp.html#a42363368b9d45e5c74658b12c60733a4a586e0df4c224f872acb58266234b75c0", null ],
      [ "GPU_MEMORY_USAGE_DYNAMIC", "_renderer_types_8hpp.html#a42363368b9d45e5c74658b12c60733a4ac37c98c4fa092663a8c3c82fd29a9624", null ],
      [ "GPU_MEMORY_USAGE_STAGING", "_renderer_types_8hpp.html#a42363368b9d45e5c74658b12c60733a4a0808e7228a69ba520b6e4ace71370db1", null ]
    ] ],
    [ "eRenderBufferUsageBit", "_renderer_types_8hpp.html#ad66c770ffb139d7d4fdb4bb9d57c695d", [
      [ "RENDER_BUFFER_USAGE_VERTEX_STREAM_BIT", "_renderer_types_8hpp.html#ad66c770ffb139d7d4fdb4bb9d57c695da09bca2d9920a7020eff27fcbcd06502f", null ],
      [ "RENDER_BUFFER_USAGE_INDEX_STREAM_BIT", "_renderer_types_8hpp.html#ad66c770ffb139d7d4fdb4bb9d57c695daad7059e12b5652c8d65d230aa1b4d63f", null ],
      [ "RENDER_BUFFER_USAGE_UNIFORM_BIT", "_renderer_types_8hpp.html#ad66c770ffb139d7d4fdb4bb9d57c695da297bb3662a43798978d9b92d8ad26b7d", null ]
    ] ],
    [ "eSampleMode", "_renderer_types_8hpp.html#a8e3d15bd8a44447bee6648e5ac7ab19c", [
      [ "SAMPLE_MODE_POINT", "_renderer_types_8hpp.html#a8e3d15bd8a44447bee6648e5ac7ab19ca8f649861aa01ca7cbe0b031437e06014", null ],
      [ "SAMPLE_MODE_LINEAR", "_renderer_types_8hpp.html#a8e3d15bd8a44447bee6648e5ac7ab19ca3c51bfd3e996ba7d1c47f3ea6b69403d", null ],
      [ "SAMPLE_MODE_COUNT", "_renderer_types_8hpp.html#a8e3d15bd8a44447bee6648e5ac7ab19ca3ef3edd7f584933cc91c554f4ab5682a", null ],
      [ "SAMPLE_MODE_DEFAULT", "_renderer_types_8hpp.html#a8e3d15bd8a44447bee6648e5ac7ab19ca927d98fedf6d6506b4f552bf4685f97e", null ]
    ] ],
    [ "eShaderStage", "_renderer_types_8hpp.html#a6347a06caab7346c4c5c345c3cb33afb", [
      [ "SHADER_STAGE_VERTEX", "_renderer_types_8hpp.html#a6347a06caab7346c4c5c345c3cb33afbab2afbff48fd225f6c90f227c244f2943", null ],
      [ "SHADER_STAGE_FRAGMENT", "_renderer_types_8hpp.html#a6347a06caab7346c4c5c345c3cb33afbaae882816d7a91c959abe661a86e939cc", null ]
    ] ],
    [ "eTextureUsageBit", "_renderer_types_8hpp.html#add4a81e322f7e714f5665626ca5774a8", [
      [ "TEXTURE_USAGE_TEXTURE_BIT", "_renderer_types_8hpp.html#add4a81e322f7e714f5665626ca5774a8a0c21638e2cffeef42e315f18b6810af2", null ],
      [ "TEXTURE_USAGE_COLOR_TARGET_BIT", "_renderer_types_8hpp.html#add4a81e322f7e714f5665626ca5774a8a886d5bf8906b88163615318f199462f1", null ],
      [ "TEXTURE_USAGE_DEPTH_STENCIL_TARGET_BIT", "_renderer_types_8hpp.html#add4a81e322f7e714f5665626ca5774a8a698940efbcb7c21576ba387c99b6a954", null ]
    ] ]
];