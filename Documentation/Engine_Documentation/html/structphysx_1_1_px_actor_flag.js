var structphysx_1_1_px_actor_flag =
[
    [ "Enum", "structphysx_1_1_px_actor_flag.html#a123559ef67fcc063d513f580fcfd02b8", [
      [ "eVISUALIZATION", "structphysx_1_1_px_actor_flag.html#a123559ef67fcc063d513f580fcfd02b8a4f8f1ca0fe845730c1f0d7e4cf1567e4", null ],
      [ "eDISABLE_GRAVITY", "structphysx_1_1_px_actor_flag.html#a123559ef67fcc063d513f580fcfd02b8a357f87141d1a7202f3218148c5ee9c6a", null ],
      [ "eSEND_SLEEP_NOTIFIES", "structphysx_1_1_px_actor_flag.html#a123559ef67fcc063d513f580fcfd02b8a269cc7def6fcd874db6ed847e1d41f5d", null ],
      [ "eDISABLE_SIMULATION", "structphysx_1_1_px_actor_flag.html#a123559ef67fcc063d513f580fcfd02b8a0c6c9a553d3fc8ae1ad5a09cfdc7f2b5", null ]
    ] ]
];