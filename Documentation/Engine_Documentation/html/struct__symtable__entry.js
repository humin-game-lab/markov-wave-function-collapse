var struct__symtable__entry =
[
    [ "ste_child_free", "struct__symtable__entry.html#a702e638b28c6cb34b4995d5ba9239cb3", null ],
    [ "ste_children", "struct__symtable__entry.html#a95a1b4f65170d448fe57a66fd73924f5", null ],
    [ "ste_col_offset", "struct__symtable__entry.html#a1d2a1f3fd82944d610d7cb8b6d87b5bc", null ],
    [ "ste_coroutine", "struct__symtable__entry.html#acfd84600a91259ce3baf69984601c0cf", null ],
    [ "ste_directives", "struct__symtable__entry.html#a84da99847294a4a8608ac8078b4f1286", null ],
    [ "ste_free", "struct__symtable__entry.html#ad9adb48b9bbb65e0978e7bbbc0ab28f4", null ],
    [ "ste_generator", "struct__symtable__entry.html#aa52a41915a93b8df3d29283bda786577", null ],
    [ "ste_id", "struct__symtable__entry.html#a5592e20eb030643d8f0ac15eb2a420a8", null ],
    [ "ste_lineno", "struct__symtable__entry.html#acb6b33690a6ef14df11c57993f16df35", null ],
    [ "ste_name", "struct__symtable__entry.html#a10a3fb305050313babc7322ffb401456", null ],
    [ "ste_needs_class_closure", "struct__symtable__entry.html#aa881e4208d25c45d5ea7724289300998", null ],
    [ "ste_nested", "struct__symtable__entry.html#a01768336ee197e6e432af802e3833a76", null ],
    [ "ste_opt_col_offset", "struct__symtable__entry.html#ab37cab7a9c4bb2ec9e0d7716b22e84de", null ],
    [ "ste_opt_lineno", "struct__symtable__entry.html#a577586ad1073e9827effb2a1307ae6ad", null ],
    [ "ste_returns_value", "struct__symtable__entry.html#ae19032c58d983089cff8249a656a20d2", null ],
    [ "ste_symbols", "struct__symtable__entry.html#a9861a26aa97ecd761978f0945f597198", null ],
    [ "ste_table", "struct__symtable__entry.html#a42ce617896ae1d9718d309d54dc0dd09", null ],
    [ "ste_type", "struct__symtable__entry.html#a9a070f2be73f328f86b565c0d3a1439f", null ],
    [ "ste_varargs", "struct__symtable__entry.html#a8c10d18a77ef271880c58c5024735675", null ],
    [ "ste_varkeywords", "struct__symtable__entry.html#adf09b37ba6e825fffd14244163b9e556", null ],
    [ "ste_varnames", "struct__symtable__entry.html#a7c4f7b2d6a187257a2966cf80c447d62", null ]
];