var _anim_types_8hpp =
[
    [ "eAnimationType", "_anim_types_8hpp.html#a4b882f4607979a3c0e8e8be2a9cbd242", [
      [ "ANIMATION_IDLE", "_anim_types_8hpp.html#a4b882f4607979a3c0e8e8be2a9cbd242a365057b4417eea6fa7f37cb60877369d", null ],
      [ "ANIMATION_WALK", "_anim_types_8hpp.html#a4b882f4607979a3c0e8e8be2a9cbd242a83054b457d88f8f493483de9ce763359", null ],
      [ "ANIMATION_ATTACK", "_anim_types_8hpp.html#a4b882f4607979a3c0e8e8be2a9cbd242a1dd751c641fd646c86604000653d31d7", null ],
      [ "ANIMATION_DIE", "_anim_types_8hpp.html#a4b882f4607979a3c0e8e8be2a9cbd242a50afdc9be62a82c2180167c0404f8563", null ],
      [ "ANIMATION_COUNT", "_anim_types_8hpp.html#a4b882f4607979a3c0e8e8be2a9cbd242a3329b7573112120d71c1a27f9f30e485", null ]
    ] ],
    [ "SpriteAnimPlaybackType", "_anim_types_8hpp.html#a10b86173ff27edf621fd34bd002f1e36", [
      [ "SPRITE_ANIM_PLAYBACK_ONCE", "_anim_types_8hpp.html#a10b86173ff27edf621fd34bd002f1e36a2770b0862f6e9a8356b05880ac951eec", null ],
      [ "SPRITE_ANIM_PLAYBACK_LOOP", "_anim_types_8hpp.html#a10b86173ff27edf621fd34bd002f1e36ada004385976776431967820ea0bd1418", null ],
      [ "SPRITE_ANIM_PLAYBACK_PINGPONG", "_anim_types_8hpp.html#a10b86173ff27edf621fd34bd002f1e36ab13d207ed03ea501249b36234785c6b7", null ],
      [ "NUM_SPRITE_ANIM_PLAYBACK_TYPES", "_anim_types_8hpp.html#a10b86173ff27edf621fd34bd002f1e36a67ac5525df0e72215efc70f5ffb92cab", null ]
    ] ]
];