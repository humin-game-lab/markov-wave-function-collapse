var class_buffer_layout =
[
    [ "BufferLayout", "class_buffer_layout.html#aabb1044969d189698f55f76ebcd2c77f", null ],
    [ "GetAttributeCount", "class_buffer_layout.html#afb38770284a0a9190ec9686d83f2f224", null ],
    [ "GetStride", "class_buffer_layout.html#a30c6429ace5c25adef814cea5355ef05", null ],
    [ "m_attributes", "class_buffer_layout.html#a5c06999c5e0ea35b1e872afd6ff04c19", null ],
    [ "m_copyFromMaster", "class_buffer_layout.html#a27f497faa7ebe35be50402e78b2e7d29", null ],
    [ "m_stride", "class_buffer_layout.html#a313526e285bc665e173f07e461df4513", null ]
];