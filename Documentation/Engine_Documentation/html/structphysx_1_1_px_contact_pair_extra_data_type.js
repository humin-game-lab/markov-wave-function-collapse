var structphysx_1_1_px_contact_pair_extra_data_type =
[
    [ "Enum", "structphysx_1_1_px_contact_pair_extra_data_type.html#a968eb72b31e8bada3d1f9e9b44d4ea78", [
      [ "ePRE_SOLVER_VELOCITY", "structphysx_1_1_px_contact_pair_extra_data_type.html#a968eb72b31e8bada3d1f9e9b44d4ea78a717066519563eee21c4a0ffe0a538460", null ],
      [ "ePOST_SOLVER_VELOCITY", "structphysx_1_1_px_contact_pair_extra_data_type.html#a968eb72b31e8bada3d1f9e9b44d4ea78adbf9269eb38799431b5d302daf820659", null ],
      [ "eCONTACT_EVENT_POSE", "structphysx_1_1_px_contact_pair_extra_data_type.html#a968eb72b31e8bada3d1f9e9b44d4ea78a1f34361d6b63e155a1af47c78fd13511", null ],
      [ "eCONTACT_PAIR_INDEX", "structphysx_1_1_px_contact_pair_extra_data_type.html#a968eb72b31e8bada3d1f9e9b44d4ea78aaddcf5038a5906c93c29bd23596cda58", null ]
    ] ]
];