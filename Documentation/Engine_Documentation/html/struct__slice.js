var struct__slice =
[
    [ "dims", "struct__slice.html#a93030a4ad534e07757aee5a01339c4e1", null ],
    [ "ExtSlice", "struct__slice.html#a44ebfdf18406d0cb0e38bb666a9480c5", null ],
    [ "Index", "struct__slice.html#a02eb8abcfed6c123d5a0abeb5b2879b5", null ],
    [ "kind", "struct__slice.html#a9db6ac61022be11837644803e62dbae3", null ],
    [ "lower", "struct__slice.html#a6f093b55e957623c74a6331d8f937500", null ],
    [ "Slice", "struct__slice.html#ac6ffeb569b2e56f75a0bbedd62c430cb", null ],
    [ "step", "struct__slice.html#ace8e2eccc20477db2a1ac90d267154b2", null ],
    [ "upper", "struct__slice.html#a9535a1f77a13185667a1be7eacd5f557", null ],
    [ "v", "struct__slice.html#af3631803077d39b2c1771b1ad6645e01", null ],
    [ "value", "struct__slice.html#af026e3a8887c949516ce9a869cf73fb4", null ]
];