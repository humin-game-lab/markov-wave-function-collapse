var struct_p_vector_base =
[
    [ "differenceType", "struct_p_vector_base.html#a5f8b035d93f37207a5542a66473e5752", null ],
    [ "maxSize", "struct_p_vector_base.html#a4e94f9da5a4e57efaaa9a89efd679fb1ab8561f5714ca325a6871943f2ea38bae", null ],
    [ "PVectorBase", "struct_p_vector_base.html#a567b494749521f8b31767598696bdda2", null ],
    [ "PVectorBase", "struct_p_vector_base.html#a11c33a89761a38a422df897c11181a6f", null ],
    [ "PVectorBase", "struct_p_vector_base.html#a8b6ea8d2a69cef27d58297f1a12e246e", null ],
    [ "~PVectorBase", "struct_p_vector_base.html#aa4109db5857a3774db9bded851fe7042", null ],
    [ "Allocate", "struct_p_vector_base.html#ab9fd8c19ab55e1e0072990c1eae454f3", null ],
    [ "Free", "struct_p_vector_base.html#ace748df295e27ce96d68d7bd28f2794a", null ],
    [ "GetAllocator", "struct_p_vector_base.html#aedb7853f5d29bdca06bd3629d10c2f81", null ],
    [ "GetInternalAllocator", "struct_p_vector_base.html#aee53bf6b11bc2425c605b98bb64f4658", null ],
    [ "GetInternalCapacityPtr", "struct_p_vector_base.html#a7dcd614ea5912a678a9d971eed946f49", null ],
    [ "GetNewCapacity", "struct_p_vector_base.html#ae48bcd3da3df3958b889e31893544346", null ],
    [ "GetSetableAllocator", "struct_p_vector_base.html#a3ffde718b46aac95c601d9b550da6117", null ],
    [ "GetSetableInternalAllocator", "struct_p_vector_base.html#a1005a7d73283d900cb51cf6cf3fa5230", null ],
    [ "GetSetableInternalCapacityPtr", "struct_p_vector_base.html#a5f3dd90f56392e87963e0607c1de88f2", null ],
    [ "SetAllocator", "struct_p_vector_base.html#a9fbe439941e5ae3b3f90395ae7223c4a", null ],
    [ "m_begin", "struct_p_vector_base.html#afeb83f9fbd26997408a9982aa838ab1c", null ],
    [ "m_capacityAllocator", "struct_p_vector_base.html#a9a4e29a4b81bfa5e96e43c9f8522feb7", null ],
    [ "m_end", "struct_p_vector_base.html#abdea5aedbbeb7ff11cb6a31f290ab8ef", null ]
];