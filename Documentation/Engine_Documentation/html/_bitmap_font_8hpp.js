var _bitmap_font_8hpp =
[
    [ "GlyphData", "struct_glyph_data.html", "struct_glyph_data" ],
    [ "VariableFontLoader", "struct_variable_font_loader.html", "struct_variable_font_loader" ],
    [ "BitmapFont", "class_bitmap_font.html", "class_bitmap_font" ],
    [ "eFontType", "_bitmap_font_8hpp.html#aab4869d9a6f2ae659546b022e89b3913", [
      [ "FIXED_WIDTH", "_bitmap_font_8hpp.html#aab4869d9a6f2ae659546b022e89b3913a7857090da666bfba1583a58e8c3f29c5", null ],
      [ "PROPORTIONAL", "_bitmap_font_8hpp.html#aab4869d9a6f2ae659546b022e89b3913ad35adb33a4bd3162eff7f960ea0ad37a", null ],
      [ "VARIABLE_WIDTH", "_bitmap_font_8hpp.html#aab4869d9a6f2ae659546b022e89b3913ab609c99897a51516c313416645b8ff00", null ],
      [ "SIGNED_DISTANCE_FIELD", "_bitmap_font_8hpp.html#aab4869d9a6f2ae659546b022e89b3913a6c220f00166b8473253615727d72b5ce", null ]
    ] ],
    [ "eTextBoxMode", "_bitmap_font_8hpp.html#a43ab5182f45ead09554e6d3852c21e2b", [
      [ "TEXT_BOX_MODE_OVERLAP", "_bitmap_font_8hpp.html#a43ab5182f45ead09554e6d3852c21e2ba8e6a790273a8fbb1bff52fcae9b1ae10", null ],
      [ "TEXT_BOX_MODE_SHRINK", "_bitmap_font_8hpp.html#a43ab5182f45ead09554e6d3852c21e2ba08e78e7ca669422ca314beca7fbb94d6", null ]
    ] ]
];