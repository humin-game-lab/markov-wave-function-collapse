var struct_py_function_object =
[
    [ "func_annotations", "struct_py_function_object.html#a715b0fe3ce91d9f9705d08b1e9910023", null ],
    [ "func_closure", "struct_py_function_object.html#aafd3e2863284c80370b4fefa2116e120", null ],
    [ "func_code", "struct_py_function_object.html#a6c5f4d38976ab36a863e6e5d03581cd1", null ],
    [ "func_defaults", "struct_py_function_object.html#ae6c99ddf86631063aaeb760e324036fd", null ],
    [ "func_dict", "struct_py_function_object.html#a291b33faa7d01907b1ddbc07e4f37409", null ],
    [ "func_doc", "struct_py_function_object.html#aae1ad9ce5d3f2951306948a08f4c12dd", null ],
    [ "func_globals", "struct_py_function_object.html#a236accb7c482a4e856b70b8fb082debd", null ],
    [ "func_kwdefaults", "struct_py_function_object.html#ab0c1ae70b61a168d69e90369f3b31849", null ],
    [ "func_module", "struct_py_function_object.html#a77f851f25b514b4a7d902eeba5f3b681", null ],
    [ "func_name", "struct_py_function_object.html#a5667a7154d870b8644a469a735283fc8", null ],
    [ "func_qualname", "struct_py_function_object.html#af3a9b62f81d731aaeac0af99635b1b72", null ],
    [ "func_weakreflist", "struct_py_function_object.html#abef9b746762125a815e52b8c6754f36e", null ]
];