var structphysx_1_1_px_vehicle_types =
[
    [ "Enum", "structphysx_1_1_px_vehicle_types.html#ae6d4221b90e8de941499e07399bfbe19", [
      [ "eDRIVE4W", "structphysx_1_1_px_vehicle_types.html#ae6d4221b90e8de941499e07399bfbe19ab60d6bab81f202a73f6776b0c8671d36", null ],
      [ "eDRIVENW", "structphysx_1_1_px_vehicle_types.html#ae6d4221b90e8de941499e07399bfbe19a8a476c99fff9ad25d6594999035cc44c", null ],
      [ "eDRIVETANK", "structphysx_1_1_px_vehicle_types.html#ae6d4221b90e8de941499e07399bfbe19a8e02b207c2f68fea3694a85c4a5f833c", null ],
      [ "eNODRIVE", "structphysx_1_1_px_vehicle_types.html#ae6d4221b90e8de941499e07399bfbe19a0b5e1b6ac4cadbd834a6c3e13f012ba5", null ],
      [ "eUSER1", "structphysx_1_1_px_vehicle_types.html#ae6d4221b90e8de941499e07399bfbe19aebdf63f49cbcada79e5ed930919b557f", null ],
      [ "eUSER2", "structphysx_1_1_px_vehicle_types.html#ae6d4221b90e8de941499e07399bfbe19a1c18b748d0aaa56dafc330c79bd23c55", null ],
      [ "eUSER3", "structphysx_1_1_px_vehicle_types.html#ae6d4221b90e8de941499e07399bfbe19ae80d896b626b6b383e156dc72df1bb94", null ],
      [ "eMAX_NB_VEHICLE_TYPES", "structphysx_1_1_px_vehicle_types.html#ae6d4221b90e8de941499e07399bfbe19aedbebeacae432179f1fa06e2d1501b59", null ]
    ] ]
];