var structphysx_1_1_px_contact_pair_extra_data_iterator =
[
    [ "PxContactPairExtraDataIterator", "structphysx_1_1_px_contact_pair_extra_data_iterator.html#a1f6b9a0a12e9fc4a8d543f0a84f8049b", null ],
    [ "nextItemSet", "structphysx_1_1_px_contact_pair_extra_data_iterator.html#a5eca8c7cba646ee9957262506677dc1c", null ],
    [ "contactPairIndex", "structphysx_1_1_px_contact_pair_extra_data_iterator.html#a0a1b552bc559b7317b669c53bf1b3c71", null ],
    [ "currPtr", "structphysx_1_1_px_contact_pair_extra_data_iterator.html#a97cd17afdd01a34f23d42ad89ce18bc6", null ],
    [ "endPtr", "structphysx_1_1_px_contact_pair_extra_data_iterator.html#afa2eabbc3f520f33d68d430739512cc8", null ],
    [ "eventPose", "structphysx_1_1_px_contact_pair_extra_data_iterator.html#aea5324b118d0b411b98644243b8fd03a", null ],
    [ "postSolverVelocity", "structphysx_1_1_px_contact_pair_extra_data_iterator.html#a30ebf4d76abb5610f9256cdce73befff", null ],
    [ "preSolverVelocity", "structphysx_1_1_px_contact_pair_extra_data_iterator.html#aa1b71ca14a8b0f7f179a16c2c2d8f1b2", null ]
];