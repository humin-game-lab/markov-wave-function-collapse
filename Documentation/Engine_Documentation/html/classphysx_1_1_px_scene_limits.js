var classphysx_1_1_px_scene_limits =
[
    [ "PxSceneLimits", "classphysx_1_1_px_scene_limits.html#a1cceb20d04b188e2fd5f5041ca56f8a1", null ],
    [ "isValid", "classphysx_1_1_px_scene_limits.html#a441bf1fe4d04541584e39f3395570344", null ],
    [ "setToDefault", "classphysx_1_1_px_scene_limits.html#aabe10600b43894909117c7d87f82a764", null ],
    [ "maxNbActors", "classphysx_1_1_px_scene_limits.html#a613c3b2d1b3dbcaa2cdfb489a4f76f5f", null ],
    [ "maxNbAggregates", "classphysx_1_1_px_scene_limits.html#af0d7e3761251d2350297b7c2233952d4", null ],
    [ "maxNbBodies", "classphysx_1_1_px_scene_limits.html#a99462e546ae514471fa07adda6f884eb", null ],
    [ "maxNbBroadPhaseOverlaps", "classphysx_1_1_px_scene_limits.html#a7d75e1279274ced93da938b1a3411675", null ],
    [ "maxNbConstraints", "classphysx_1_1_px_scene_limits.html#a003e9ccf0f4fc0d03752f7e47b246a61", null ],
    [ "maxNbDynamicShapes", "classphysx_1_1_px_scene_limits.html#a3a1d5544dd40464dc2389d47478aaf5b", null ],
    [ "maxNbRegions", "classphysx_1_1_px_scene_limits.html#a47cc621b98e8921ed5deebe416107252", null ],
    [ "maxNbStaticShapes", "classphysx_1_1_px_scene_limits.html#a31f8a018216314d1b021ef5fcdc086e8", null ]
];