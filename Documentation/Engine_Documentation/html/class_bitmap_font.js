var class_bitmap_font =
[
    [ "BitmapFont", "class_bitmap_font.html#a3860ccb29361d0dbf19e5892cd01b9ae", null ],
    [ "BitmapFont", "class_bitmap_font.html#a8a71710c2615c19b63189092e2d8f44c", null ],
    [ "AddVertsForText2D", "class_bitmap_font.html#a2e838d38dd42e4780eb539e057cbc591", null ],
    [ "AddVertsForText3D", "class_bitmap_font.html#a7bb209a39a7bc94239ca29cea4a3c95b", null ],
    [ "AddVertsForTextInBox2D", "class_bitmap_font.html#af9c548f587d279312612756508a54371", null ],
    [ "AddVertsForTextInBox3D", "class_bitmap_font.html#acab153e00f26d4aac6efc9f80a37457e", null ],
    [ "GetGlyphAspect", "class_bitmap_font.html#a87bff51f5d49943bca62a9314284b972", null ],
    [ "GetGlyphData", "class_bitmap_font.html#a4332b83cd73dfc35b86d9f12bdaa6429", null ],
    [ "GetLimitsForSpriteIndex", "class_bitmap_font.html#a19f01f82f9c70a05e9061821253f2db0", null ],
    [ "GetNewCellAspect", "class_bitmap_font.html#ae547d9de25080f88a9f82725bc88fa57", null ],
    [ "GetNewCellAspect3D", "class_bitmap_font.html#a71460bb60fea74da73259a883de1e641", null ],
    [ "GetTextBoundingBox", "class_bitmap_font.html#a4c8418c9ddbc073a76db97a749fac444", null ],
    [ "GetTexture", "class_bitmap_font.html#a2dc9c2a6d821c5a7a6e5ca35a95cbdad", null ],
    [ "InitializeProportionalFonts", "class_bitmap_font.html#ae83168b1c1c8ed4056da66bf6d43ff72", null ],
    [ "InitializeVariableWidthFonts", "class_bitmap_font.html#a402a1ba97d5bdf4e01f86e59111f3090", null ],
    [ "MakeSpriteSheetfromImage", "class_bitmap_font.html#a29d4b8faecf3af317f99de6ca57be7ca", null ],
    [ "RenderContext", "class_bitmap_font.html#adbc71e71bb875e994e20b698201b22a5", null ]
];