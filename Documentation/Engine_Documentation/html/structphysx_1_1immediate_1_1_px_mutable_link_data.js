var structphysx_1_1immediate_1_1_px_mutable_link_data =
[
    [ "angularDamping", "structphysx_1_1immediate_1_1_px_mutable_link_data.html#ae1129653219caf078bf2c6eb9d213370", null ],
    [ "disableGravity", "structphysx_1_1immediate_1_1_px_mutable_link_data.html#a32ac6255eba7a5ae372859e6d9bbf937", null ],
    [ "inverseInertia", "structphysx_1_1immediate_1_1_px_mutable_link_data.html#ae5b8bc7000c448d3fedce4e6b54e62e9", null ],
    [ "inverseMass", "structphysx_1_1immediate_1_1_px_mutable_link_data.html#a396601b8ee34ed0ce1b611334d68ed70", null ],
    [ "linearDamping", "structphysx_1_1immediate_1_1_px_mutable_link_data.html#ab45fd6763aee66aa7ce9c77a8aaed034", null ],
    [ "maxAngularVelocitySq", "structphysx_1_1immediate_1_1_px_mutable_link_data.html#ae9f9d165a66d3cbf12328d71b0540cfe", null ],
    [ "maxLinearVelocitySq", "structphysx_1_1immediate_1_1_px_mutable_link_data.html#a5c8517ab03442ee46df885ad6122c53e", null ]
];