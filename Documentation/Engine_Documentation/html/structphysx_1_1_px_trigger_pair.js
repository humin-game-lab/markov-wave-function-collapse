var structphysx_1_1_px_trigger_pair =
[
    [ "PxTriggerPair", "structphysx_1_1_px_trigger_pair.html#a2d3455b7b7a52a14b213c12c080228f2", null ],
    [ "flags", "structphysx_1_1_px_trigger_pair.html#a4651c8dc2c8b15f655bd074bb66222b3", null ],
    [ "otherActor", "structphysx_1_1_px_trigger_pair.html#ab97fafda7fa3b6182a2d5edb7bd49d6f", null ],
    [ "otherShape", "structphysx_1_1_px_trigger_pair.html#a99aadbd961e1038e99c7c4368c4fd625", null ],
    [ "status", "structphysx_1_1_px_trigger_pair.html#a2718e9f4aa378add538c38859db85554", null ],
    [ "triggerActor", "structphysx_1_1_px_trigger_pair.html#a38e052ada3268b02740e098488c5e321", null ],
    [ "triggerShape", "structphysx_1_1_px_trigger_pair.html#a225e4ef405ecbe5653e3d606736d58e9", null ]
];