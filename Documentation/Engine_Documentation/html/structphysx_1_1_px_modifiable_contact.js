var structphysx_1_1_px_modifiable_contact =
[
    [ "PX_ALIGN", "structphysx_1_1_px_modifiable_contact.html#a8abae8c86b33736a30397777fcfad6d1", null ],
    [ "dynamicFriction", "structphysx_1_1_px_modifiable_contact.html#a1cfe12215b2af7875fec6e0712d184cb", null ],
    [ "materialFlags", "structphysx_1_1_px_modifiable_contact.html#a9b0b419ca598660c01e4842186da11ea", null ],
    [ "materialIndex0", "structphysx_1_1_px_modifiable_contact.html#ab2b939fddd48acc932203d1f5ecb2a02", null ],
    [ "materialIndex1", "structphysx_1_1_px_modifiable_contact.html#a1fc6fae495731690ade224fde4495e98", null ],
    [ "restitution", "structphysx_1_1_px_modifiable_contact.html#aae0f14456769bcebe5366abffbeca6e1", null ],
    [ "staticFriction", "structphysx_1_1_px_modifiable_contact.html#a4771aea201f4f84312747ad622b2ab4e", null ]
];