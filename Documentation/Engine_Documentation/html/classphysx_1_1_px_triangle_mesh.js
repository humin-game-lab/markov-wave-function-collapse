var classphysx_1_1_px_triangle_mesh =
[
    [ "PxTriangleMesh", "classphysx_1_1_px_triangle_mesh.html#a7110db9d789a156f42bafdce7bcd71f1", null ],
    [ "PxTriangleMesh", "classphysx_1_1_px_triangle_mesh.html#a5993e1fad217ada4aa03694ba5cea6ab", null ],
    [ "~PxTriangleMesh", "classphysx_1_1_px_triangle_mesh.html#ace6e8d46fdf97425a77a65b5ba26512d", null ],
    [ "acquireReference", "classphysx_1_1_px_triangle_mesh.html#a16d644dc154b17a2546f1cbda2ae19d0", null ],
    [ "getLocalBounds", "classphysx_1_1_px_triangle_mesh.html#aacb2df4aabd0d8be3413b622104ade4a", null ],
    [ "getNbTriangles", "classphysx_1_1_px_triangle_mesh.html#ad3538f5500079c6793f43d82a3911168", null ],
    [ "getNbVertices", "classphysx_1_1_px_triangle_mesh.html#ae78be62eea7311d6b5398e985a776ea8", null ],
    [ "getReferenceCount", "classphysx_1_1_px_triangle_mesh.html#a4c305a2ef5d96774b9c6c0bd5a582a5d", null ],
    [ "getTriangleMaterialIndex", "classphysx_1_1_px_triangle_mesh.html#a99c53cfddcb928f8698c7864cf5217de", null ],
    [ "getTriangleMeshFlags", "classphysx_1_1_px_triangle_mesh.html#ab98d1078a949fe5d4c7dc91693bd6a92", null ],
    [ "getTriangles", "classphysx_1_1_px_triangle_mesh.html#a7fe8e10770aca723d00269bcf3a995c7", null ],
    [ "getTrianglesRemap", "classphysx_1_1_px_triangle_mesh.html#aa02843f25d2a6fd9cc603e3f64ea9c0c", null ],
    [ "getVertices", "classphysx_1_1_px_triangle_mesh.html#a12def58804fa315cc270003da4e2303f", null ],
    [ "getVerticesForModification", "classphysx_1_1_px_triangle_mesh.html#aef3dde1f186955d810221cae8db02f64", null ],
    [ "isKindOf", "classphysx_1_1_px_triangle_mesh.html#ada0a83dbd2fa793274edd4338f3d373a", null ],
    [ "refitBVH", "classphysx_1_1_px_triangle_mesh.html#a6bc52386004f6f5c2f24c6ffd27278d9", null ],
    [ "release", "classphysx_1_1_px_triangle_mesh.html#a7ce4ebcbb0affad88c1bbb807ae81cba", null ]
];