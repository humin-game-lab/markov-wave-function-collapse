var _phys_x_types_8hpp =
[
    [ "PhysXVehicleBaseAttributes", "struct_phys_x_vehicle_base_attributes.html", "struct_phys_x_vehicle_base_attributes" ],
    [ "PhysXConvexMeshCookingTypes_T", "_phys_x_types_8hpp.html#ad6fe708feab7f1473f990b1edb55c44d", [
      [ "QUICKHULL", "_phys_x_types_8hpp.html#ad6fe708feab7f1473f990b1edb55c44da5e0e28a6f01f0be11cc4794d7df40196", null ]
    ] ],
    [ "PhysXGeometryTypes_T", "_phys_x_types_8hpp.html#a99cbb583264adac6f3361a9f175ec714", [
      [ "BOX", "_phys_x_types_8hpp.html#a99cbb583264adac6f3361a9f175ec714a311c29de5b0fd16d11c0cf1f98a73370", null ],
      [ "CAPSULE", "_phys_x_types_8hpp.html#a99cbb583264adac6f3361a9f175ec714a4454239be41a546a792a44bf06bd3117", null ],
      [ "SPHERE", "_phys_x_types_8hpp.html#a99cbb583264adac6f3361a9f175ec714aae4f0962d104ea473feec5598689316d", null ],
      [ "CONVEX_HULL", "_phys_x_types_8hpp.html#a99cbb583264adac6f3361a9f175ec714aaa3ca75558bec292d41e23d4b6094130", null ]
    ] ],
    [ "PhysXJointType_T", "_phys_x_types_8hpp.html#afe9609ff15d1276d23956a7249edcef0", [
      [ "SIMPLE_SPHERICAL", "_phys_x_types_8hpp.html#afe9609ff15d1276d23956a7249edcef0ae6d8e00a5d94e7dcec79f05710fc983b", null ],
      [ "LIMITED_SPHERICAL", "_phys_x_types_8hpp.html#afe9609ff15d1276d23956a7249edcef0a708f5e5487ab099fc2c7b448c7f37203", null ],
      [ "SIMPLE_FIXED", "_phys_x_types_8hpp.html#afe9609ff15d1276d23956a7249edcef0a41089fe1d0366e6555285feed641d6b5", null ],
      [ "BREAKABLE_FIXED", "_phys_x_types_8hpp.html#afe9609ff15d1276d23956a7249edcef0a11e690ccab9c66bd611c8f0658cdb55a", null ],
      [ "D6_DAMPED", "_phys_x_types_8hpp.html#afe9609ff15d1276d23956a7249edcef0a2087e75ecd3fe24e93f8a2784cd0fd3e", null ],
      [ "D6_SIMPLE", "_phys_x_types_8hpp.html#afe9609ff15d1276d23956a7249edcef0aa4e00ec92ec5537318ecb39ce589464e", null ]
    ] ]
];