var structsymtable =
[
    [ "recursion_depth", "structsymtable.html#af3722b3fc714efc89205483115e9e0ae", null ],
    [ "recursion_limit", "structsymtable.html#aed793b168492f8eb27752d1c36122f80", null ],
    [ "st_blocks", "structsymtable.html#a0cdfefdd535d12f0f2b9587d23a9ac7e", null ],
    [ "st_cur", "structsymtable.html#ae79ef4b7d6cd0f034c75561fb4ae5aa0", null ],
    [ "st_filename", "structsymtable.html#a3a75559c0160e93600882ad8d51f1cbe", null ],
    [ "st_future", "structsymtable.html#ae8e551973e941466d3d5fee2f96c0815", null ],
    [ "st_global", "structsymtable.html#a7c81861d1b92f6354eee2de2e83585fd", null ],
    [ "st_nblocks", "structsymtable.html#a5a1d9787075312c4d687157e452235b9", null ],
    [ "st_private", "structsymtable.html#a9de0bc82e83fa302373057f56043fdbb", null ],
    [ "st_stack", "structsymtable.html#a86921f5d2e12dee08427e02bac3a95ee", null ],
    [ "st_top", "structsymtable.html#a1e660bc90af2b75c54cf04c639dd6d29", null ]
];