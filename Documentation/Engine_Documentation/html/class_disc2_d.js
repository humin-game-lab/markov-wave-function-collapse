var class_disc2_d =
[
    [ "Disc2D", "class_disc2_d.html#a0f807046798971dffad13dfec34f1d0d", null ],
    [ "Disc2D", "class_disc2_d.html#a993b71c63d7cee167d85596fc9dda2c6", null ],
    [ "~Disc2D", "class_disc2_d.html#a99df28dec78356544641c61bdc03301a", null ],
    [ "GetCentre", "class_disc2_d.html#a220cafe3e7227d25e84f31f3bf25857c", null ],
    [ "GetRadius", "class_disc2_d.html#a74474f60697523551cb1d30641b9c9bd", null ],
    [ "TranslateByVector", "class_disc2_d.html#a91b1dcecfe394ea8e3eaf4ec86842f00", null ]
];