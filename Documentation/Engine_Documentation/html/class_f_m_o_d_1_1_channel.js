var class_f_m_o_d_1_1_channel =
[
    [ "getChannelGroup", "class_f_m_o_d_1_1_channel.html#a4b6d310d04eb844190db7a4074af2b2c", null ],
    [ "getCurrentSound", "class_f_m_o_d_1_1_channel.html#af8c6cb4f1490eda077f4a661437a0809", null ],
    [ "getFrequency", "class_f_m_o_d_1_1_channel.html#a99f1dcf4b2a67f509d08f138e9e3cbca", null ],
    [ "getIndex", "class_f_m_o_d_1_1_channel.html#ad1d410d1d16d0c153dd0af5de8fda88e", null ],
    [ "getLoopCount", "class_f_m_o_d_1_1_channel.html#a23128bf2475ef316e1c38a3606ccbfd0", null ],
    [ "getLoopPoints", "class_f_m_o_d_1_1_channel.html#a32b040f752fc4fba7d46054ddbd6b913", null ],
    [ "getPosition", "class_f_m_o_d_1_1_channel.html#a580cb740f80cf139faaf21181490d98f", null ],
    [ "getPriority", "class_f_m_o_d_1_1_channel.html#ae2a16710e706c0223937bc99fb3cad3d", null ],
    [ "isVirtual", "class_f_m_o_d_1_1_channel.html#aba59130d19b04270bbe8d765d79018f9", null ],
    [ "setChannelGroup", "class_f_m_o_d_1_1_channel.html#a570b83eb8178325a131b081a8d5ffd81", null ],
    [ "setFrequency", "class_f_m_o_d_1_1_channel.html#a355cd052a405c0ecdbaac7c65d24148b", null ],
    [ "setLoopCount", "class_f_m_o_d_1_1_channel.html#a7dc161cf3a929cd48da580634059f6a6", null ],
    [ "setLoopPoints", "class_f_m_o_d_1_1_channel.html#abcac3fd6f8fb5a3f1097237a9ff00ee6", null ],
    [ "setPosition", "class_f_m_o_d_1_1_channel.html#a46f8f6738b500bbb995bd4a4ecce826f", null ],
    [ "setPriority", "class_f_m_o_d_1_1_channel.html#a83a9a2f49e4bdde446f0d574847f5329", null ]
];