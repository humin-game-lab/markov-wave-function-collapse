var _phys_x_vehicle_create_8hpp =
[
    [ "ActorUserData", "structvehicle_1_1_actor_user_data.html", "structvehicle_1_1_actor_user_data" ],
    [ "ShapeUserData", "structvehicle_1_1_shape_user_data.html", "structvehicle_1_1_shape_user_data" ],
    [ "VehicleDesc", "structvehicle_1_1_vehicle_desc.html", "structvehicle_1_1_vehicle_desc" ],
    [ "VEHICLE_COMMON_H", "_phys_x_vehicle_create_8hpp.html#a2da73029ff80b2dfd6b59f69299deabc", null ],
    [ "configureUserData", "_phys_x_vehicle_create_8hpp.html#a22cc0d28113dfe7beb97c402e49a8d4f", null ],
    [ "createChassisMesh", "_phys_x_vehicle_create_8hpp.html#a1b96eb46cea960616788c492e6994e20", null ],
    [ "createDrivablePlane", "_phys_x_vehicle_create_8hpp.html#a2b4dc02a9d74b8097a085a284af41336", null ],
    [ "createVehicle4W", "_phys_x_vehicle_create_8hpp.html#ad1029d0d454f9bb6452113b64b5f3f69", null ],
    [ "createVehicleActor", "_phys_x_vehicle_create_8hpp.html#a289eaabd6bee5aa657d7dcc3c75eb619", null ],
    [ "createWheelMesh", "_phys_x_vehicle_create_8hpp.html#a8b455089b37fa6edd650de1ad7f124b4", null ],
    [ "customizeVehicleToLengthScale", "_phys_x_vehicle_create_8hpp.html#afc30f22cee0f1302ad6a810095a5e39a", null ],
    [ "customizeVehicleToLengthScale", "_phys_x_vehicle_create_8hpp.html#a835aa644aaa67bfc44b3f2bc8007a93c", null ]
];