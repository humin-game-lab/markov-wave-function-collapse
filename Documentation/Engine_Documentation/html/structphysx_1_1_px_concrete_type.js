var structphysx_1_1_px_concrete_type =
[
    [ "Enum", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3f", [
      [ "eUNDEFINED", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa1e5e984fe4a110c262c91e414ce7e042", null ],
      [ "eHEIGHTFIELD", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa4ab644f203ee871bb8052fe37a7eea69", null ],
      [ "eCONVEX_MESH", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa175a7f518dd6f620c1f465f6f668d61f", null ],
      [ "eTRIANGLE_MESH_BVH33", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa2f40959c66316434cce718c4daae62f1", null ],
      [ "eTRIANGLE_MESH_BVH34", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa3969b9623480218881139c19a0aa58d1", null ],
      [ "eRIGID_DYNAMIC", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa3a4fcaeaeb10eaa98b2bfc0a0b13e651", null ],
      [ "eRIGID_STATIC", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fae238f80b3a3263638aaaae04aeb89c2a", null ],
      [ "eSHAPE", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa3b44d50a92329b1b041cad8707107a74", null ],
      [ "eMATERIAL", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fad8895eaca26d394b2cb1c87295a46e43", null ],
      [ "eCONSTRAINT", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa3affac757b2be1a57a271f313c08701b", null ],
      [ "eAGGREGATE", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa555d89dddf8729604a37d54eeaf893d6", null ],
      [ "eARTICULATION", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa80c30b4c1c903b74dd7317b812cdcd6f", null ],
      [ "eARTICULATION_REDUCED_COORDINATE", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa05d2b8aa0080ae70ca6ce3b2b17c16ed", null ],
      [ "eARTICULATION_LINK", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fafcfc73d3f3d9af7cbebd877271458c96", null ],
      [ "eARTICULATION_JOINT", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa2784d325faec9261d744744c7b09d596", null ],
      [ "eARTICULATION_JOINT_REDUCED_COORDINATE", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fadd9122e6fe81a0c787220ce96d16e07b", null ],
      [ "ePRUNING_STRUCTURE", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa74454863a8500f3ecbd9d4cfd4e64530", null ],
      [ "eBVH_STRUCTURE", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa72cf6aaf9912667b72e8bafdcae101c7", null ],
      [ "ePHYSX_CORE_COUNT", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3faf6eb223d38724cd374998f89dd1625a3", null ],
      [ "eFIRST_PHYSX_EXTENSION", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa68bafcf9b5a1b998d78dd7df51dd5c36", null ],
      [ "eFIRST_VEHICLE_EXTENSION", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa8493b040a55193fa0cc82f488e82dc9e", null ],
      [ "eFIRST_USER_EXTENSION", "structphysx_1_1_px_concrete_type.html#afeb58083f86f214b61abab4a62b06f3fa80e2dd420172f4f503d58ec96fc96431", null ]
    ] ]
];