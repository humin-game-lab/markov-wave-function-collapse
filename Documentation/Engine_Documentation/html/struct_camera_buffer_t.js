var struct_camera_buffer_t =
[
    [ "CameraPosition", "struct_camera_buffer_t.html#aa985fc745ee3dd0630271d3d603f7ac6", null ],
    [ "pad", "struct_camera_buffer_t.html#abbdccb302753811675fea5fcfb4e4d80", null ],
    [ "ProjectionMatrix", "struct_camera_buffer_t.html#a8edee0da0f62720abd2b21c6a14c053c", null ],
    [ "ViewMatrix", "struct_camera_buffer_t.html#af2444ebe2c117c72e2b9394838eacb16", null ]
];