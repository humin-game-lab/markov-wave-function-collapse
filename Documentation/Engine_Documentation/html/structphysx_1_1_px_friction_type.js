var structphysx_1_1_px_friction_type =
[
    [ "Enum", "structphysx_1_1_px_friction_type.html#aa2bd47515042cacbc4e34cb163331a86", [
      [ "ePATCH", "structphysx_1_1_px_friction_type.html#aa2bd47515042cacbc4e34cb163331a86a34ef7028887de433ee6e0e684269572e", null ],
      [ "eONE_DIRECTIONAL", "structphysx_1_1_px_friction_type.html#aa2bd47515042cacbc4e34cb163331a86afe7ec72e18480cbbc52da4c69c47f144", null ],
      [ "eTWO_DIRECTIONAL", "structphysx_1_1_px_friction_type.html#aa2bd47515042cacbc4e34cb163331a86a6df6b8e77adce0be2539f645c6e2c494", null ],
      [ "eFRICTION_COUNT", "structphysx_1_1_px_friction_type.html#aa2bd47515042cacbc4e34cb163331a86a026ce7982914d0f7e97e44166a663536", null ]
    ] ]
];