var structphysx_1_1_px_contact_patch =
[
    [ "PxContactPatchFlags", "structphysx_1_1_px_contact_patch.html#a72ab3fbb5edf17be89db9589e74c37ea", [
      [ "eHAS_FACE_INDICES", "structphysx_1_1_px_contact_patch.html#a72ab3fbb5edf17be89db9589e74c37eaa0f5cba8b2409a3dcc1a9981279de253d", null ],
      [ "eMODIFIABLE", "structphysx_1_1_px_contact_patch.html#a72ab3fbb5edf17be89db9589e74c37eaa7f3f03d4e81eca704ab0dad46cab1e82", null ],
      [ "eFORCE_NO_RESPONSE", "structphysx_1_1_px_contact_patch.html#a72ab3fbb5edf17be89db9589e74c37eaac2004cfec76bce65297d7bb1c501d695", null ],
      [ "eHAS_MODIFIED_MASS_RATIOS", "structphysx_1_1_px_contact_patch.html#a72ab3fbb5edf17be89db9589e74c37eaa8bf758ec9c8460acf5d80e0e0f5c4455", null ],
      [ "eHAS_TARGET_VELOCITY", "structphysx_1_1_px_contact_patch.html#a72ab3fbb5edf17be89db9589e74c37eaa27eb1a6804bef22e70cb6cab513c4b47", null ],
      [ "eHAS_MAX_IMPULSE", "structphysx_1_1_px_contact_patch.html#a72ab3fbb5edf17be89db9589e74c37eaa6cd93f7d377b08e3efc8824c715a20b8", null ],
      [ "eREGENERATE_PATCHES", "structphysx_1_1_px_contact_patch.html#a72ab3fbb5edf17be89db9589e74c37eaab086df13f7b7125770139a798b3639c9", null ],
      [ "eCOMPRESSED_MODIFIED_CONTACT", "structphysx_1_1_px_contact_patch.html#a72ab3fbb5edf17be89db9589e74c37eaa030f39a2e60ecc49ab7c32c9a1740992", null ]
    ] ],
    [ "PX_ALIGN", "structphysx_1_1_px_contact_patch.html#a96d22d50e337b17fa348c52074833a3e", null ],
    [ "PX_ALIGN", "structphysx_1_1_px_contact_patch.html#a294d6f0ed159ad2626cad78f619d4eda", null ],
    [ "dynamicFriction", "structphysx_1_1_px_contact_patch.html#a7afc97c283fb00ffd8f65318ff9c1c37", null ],
    [ "internalFlags", "structphysx_1_1_px_contact_patch.html#a25d79d43f4ed6168a32ed1807f1efa84", null ],
    [ "materialFlags", "structphysx_1_1_px_contact_patch.html#a4f0d73e3b1f174dc52394656ad628d12", null ],
    [ "materialIndex0", "structphysx_1_1_px_contact_patch.html#ab943c88362bff290658ecadbc953541d", null ],
    [ "materialIndex1", "structphysx_1_1_px_contact_patch.html#a93e55e8b19a0b28ae13a81c60b29b131", null ],
    [ "nbContacts", "structphysx_1_1_px_contact_patch.html#a018d6fb2edee8d2be8159a4564dfd630", null ],
    [ "restitution", "structphysx_1_1_px_contact_patch.html#a2b92d096d350c667ec73d460272b2785", null ],
    [ "startContactIndex", "structphysx_1_1_px_contact_patch.html#afd08c880e1299d4e55f1acd7c06d4cb9", null ],
    [ "staticFriction", "structphysx_1_1_px_contact_patch.html#abc16ed23e6c1244303609a3c0d73b206", null ]
];