var classphysx_1_1_px_simulation_statistics =
[
    [ "RbPairStatsType", "classphysx_1_1_px_simulation_statistics.html#a57d54a592e3bd9c1b46366ce13185a97", [
      [ "eDISCRETE_CONTACT_PAIRS", "classphysx_1_1_px_simulation_statistics.html#a57d54a592e3bd9c1b46366ce13185a97ae1ff6940cc4a425a4eb1a2efde038723", null ],
      [ "eCCD_PAIRS", "classphysx_1_1_px_simulation_statistics.html#a57d54a592e3bd9c1b46366ce13185a97a70c239c8d9140cb57421382a81f7dc59", null ],
      [ "eMODIFIED_CONTACT_PAIRS", "classphysx_1_1_px_simulation_statistics.html#a57d54a592e3bd9c1b46366ce13185a97a513759f7d468d34d315e09551fb63d70", null ],
      [ "eTRIGGER_PAIRS", "classphysx_1_1_px_simulation_statistics.html#a57d54a592e3bd9c1b46366ce13185a97a6b5e6de559738d68471966eeb4ff641c", null ]
    ] ],
    [ "PxSimulationStatistics", "classphysx_1_1_px_simulation_statistics.html#afadccd49fc0ca23947e77d0fcf090e37", null ],
    [ "getNbBroadPhaseAdds", "classphysx_1_1_px_simulation_statistics.html#af33adadf9fd54c6e0f2c8fcb53321d14", null ],
    [ "getNbBroadPhaseRemoves", "classphysx_1_1_px_simulation_statistics.html#a936c9334c9702d7ed48737a4c042b2ed", null ],
    [ "getRbPairStats", "classphysx_1_1_px_simulation_statistics.html#a28b2a8777b40440b259f549b7f83888d", null ],
    [ "compressedContactSize", "classphysx_1_1_px_simulation_statistics.html#a562f80dd3b56f2821942c2a9426ec3e0", null ],
    [ "nbActiveConstraints", "classphysx_1_1_px_simulation_statistics.html#a7ddfa057e14246355b14c186824d9fec", null ],
    [ "nbActiveDynamicBodies", "classphysx_1_1_px_simulation_statistics.html#aa0f27f3eb1fe17ab23b6980f64362148", null ],
    [ "nbActiveKinematicBodies", "classphysx_1_1_px_simulation_statistics.html#a5b888be8e995cc722255031e8feb6f22", null ],
    [ "nbAggregates", "classphysx_1_1_px_simulation_statistics.html#af2997e8e7a3452c295d5db8cfd661aa7", null ],
    [ "nbArticulations", "classphysx_1_1_px_simulation_statistics.html#ab3eda53668ad0c7014e41f45b05f1a1f", null ],
    [ "nbAxisSolverConstraints", "classphysx_1_1_px_simulation_statistics.html#a88246b64b2a8efad7b77c8f3a41175f1", null ],
    [ "nbBroadPhaseAdds", "classphysx_1_1_px_simulation_statistics.html#a47b635eb117d6fba973553212a4afa59", null ],
    [ "nbBroadPhaseRemoves", "classphysx_1_1_px_simulation_statistics.html#a24c5b0d294f3079d2f35a473d3b45acd", null ],
    [ "nbCCDPairs", "classphysx_1_1_px_simulation_statistics.html#a2ae3c9e8afaf32c2ba188a591a0ac166", null ],
    [ "nbDiscreteContactPairs", "classphysx_1_1_px_simulation_statistics.html#a548f6cb9fc48d7227d5987043323607f", null ],
    [ "nbDiscreteContactPairsTotal", "classphysx_1_1_px_simulation_statistics.html#acb250c4e7f1f79817a34b1d849ca98a5", null ],
    [ "nbDiscreteContactPairsWithCacheHits", "classphysx_1_1_px_simulation_statistics.html#a15b752edfd0d47a7bd10ef272ace0847", null ],
    [ "nbDiscreteContactPairsWithContacts", "classphysx_1_1_px_simulation_statistics.html#ae5734639714fdd65b7a8897965f72cdb", null ],
    [ "nbDynamicBodies", "classphysx_1_1_px_simulation_statistics.html#a316c67496e75c9fca03946002642ced0", null ],
    [ "nbKinematicBodies", "classphysx_1_1_px_simulation_statistics.html#ae660afac824314e941d50e46a0c1d647", null ],
    [ "nbLostPairs", "classphysx_1_1_px_simulation_statistics.html#aa461b72e55f113c6316edce16ffe1b30", null ],
    [ "nbLostTouches", "classphysx_1_1_px_simulation_statistics.html#a8e158d72d7fcbf6c4f5abc8d158eaa75", null ],
    [ "nbModifiedContactPairs", "classphysx_1_1_px_simulation_statistics.html#a1a94818451e88a0e7ac34ba470d0d2fc", null ],
    [ "nbNewPairs", "classphysx_1_1_px_simulation_statistics.html#a571ea29e1e00f19af778f31b03848cc5", null ],
    [ "nbNewTouches", "classphysx_1_1_px_simulation_statistics.html#ada91b2d90972dd3c3ac123ae12f9cd28", null ],
    [ "nbPartitions", "classphysx_1_1_px_simulation_statistics.html#a2b85892dff03d2371886c65e4081db3d", null ],
    [ "nbShapes", "classphysx_1_1_px_simulation_statistics.html#ab7a159432b2f136036262f9fa291b2a9", null ],
    [ "nbStaticBodies", "classphysx_1_1_px_simulation_statistics.html#a63ebc462f8ee2f9f7290c5638580f8c4", null ],
    [ "nbTriggerPairs", "classphysx_1_1_px_simulation_statistics.html#a1ece52e7bc140a0b6a3dcef4a3a7019a", null ],
    [ "peakConstraintMemory", "classphysx_1_1_px_simulation_statistics.html#a2a052a8041f0e8eac33ce443b9da3702", null ],
    [ "requiredContactConstraintMemory", "classphysx_1_1_px_simulation_statistics.html#a0b6ed4b5870a41b128bf6272bcd7eb43", null ]
];