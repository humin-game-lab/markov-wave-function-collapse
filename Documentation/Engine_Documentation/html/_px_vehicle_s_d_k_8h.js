var _px_vehicle_s_d_k_8h =
[
    [ "PxVehicleTypes", "structphysx_1_1_px_vehicle_types.html", "structphysx_1_1_px_vehicle_types" ],
    [ "PxVehicleConcreteType", "structphysx_1_1_px_vehicle_concrete_type.html", "structphysx_1_1_px_vehicle_concrete_type" ],
    [ "PxVehicleUpdateMode", "structphysx_1_1_px_vehicle_update_mode.html", "structphysx_1_1_px_vehicle_update_mode" ],
    [ "PX_DEBUG_VEHICLE_ON", "_px_vehicle_s_d_k_8h.html#a0c3a7dea79cf9968bd350a49ce0fd770", null ],
    [ "PX_MAX_NB_WHEELS", "_px_vehicle_s_d_k_8h.html#aa52f6479b78eccd706af05e1e0a0c43f", null ],
    [ "PxCloseVehicleSDK", "_px_vehicle_s_d_k_8h.html#a5d073c9d7f836e3f38e960c0eb6c93fe", null ],
    [ "PxInitVehicleSDK", "_px_vehicle_s_d_k_8h.html#a056693017bef87b785a774ce7f710ee9", null ],
    [ "PxVehicleSetBasisVectors", "_px_vehicle_s_d_k_8h.html#a7357616e2b02b6ec74cd3ea3ec5e156d", null ],
    [ "PxVehicleSetMaxHitActorAcceleration", "_px_vehicle_s_d_k_8h.html#ad9563ca79320f49cdab235a7f61a1625", null ],
    [ "PxVehicleSetSweepHitRejectionAngles", "_px_vehicle_s_d_k_8h.html#ae58125ffc143d8022f74ba74a6545e89", null ],
    [ "PxVehicleSetUpdateMode", "_px_vehicle_s_d_k_8h.html#aa22f89be9e818c8848bca86940525bef", null ]
];