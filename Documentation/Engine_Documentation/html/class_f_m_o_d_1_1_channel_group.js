var class_f_m_o_d_1_1_channel_group =
[
    [ "addGroup", "class_f_m_o_d_1_1_channel_group.html#a8f9abbc359b51ed16f8bf158b0d62ba6", null ],
    [ "getChannel", "class_f_m_o_d_1_1_channel_group.html#aadda005170e610a32c03473499b94823", null ],
    [ "getGroup", "class_f_m_o_d_1_1_channel_group.html#a5ac9853233cf3fa367022f1219db26ff", null ],
    [ "getName", "class_f_m_o_d_1_1_channel_group.html#a152e3dfbbde9a125c002845e8ffd9231", null ],
    [ "getNumChannels", "class_f_m_o_d_1_1_channel_group.html#a31d5e3b8424b09069841b40633dee207", null ],
    [ "getNumGroups", "class_f_m_o_d_1_1_channel_group.html#afa0b87ae5828685c64c1aa921872fd58", null ],
    [ "getParentGroup", "class_f_m_o_d_1_1_channel_group.html#ab953a5c6bf1dc0d25bed4af7c0653bee", null ],
    [ "release", "class_f_m_o_d_1_1_channel_group.html#a4f7fc3cf21050e3cd5e84f8a2f9a0fc9", null ]
];