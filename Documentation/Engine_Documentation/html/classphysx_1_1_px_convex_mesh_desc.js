var classphysx_1_1_px_convex_mesh_desc =
[
    [ "PxConvexMeshDesc", "classphysx_1_1_px_convex_mesh_desc.html#a34d5f1913d9862f66ecf1ffdd7fd6cf8", null ],
    [ "isValid", "classphysx_1_1_px_convex_mesh_desc.html#a600d025b4ba6da350b0db1e7dd9218c5", null ],
    [ "setToDefault", "classphysx_1_1_px_convex_mesh_desc.html#a45dfa1e6e044010dd745821f00f33c00", null ],
    [ "flags", "classphysx_1_1_px_convex_mesh_desc.html#a04e3081b1a51240afd8a341f66b1a966", null ],
    [ "indices", "classphysx_1_1_px_convex_mesh_desc.html#a7efdc461d784e702ce4ef42d491cbaed", null ],
    [ "points", "classphysx_1_1_px_convex_mesh_desc.html#adf8537597f2caf5aaf8f3ddabade06d4", null ],
    [ "polygons", "classphysx_1_1_px_convex_mesh_desc.html#a9adcf3327cfbe1156840ca00466557c0", null ],
    [ "quantizedCount", "classphysx_1_1_px_convex_mesh_desc.html#a4776167b8a48f7d7344651b4bda1503e", null ],
    [ "vertexLimit", "classphysx_1_1_px_convex_mesh_desc.html#a3887058887d32c268694bd4fc464e42b", null ]
];