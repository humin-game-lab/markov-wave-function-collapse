var structphysx_1_1_px_controller_shape_type =
[
    [ "Enum", "structphysx_1_1_px_controller_shape_type.html#ad409bab07e87aa25b7221ccebb065e0b", [
      [ "eBOX", "structphysx_1_1_px_controller_shape_type.html#ad409bab07e87aa25b7221ccebb065e0bae809fbdbe553f3f22f57532fcd3b5fac", null ],
      [ "eCAPSULE", "structphysx_1_1_px_controller_shape_type.html#ad409bab07e87aa25b7221ccebb065e0bac999208b838ff2a995b98e7eaa9e1877", null ],
      [ "eFORCE_DWORD", "structphysx_1_1_px_controller_shape_type.html#ad409bab07e87aa25b7221ccebb065e0ba98c13dc567ab1b640c481bddf61746ce", null ]
    ] ]
];